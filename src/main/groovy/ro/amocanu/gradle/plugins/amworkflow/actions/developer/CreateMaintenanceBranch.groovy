/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.actions.versioned.Version
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

@PackageScope
class CreateMaintenanceBranch extends AbstractDeveloperAction {

    private final PluginExt ext
    private final GitRepo git
    private final String majorSegmentAsString

    CreateMaintenanceBranch(PluginExt ext, GitRepo git, String majorSegmentAsString) {
        super(ConfigurationFactory.fromExtension(ext), git)
        this.ext = ext
        this.git = git
        this.majorSegmentAsString = majorSegmentAsString
    }

    @Override
    protected void runDeveloperAction() {
        int majorSegment
        try {
            majorSegment = Integer.valueOf(majorSegmentAsString)
        } catch (any) {
            throw new GradleException("Could not convert '${majorSegmentAsString}' to an integer!")
        }

        if (majorSegment < 1) {
            throw new GradleException('Maintenance branches may only be created starting with version 1!')
        }

        def workerConf = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, majorSegmentAsString)
        def worker = new CreateMaintenanceBranchWorker(workerConf, git, majorSegment)
        worker.run()
    }

    private class CreateMaintenanceBranchWorker implements Runnable {

        private final int majorVersionSegment
        private final Configuration conf
        private String startPoint

        CreateMaintenanceBranchWorker(Configuration conf, GitRepo git, int major) {
            this.majorVersionSegment = major
            this.conf = conf
        }
        
        @Override
        public void run() {
            maintenanceBranchMustNotExist()
            maintenanceBranchMustNotExistWithDifferentCase()
            fetchAllTagsFromCentral()
            findStartingPoint()
            nextVersionMustExist()
            createLocalMaintenanceBranch()
            pushMaintenanceBranchToCentral()
        }
        
        private void maintenanceBranchMustNotExist() {
            def exception = new GradleException("Maintenance branch '${conf.releaseBranch}' already exists!")

            if (git.isLocalBranch(conf.releaseBranch)) {
                throw exception
            }

            if (git.isCentralBranch(conf.releaseBranch)) {
                throw exception
            }
        }

        private void maintenanceBranchMustNotExistWithDifferentCase() {
            def releaseBranch = conf.releaseBranch
            def branch = git.localBranches().find { releaseBranch.equalsIgnoreCase(it) }
            if (branch) {
                throw new GradleException("Maintenance branch '${releaseBranch}' already exists with a different case: '${branch}'!")
            }
    
            def remoteFeatureBranch = "${conf.centralGitRepository}/${releaseBranch}"
            def remoteBranch = git.remoteTrackingCentralBranches().find { remoteFeatureBranch.equalsIgnoreCase(it) }
            if (remoteBranch) {
                throw new GradleException("Remote-tracking maintenance branch '${remoteFeatureBranch}' already exists with a different case: '${remoteBranch}'!")
            }
        }

        private void fetchAllTagsFromCentral() {
            git.fetchAllTags()
        }

        private findStartingPoint() {
            def allTags = git.allTags()
            def allCandidateVersions = allTags
                .findAll { tag -> conf.isReleaseName(tag) }
                .collect { tag -> conf.releaseIdFromReleaseName(tag) }
                .findAll { releaseId ->
                    Version.isVersionString(releaseId) && Version.fromString(releaseId).major == majorVersionSegment }

            if (!allCandidateVersions) {
                throw new GradleException(
                    "Maintenance branch '${conf.releaseBranch}' cannot be created because no appropriate version tag was found!")
            }

            startPoint = conf.releaseNameFromReleaseId(allCandidateVersions.sort().last())
        }

        private void nextVersionMustExist() {
            def nextVersionMajor = majorVersionSegment + 1
            def allTags = git.allTags()
            def nextVersionTags = allTags
                .findAll { tag -> conf.isReleaseName(tag) }
                .collect { tag -> conf.releaseIdFromReleaseName(tag) }
                .any { releaseId ->
                    Version.isVersionString(releaseId) && Version.fromString(releaseId).major == nextVersionMajor }

            if (!nextVersionTags) {
                def nextVersion = (new Version(nextVersionMajor, 0, 0)).toString()
                throw new GradleException(
                    "Maintenance branch '${conf.releaseBranch}' cannot be created before version ${nextVersion} is released!")
            }
        }

        private void createLocalMaintenanceBranch() {
            git.createLocalAndRemoteBranchAtTag(conf.releaseBranch, startPoint)
            git.checkoutBranch(conf.releaseBranch)

            println "Created branch '${conf.releaseBranch}' from tag '${startPoint}'"
        }

        private void pushMaintenanceBranchToCentral() {
            def remoteName = conf.centralGitRepository
            git.pushBranchToCentral(conf.releaseBranch)

            println "Pushed branch '${conf.releaseBranch}' to '${remoteName}'"
        }
    }
}
