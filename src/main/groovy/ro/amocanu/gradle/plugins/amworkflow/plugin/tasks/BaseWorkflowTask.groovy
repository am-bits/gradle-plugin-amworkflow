/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.plugin.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

abstract class BaseWorkflowTask extends DefaultTask {

    @TaskAction
    def baseTaskAction() {
        PluginExt.validate(project)

        PluginExt ext = PluginExt.extensionOf(project)
        GitRepo git = makeGitRepo(ext)

        try {
            makeDelegate(ext, git).run()
        } finally {
            git.close()
        }
    }

    private GitRepo makeGitRepo(PluginExt ext) {
        String central
        String username
        String password

        switch (ext.root.workflowType) {
            case WorkflowType.VERSIONED.toString():
                central = ext.versioned.centralGitRepository
                username = ext.versioned.gitUsername
                password = ext.versioned.gitPassword
                break
            case WorkflowType.CONTINUOUS.toString():
                central = ext.continuous.centralGitRepository
                username = ext.continuous.gitUsername
                password = ext.continuous.gitPassword
                break
            default:
                assert false
        }

        return username && password ?
            GrGitRepo.open(project.projectDir.canonicalPath, central, username, password) :
            GrGitRepo.open(project.projectDir.canonicalPath, central)
    }

    abstract protected Runnable makeDelegate(PluginExt ext, GitRepo git)
    
    protected String usage(Iterable params) {
        Iterable usages = params.collect { it.usage }
        return "\n\tUsage: ${name} ${usages.join(' ')}"
    }
}
