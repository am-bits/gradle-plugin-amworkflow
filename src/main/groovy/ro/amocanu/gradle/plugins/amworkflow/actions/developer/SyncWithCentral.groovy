/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

@PackageScope
class SyncWithCentral implements Runnable {

    private final PluginExt ext
    private final GitRepo git
    private final Configuration conf

    private String initialBranch

    SyncWithCentral(PluginExt ext, GitRepo git) {
        this.ext = ext
        this.git = git
        this.conf = ConfigurationFactory.fromExtension(ext)
    }

    @Override
    void run() {
        mustNotBeDetachedHead() // detached head not allowed because the action checks out branches

        def mainBranchWorker = new SyncWithCentralWorker(conf, git)

        mainBranchWorker.repoMustNotHaveChanges()
        mainBranchWorker.fetchFromCentralAndPrune()
        mainBranchWorker.mustHaveRemoteReleaseBranch()

        initialBranch = git.currentBranch()
        mainBranchWorker.checkOutBranch(conf.releaseBranch)

        try {
            mainBranchWorker.run()

            if (conf.versioned) {
                removeLocalOnlyMaintenanceBranches()
                removeLocalOnlyPseudoMaintenanceBranches()
                centralShouldNotHavePseudoMaintenanceBranches()

                Iterable<String> maintainedVersions = getMaintainedVersions()
                for (String v in maintainedVersions) {
                    def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v)
                    def maintenanceBranchWorker = new SyncWithCentralWorker(conf_v, git)
                    maintenanceBranchWorker.run()
                }
            }
        } finally {
            checkOutInitialBranchOrReleaseBranch()
        }
    }

    private void mustNotBeDetachedHead() {
        if (git.isDetachedHead()) {
            throw new GradleException('Cannot perform task while in detached head!')
        }
    }

    private static Collection<String> removeLocalOnlyBranches(GitRepo git, Closure<Boolean> matchingCriteria) {
        def report =
            git.localBranches()
            .findAll{ branch ->
                matchingCriteria(branch) && !git.isCentralBranch(branch)
            }
            .collect { branch ->
                def commit = git.commitIdOfBranch(branch)
                git.removeBranch(branch)
                "${branch} (was commit: ${commit})"
            }
        return report
    }

    private static Collection<String> illegalCentralBranches(GitRepo git, Closure<Boolean> matchingCriteria) {
        def report =
            git.remoteTrackingCentralBranches()
            .findAll { remoteBranch ->
                def n = remoteBranch.indexOf('/')
                def branch = remoteBranch[n+1..-1]
                matchingCriteria(branch)
            }
            .collect { remoteBranch ->
                def commit = git.commitIdOfRemoteBranch(remoteBranch)
                "${remoteBranch} (see commit: ${commit})"
            }
        return report
    }

    private void removeLocalOnlyMaintenanceBranches() {
        def removalReport = removeLocalOnlyBranches(git,
            { it ==~ conf.versioned.maintenanceBranchFromMaintainedVersion('[1-9][0-9]*', '') })
        printLocalOnlyBranchesRemovalReport(
            conf.versioned.maintenanceBranchFromMaintainedVersion('*', ''),
            removalReport)
    }

    private void removeLocalOnlyPseudoMaintenanceBranches() {
        def isPseudoMaintenanceBranch = { String it ->
            String maintenancePattern = conf.versioned.maintenanceBranchFromMaintainedVersion('[1-9][0-9]*', '')
            ! (it ==~ maintenancePattern) &&
                it.toLowerCase() ==~ maintenancePattern.toLowerCase()
        }
        def removalReport = removeLocalOnlyBranches(git, isPseudoMaintenanceBranch)
        printIllegalShadowsRemovalReport(
            conf.versioned.maintenanceBranchFromMaintainedVersion('*', ''),
            removalReport)
    }

    private void centralShouldNotHavePseudoMaintenanceBranches() {
        def isPseudoMaintenanceBranch = { String it ->
            String maintenancePattern = conf.versioned.maintenanceBranchFromMaintainedVersion('[1-9][0-9]*', '')
            ! (it ==~ maintenancePattern) &&
                it.toLowerCase() ==~ maintenancePattern.toLowerCase()
        }
        def report = illegalCentralBranches(git, isPseudoMaintenanceBranch)
        printIllegalShadowsOnCentralReport(
            conf.versioned.maintenanceBranchFromMaintainedVersion('*', ''),
            report)
    }

    private static void printLocalOnlyBranchesRemovalReport(String what, Collection<String> removalReport) {
        if (removalReport) {
            def count = removalReport.size()
            if (count == 1) {
                println "Removed local-only '${what}' branch:"
            } else {
                println "Removed local-only '${what}' branches:"
            }
            removalReport.each {
                println "  ${it}"
            }
        }
    }

    private static void printIllegalShadowsRemovalReport(String what, Collection<String> removalReport) {
        if (removalReport) {
            def count = removalReport.size()
            if (count == 1) {
                println "Removed illegal, local-only branch shadowing '${what}':"
            } else {
                println "Removed illegal, local-only branches shadowing '${what}':"
            }
            removalReport.each {
                println "  ${it}"
            }
        }
    }

    private static void printIllegalShadowsOnCentralReport(String what, Collection<String> report) {
        if (report) {
            def count = report.size()
            if (count == 1) {
                println "WARNING: Found illegal branch in central repository, shadowing '${what}'! Some workflow tasks will not work unless this branch is removed:"
            } else {
                println "WARNING: Found illegal branches in central repository, shadowing '${what}'! Some workflow tasks will not work unless these branches are removed:"
            }
            report.each {
                println "  ${it}"
            }
        }
    }

    private Iterable<Integer> getMaintainedVersions() {
        int sz = (conf.centralGitRepository + '/').size()
        git.remoteTrackingCentralBranches()
            .findAll { remoteBranch ->
                assert remoteBranch.size() > sz
                def branch = remoteBranch[sz..-1]
                conf.versioned.isMaintenanceBranch(branch)
            }
            .collect { remoteBranch ->
                conf.versioned.maintainedVersionFromMaintenanceBranch(remoteBranch[sz..-1])
            }
            .toSet()
            .sort()
    }

    private void checkOutInitialBranchOrReleaseBranch() {
        assert (initialBranch)
        if (initialBranch != git.currentBranch()) { 
            if (git.localBranches().any { it.equalsIgnoreCase(initialBranch) }) {
                git.checkoutBranch(initialBranch)
            } else {
                git.checkoutBranch(conf.releaseBranch)
            }
        }
    }

    private class SyncWithCentralWorker extends BaseAction {

        SyncWithCentralWorker(Configuration conf, GitRepo git) {
            super(conf, git)
        }

        @Override
        void run() {
            centralShouldHaveReleaseBranch()
            syncReleaseBranchWithUpstreamIfPossible()

            removeLocalOnlyFeatureBranches()
            removeLocalOnlyPseudoFeatureBranches()
            syncFeatureBranchesWithUpstreamIfPossible()
            centralShouldNotHavePseudoFeatureBranches()

            removeLocalOnlyPreReleaseBranch()
            removeLocalOnlyPseudoPreReleaseBranches()
            centralShouldNotHavePseudoPreReleaseBranch()
        }

        private centralShouldHaveReleaseBranch() {
            if (!git.isCentralBranch(conf.releaseBranch)) {
                println "WARNING: Branch '${conf.releaseBranch}' not found in central repository! Some workflow tasks will not work."
            }
        }

        private void syncReleaseBranchWithUpstreamIfPossible() {
            def releaseBranch = conf.releaseBranch
            def remoteReleaseBranch = "${conf.centralGitRepository}/${conf.releaseBranch}"
            if (git.isLocalBranch(releaseBranch)) {
                syncBranchWithUpstreamIfPossible(releaseBranch, remoteReleaseBranch)
                releaseBranchShouldBeSynced()
            }
        }

        private void syncBranchWithUpstreamIfPossible(String branchName, String upstreamBranchName) {
            def branchToUpstreamRelation = git.relation(
                git.commitIdOfBranch(branchName),
                git.commitIdOfRemoteBranch(upstreamBranchName))
    
            if (branchToUpstreamRelation == Relation.ANCESTOR) {
                git.checkoutBranch(branchName)
                git.fastForwardCurrentBranchTo(upstreamBranchName)
                println "Branch '${branchName}' was fast-forwarded to '${upstreamBranchName}'"
            }
        }

        private void releaseBranchShouldBeSynced() {
            def branchName = conf.releaseBranch
            def upstreamBranchName = "${conf.centralGitRepository}/${conf.releaseBranch}"
    
            def branchToUpstreamRelation = git.relation(
                git.commitIdOfBranch(branchName),
                git.commitIdOfRemoteBranch(upstreamBranchName))
    
            switch (branchToUpstreamRelation) {
                case Relation.SAME:
                    // nothing to do, already sync-ed
                    break
    
                case Relation.ANCESTOR:
                    println "Branch '${branchName}' is behind its upstream '${upstreamBranchName}'"
                    break
    
                case Relation.NONE:
                    println "Branch '${branchName}' is diverging from its upstream '${upstreamBranchName}'!"
                    break
    
                case Relation.DESCENDANT:
                    println "Branch '${branchName}' is ahead of its upstream '${upstreamBranchName}'!"
                    break
    
                default:
                    assert false
            }
        }

        private void removeLocalOnlyFeatureBranches() {
            def removalReport = SyncWithCentral.removeLocalOnlyBranches(git, conf.isFeatureBranch)
            SyncWithCentral.printLocalOnlyBranchesRemovalReport(conf.featureBranchFromFeatureName('*'), removalReport)
        }

        private void removeLocalOnlyPseudoFeatureBranches() {
            def removalReport = SyncWithCentral.removeLocalOnlyBranches(git, conf.isPseudoFeatureBranch)
            SyncWithCentral.printIllegalShadowsRemovalReport(conf.featureBranchFromFeatureName('*'), removalReport)
        }

        private void syncFeatureBranchesWithUpstreamIfPossible() {
            git.localBranches()
                .findAll {
                    conf.isFeatureBranch(it)
                }
                .each { featureBranch ->
                    def remoteFeatureBranch = "${conf.centralGitRepository}/${featureBranch}"
                    syncBranchWithUpstreamIfPossible(featureBranch, remoteFeatureBranch)
                }
        }

        private void centralShouldNotHavePseudoFeatureBranches() {
            def report = SyncWithCentral.illegalCentralBranches(git, conf.isPseudoFeatureBranch)
            SyncWithCentral.printIllegalShadowsOnCentralReport(
                conf.featureBranchFromFeatureName('*'),
                report)
        }

        private void removeLocalOnlyPreReleaseBranch() {
            def removalReport = SyncWithCentral.removeLocalOnlyBranches(git, { it == conf.preReleaseBranch })
            SyncWithCentral.printLocalOnlyBranchesRemovalReport(conf.preReleaseBranch, removalReport)
        }

        private void removeLocalOnlyPseudoPreReleaseBranches() {
            def isPseudoPreReleaseBranch = { String it ->
                it != conf.preReleaseBranch && it.equalsIgnoreCase(conf.preReleaseBranch)
            }
            def removalReport = SyncWithCentral.removeLocalOnlyBranches(git, isPseudoPreReleaseBranch)
            SyncWithCentral.printIllegalShadowsRemovalReport(conf.preReleaseBranch, removalReport)
        }

        private void centralShouldNotHavePseudoPreReleaseBranch() {
            def isPseudoPreReleaseBranch = { String it ->
                it != conf.preReleaseBranch && it.equalsIgnoreCase(conf.preReleaseBranch)
            }
            def report = SyncWithCentral.illegalCentralBranches(git, isPseudoPreReleaseBranch)
            SyncWithCentral.printIllegalShadowsOnCentralReport(
                conf.preReleaseBranch,
                report)
        }
    }
}
