/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.operation.BranchAddOp
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@RunWith(Parameterized.class)
class MissingReleaseBranchTest extends BaseTest {

    private static final ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL = { releaseBranch -> "Branch '${releaseBranch}' not found in central repository!" }

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: ext.root.workflowType),
        continuous: new ContinuousExtension(
            centralGitRepository: ext.continuous.centralGitRepository,
            featureBranchPrefix: ext.continuous.featureBranchPrefix,
            preReleaseBranch: ext.continuous.preReleaseBranch,
            releaseBranch: 'release',
            releaseTagPrefix: ext.continuous.releaseTagPrefix,
            integrationURLTrigger: ext.continuous.integrationURLTrigger,
            ciUsername: ext.continuous.ciUsername,
            ciPassword: ext.continuous.ciPassword))

    private static final PluginExt ext3 = new PluginExt(
        root: new RootExtension(
            workflowType: ext.root.workflowType),
        continuous: new ContinuousExtension(
            centralGitRepository: ext.continuous.centralGitRepository,
            featureBranchPrefix: ext.continuous.featureBranchPrefix,
            preReleaseBranch: ext.continuous.preReleaseBranch,
            releaseBranch: 'MaSteR',
            releaseTagPrefix: ext.continuous.releaseTagPrefix,
            integrationURLTrigger: ext.continuous.integrationURLTrigger,
            ciUsername: ext.continuous.ciUsername,
            ciPassword: ext.continuous.ciPassword))

    private static final String featureName1 = 'stealtheblueprints'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String someRandomBranch = 'makealotofnoise'

    private static final String releaseBranch = ext.continuous.releaseBranch
    private static final String remoteReleaseBranch = "${ext.continuous.centralGitRepository}/${releaseBranch}"
    
    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    private PluginExt wrongExt
    private String releaseBranch2
    private String remoteReleaseBranch2

    @Parameterized.Parameters
    public static Collection pluginExtensions() {
       return [ext2, ext3]
    }

    public MissingReleaseBranchTest(PluginExt ext) {
        this.wrongExt = ext
        this.releaseBranch2 = ext.continuous.releaseBranch
        this.remoteReleaseBranch2 = "${ext.continuous.centralGitRepository}/${releaseBranch2}"
    }
    
    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, null)
    }

    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createFeatureWhenLocalReleaseBranchIsMissing() {
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [ext.continuous.releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 2. the remote-tracking branch exists in the dev repo
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 3. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 4. a random branch is checked-out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_createFeatureWhenLocalAndRemoteTrackingReleaseBranchesAreMissing() {
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch])
        env.devRepo.branch.remove(names: [remoteReleaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 2. the remote-tracking branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 3. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 4. a random branch is checked-out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_createFeatureWhenCentralReleaseBranchIsMissing() {
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)

        def startPoint = branchTip(env.devRepo, releaseBranch)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)
        env.devRepo.branch.add(name: releaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        env.devRepo.branch.add(name: remoteReleaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the release branch does not exist in the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }
        
        // 2. the local release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { releaseBranch2 == it }

        // 3. the remote-tracking release branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // 4. a local branch named like the remote-tracking release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // 5. a random branch is checked-out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(wrongExt, devGit).run()
            DeveloperActions.newCreateFeatureAction(wrongExt, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL(releaseBranch2) == e.getMessage()

        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_goToFeatureWhenLocalReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch exists
        assert branchesOf(env.devRepo).any { featureBranch1 == it }

        // 2. a random branch is checked out
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 4. the remote-tracking branch exists in the dev repo
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 5. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_goToFeatureWhenLocalAndRemoteTrackingReleaseBranchesAreMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])
        env.devRepo.branch.remove(names: [remoteReleaseBranch])
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch exists
        assert branchesOf(env.devRepo).any { featureBranch1 == it }

        // 2. a random branch is checked out
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 4. the remote-tracking branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 5. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_goToFeatureWhenCentralReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        def startPoint = branchTip(env.devRepo, releaseBranch)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)
        env.devRepo.branch.add(name: releaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        env.devRepo.branch.add(name: remoteReleaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch exists
        assert branchesOf(env.devRepo).any { featureBranch1 == it }

        // 2. a random branch is checked out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)

        // 3. the release branch does not exist in the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }
        
        // 4. the local release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { releaseBranch2 == it }

        // 5. a local branch named like the remote-tracking release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // 6. the remote-tracking release branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(wrongExt, devGit).run()
            DeveloperActions.newGoToFeatureAction(wrongExt, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL(releaseBranch2) == e.getMessage()
        
        // 2. on dev repo, the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_discardFeatureWhenLocalReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 3. the remote-tracking branch exists in the dev repo
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 4. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_discardFeatureWhenLocalAndRemoteTrackingReleaseBranchesAreMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])
        env.devRepo.branch.remove(names: [remoteReleaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 3. the remote-tracking branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 4. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_discardFeatureWhenCentralReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        def startPoint = branchTip(env.devRepo, releaseBranch)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)
        env.devRepo.branch.add(name: releaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        env.devRepo.branch.add(name: remoteReleaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the release branch does not exist in the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }
        
        // 3. the local release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { releaseBranch2 == it }

        // 4. the remote-tracking release branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // 5. a local branch named like the remote-tracking release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(wrongExt, devGit).run()
            DeveloperActions.newDiscardFeatureAction(wrongExt, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL(releaseBranch2) == e.getMessage()
        
        // 2. on dev repo, the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
    }

    @Test
    public void test_deliverFeatureWhenLocalReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 3. the remote-tracking branch exists in the dev repo
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 4. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_deliverFeatureWhenLocalAndRemoteTrackingReleaseBranchesAreMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch])
        env.devRepo.branch.remove(names: [remoteReleaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the local release branch does not exist in the dev repo
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }

        // 3. the remote-tracking branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 4. the release branch exists in central repo
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the local release branch tracks the remote-tracking release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }

    @Test
    public void test_deliverFeatureWhenCentralReleaseBranchIsMissing() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        
        def startPoint = branchTip(env.devRepo, releaseBranch)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)
        env.devRepo.branch.add(name: releaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        env.devRepo.branch.add(name: remoteReleaseBranch2, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. the release branch does not exist in the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }
        
        // 3. the local release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { releaseBranch2 == it }

        // 4. the remote-tracking release branch does not exist in the dev repo
        assert ! remoteBranchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // 5. a local branch named like the remote-tracking release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { remoteReleaseBranch2 == it }

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(wrongExt, devGit).run()
            DeveloperActions.newDeliverFeatureAction(wrongExt, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL(releaseBranch2) == e.getMessage()
        
        // 2. on dev repo, the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
    }
}
