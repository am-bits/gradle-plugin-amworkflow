/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

class IntegratorActions {

    static Runnable newCancelIntegrationAction(PluginExt ext, GitRepo git) {
        return new CancelIntegration(ConfigurationFactory.fromExtension(ext), git)
    }
    
    static Runnable newIntegrateFeatureAction(PluginExt ext, GitRepo git) {
        return new IntegrateFeature(ConfigurationFactory.fromExtension(ext), git)
    }

    static Runnable newPrepareFeatureIntegrationAction(PluginExt ext, GitRepo git) {
        return new PrepareFeatureIntegration(ConfigurationFactory.fromExtension(ext), git)
    }

    static Runnable newShowIntegrationStatusAction(PluginExt ext, GitRepo git) {
        return new ShowIntegrationStatus(ConfigurationFactory.fromExtension(ext), git)
    }

    static Runnable newContinuousIntegrateFeatureAction(PluginExt ext, GitRepo git) {
        return new ContinuousIntegrateFeature(ConfigurationFactory.fromExtension(ext), git)
    }

    static Runnable newCreateNewVersionAction(PluginExt ext, GitRepo git, String versionType) {
        return new CreateNewVersion(ConfigurationFactory.fromExtension(ext), git, versionType)
    }

    static Runnable newCancelMaintenanceIntegrationAction(PluginExt ext, GitRepo git, String maintainedVersion) {
        return new CancelIntegration(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion), git)
    }
    
    static Runnable newIntegrateMaintenanceFeatureAction(PluginExt ext, GitRepo git, String maintainedVersion) {
        return new IntegrateFeature(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion), git)
    }

    static Runnable newPrepareMaintenanceFeatureIntegrationAction(PluginExt ext, GitRepo git, String maintainedVersion) {
        return new PrepareFeatureIntegration(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion), git)
    }

    static Runnable newShowMaintenanceIntegrationStatusAction(PluginExt ext, GitRepo git, String maintainedVersion) {
        return new ShowIntegrationStatus(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion), git)
    }

    static Runnable newCreateNewMaintenanceVersionAction(PluginExt ext, GitRepo git, String maintainedVersion, String versionType) {
        return new CreateNewVersion(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion), git, versionType)
    }
}
