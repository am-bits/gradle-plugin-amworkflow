# Continuous delivery

This page describes how to use *am*workflow when doing continuous delivery.

!!! note
    The *am*workflow tasks are quite verbose. If you use a Unix shell, consider [defining and using aliases](../extra/#helpful-aliases) instead of invoking the tasks directly.

___

## The developer workflow

**1. Start a new feature.**

```no-highlight
gradle createFeature -PfeatureName=myFeature
```

The `createFeature` task creates a branch named `feature/myFeature` on the latest commit of the central repo's `master`.

The branch is pushed to the central repo immediately after its creation.

The branch is checked out in the local repo so you can start working on the new feature directly.

The default feature-branch prefix ("feature") and name of the release branch ("master") can be [changed](#configuration).

<hr class="gradient_divider"/>

**1a. Start working on an existing feature. <small>Alternative to step 1.</small>**

```no-highlight
gradle goToFeature -PfeatureName=myFeature
```

The `goToFeature` task checks out an existing feature branch. The branch must exist in the central repo, but not necessarily in the developer's repo (e.g. another developer started the feature).

<hr class="gradient_divider"/>

**2. Develop the feature.**

Write code, commit and push on branch `feature/myFeature`.

You can leverage the standard feature-branch prefix inside a <abbr title="Continuous Integration">CI</abbr> job: trigger a build every time a branch matching the pattern `feature/*` is pushed to the central repo.

<hr class="gradient_divider"/>

**3. Deliver the feature.**

```no-highlight
gradle deliverFeature
```

The task delivers the currently checked-out feature branch for integration.

Under the hood, `deliverFeature` performs a series of checks to ensure that your feature branch is properly synchronized with the central repo and ready to merge into the release branch (i.e. the release branch can be fast-forwarded).

If all goes well, it marks the feature as "delivered" by creating the `preRelease` branch at the tip of the feature branch.

Optionally, `deliverFeature` is also able to trigger a "release" job on your <abbr title="Continuous Integration">CI</abbr> server. (More about this [later](#the-integrator-workflow).)

The `preRelease` branch is transient; it exists only from the moment when a feature is delivered by the developer and up to the moment when the integration is finished, either because the feature was successfully integrated into the release branch, or because the integrator cancelled the integration.

!!! note
    At most one feature can be delivered at a time! If the `preRelease` branch already exists, the `deliverFeature` task will fail and **you must try to deliver the feature again later**.

<hr class="gradient_divider"/>

**3a. Discard the feature. <small>Alternative to step 3.</small>**

```no-highlight
gradle discardFeature -PfeatureName=myFeature
```

The `discardFeature` task removes a feature without delivering it.

The feature branch is deleted from both the local and central repos. If other developers also have the branch locally, it will be removed from their repo by `syncWithCentral` when they run any of the tasks on this page.

<hr class="gradient_divider"/>

<h3 id="developer4"></h3>
**4. Clean-up your local repo.**

```no-highlight
gradle syncWithCentral
```

The task `syncWithCentral` synchronizes the local and central repositories. All other developer tasks depend on this one, but you can also call it directly.

After a feature branch is successfully integrated, calling `syncWithCentral` (directly or indirectly) will remove the branch from the local repo.

More specifically, the `syncWithCentral` task:

  * fetches and prunes <strong>all</strong> remote-tracking branches;
  * removes all *am*workflow branches found exclusively in the local repo; these are assumed to be feature branches that were integrated and thus no longer needed;
  * removes local-only branches that shadow *am*workflow branches e.g.
    * `FEATURE/someFeature` shadows `feature/someFeature`;
  * warns about inconsistencies in the central repository, e.g.
    * the release branch does not exist in the central repo,
    * a branch in the central repo shadows an *am*workflow branch.

___

## The integrator workflow

Consider assigning the integrator role to a <abbr title="Continuous Integration">CI</abbr> server. All the steps described below can be fully automated in [Jenkins](https://jenkins.io) or any other CI server that understands Gradle build files.

<hr class="gradient_divider"/>

<h3 id="integrator1"></h3>
**1. Trigger the integration.**

If you use a CI server that supports URL triggers (for Jenkins, see [here](https://wiki.jenkins-ci.org/display/JENKINS/Remote+access+API)), you can [configure](#jenkins-url-trigger-configuration) *am*workflow to trigger the integration job automatically when the developer delivers the feature.

<hr class="gradient_divider"/>

<h3 id="integrator1a"></h3>
**1a. Check the integration status. <small>Alternative to step 1 when the integrator is a person, not a machine.</small>**

```no-highlight
gradle showIntegrationStatus -Prole=INTEGRATOR
```

The task `showIntegrationStatus` displays whether there is any feature branch that needs to be integrated.

<hr class="gradient_divider"/>

<h3 id="integrator2"></h3>
**2. Prepare the integration.**

```no-highlight
gradle prepareFeatureIntegration -Prole=INTEGRATOR
```

The `prepareFeatureIntegration` task performs various checks and simple actions (i.e. fast-forward `preRelease` or `master` if necessary) to ensure that the integration can be performed correctly.

If branch `preRelease` is not a direct descendant of `master`, `prepareFeatureIntegration` automatically cancels the integration. This could happen if someone messes up with the `preRelease` branch via Git commands instead of using the `deliverFeature` task.

Note that `prepareFeatureIntegration` is also executed at step 4 because we made `integrateFeature` depend on it for added safety. You could skip step 2, but then you'd give up a chance to fail early and end up executing the time-consuming step 3 only to fail at step 4.

<hr class="gradient_divider"/>

<h3 id="integrator3"></h3>
**3. Build and test the pre-release branch.**

Build the code, run the tests. If everything is in order, integrate the pre-release branch. Otherwise, cancel the integration.

<hr class="gradient_divider"/>

<h3 id="integrator4"></h3>
**4. Integrate the pre-release branch**

```no-highlight
gradle integrateFeature -Prole=INTEGRATOR
```

This task merges the `preRelease` branch into `master` (fast-forward) and it removes the `preRelease` branch and the feature branch from the central repo.

Because we're doing continuous delivery, it also creates a timestamp-based release tag.

<hr class="gradient_divider"/>

<h3 id="integrator4a"></h3>
**4a. Cancel the integration. <small>Alternative to step 4.</small>**

```no-highlight
gradle cancelIntegration -Prole=INTEGRATOR
```

The task `cancelIntegration` cancels the integration by removing the `preRelease` without merging it.

The task should be used if the build or the tests failed.

The developer that delivered the feature must fix the problem(s) and deliver the feature again.

___

## The integrator switch

By now you have probably noticed that the `-Prole=INTEGRATOR` property is passed to each and every single integrator task. We call this the "integrator switch".

The integrator switch makes tasks visible to the integrator that are normally hidden from the developers. To see the switch in action, compare the output of `gradle tasks` (we don't use the switch) to the output of `gradle tasks -Prole=INTEGRATOR` (we use the switch).

!!! note
    The integrator switch is just for the convenience of seeing only the developer tasks by default. Nothing prevents a developer from using the integrator switch.

___

## Configuration

*am*workflow comes with sensible defaults, but it is also flexible. To change the defaults or to activate optional features (such as integration with your CI server) add an `amworkflow` configuration block to your `build.gradle` file as described below.

The given values are the default values, except for `workflowType` which has the default value `VERSIONED`.

```no-highlight
amworkflow {
  workflowType = 'CONTINUOUS'
  continuous {
    // if not specified, Git credentials are interactive
    // otherwise, they should be defined in each team member's private gradle.properties file
    gitUsername = ''
    gitPassword = ''

    // the remote name for the central repo
    centralGitRepository = 'origin'

    releaseBranch = 'master'
    featureBranchPrefix = 'feature'
    preReleaseBranch = 'preRelease'
    releaseTagPrefix = 'build-'

    // the URL for triggering the integration job on the CI server
    // when the developer delivers the feature
    integrationURLTrigger = ''

    // CI server credentials, only needed if you use the URL trigger
    // if used, they should be defined in each team member's private gradle.properties file
    ciUsername = ''
    ciPassword = ''
  }
}
```

<h3 id="jenkins-url-trigger-configuration"></h3>
**Jenkins URL trigger configuration example:**

```no-highlight
amworkflow {
  workflowType = 'CONTINUOUS'
  continuous {
    integrationURLTrigger = 'http://ci.example.org/job/CoolProject-release/build?token=4815162342'

    // Jenkins credentials, defined in each team member's private gradle.properties file
    ciUsername = myJenkinsUsername
    ciPassword = myJenkinsPassword
  }
}
```
