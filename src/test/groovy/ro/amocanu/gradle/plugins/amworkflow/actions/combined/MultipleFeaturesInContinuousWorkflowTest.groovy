/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.combined

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MultipleFeaturesInContinuousWorkflowTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'pre',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'FarmResources'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = 'FixFarmResources'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"
    
    private static final String featureName3 = 'BuyEquipment'
    private static final String featureBranch3 = "feature/${featureName3}"
    private static final String remoteFeatureBranch3 = "origin/feature/${featureName3}"

    private static final String featureName4 = 'DefeatEnemies'
    private static final String featureBranch4 = "feature/${featureName4}"
    private static final String remoteFeatureBranch4 = "origin/feature/${featureName4}"

    private static final String featureName5 = 'RedesignLevelUpSystem'
    private static final String featureBranch5 = "feature/${featureName5}"
    private static final String remoteFeatureBranch5 = "origin/feature/${featureName5}"

    private static final String remoteName = ext.continuous.centralGitRepository
    private static final String releaseBranch = ext.continuous.releaseBranch
    private static final String remoteReleaseBranch = "${remoteName}/${ext.continuous.releaseBranch}"

    private static TestEnv env

    private static GitRepo devGit
    private static GitRepo integratorGit
    
    private static TestHelper helper

    @BeforeClass
    public static void setUp() {
        env = new TestEnv()
        
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, remoteName)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, remoteName)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }
    
    @AfterClass
    public static void tearDown() {
        integratorGit.close()
        devGit.close()
        
        env.tearDown()
    }
    
    @Test
    public void step01_createAndDeliverFeature1() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
    }
    
    @Test
    public void step02_integrateFeature1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        helper.test_continuousIntegrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
    }
    
    @Test
    public void step03_createAndDeliverFeature2() {
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
    }
    
    @Test
    public void step04_createAndWorkOnFeature3() {
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
    }

    @Test
    public void step05_integrateFeature2() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }
        
        helper.test_continuousIntegrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch2 == it }
    }

    @Test
    public void step06_moreWorkOnFeature3() {
        helper.test_goToFeature(featureName3, featureBranch3)
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        helper.test_workOnFeatureBranch(featureBranch3)
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. on central, feature3 is now a descendant of the release branch
        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, featureBranch3),
            branchTip(env.centralRepo, releaseBranch))
    }

    @Test
    public void step07_createAndWorkOnFeature4() {
        helper.test_createFeature(featureName4, featureBranch4, remoteFeatureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
    }

    @Test
    public void step08_createAndWorkOnFeature5() {
        helper.test_createFeature(featureName5, featureBranch5, remoteFeatureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
    }

    @Test
    public void step09_deliverFeature4() {
        helper.test_goToFeature(featureName4, featureBranch4)
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        helper.test_deliverFeature(featureBranch4, remoteFeatureBranch4)
    }

    @Test
    public void step10_integrateFeature4() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch4 == it }
        
        helper.test_continuousIntegrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch4 == it }
    }

    @Test
    public void step11_moreWorkOnFeature3() {
        helper.test_goToFeature(featureName3, featureBranch3)
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        helper.test_workOnFeatureBranch(featureBranch3)
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. on central, feature3 is now a descendant of the release branch
        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, featureBranch3),
            branchTip(env.centralRepo, releaseBranch))
    }

    @Test
    public void step12_deliverFeature3() {
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)
    }

    @Test
    public void step13_integrateFeature3() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch3 == it }
        
        helper.test_continuousIntegrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch3 == it }
    }
    
    @Test
    public void step14_deliverFeature5() {
        helper.test_goToFeature(featureName5, featureBranch5)
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        pushBranchToRemote(env.devRepo, remoteName, featureBranch5)

        helper.test_deliverFeature(featureBranch5, remoteFeatureBranch5)
    }

    @Test
    public void step15_integrateFeature5() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch5 == it }
        
        helper.test_continuousIntegrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch5 == it }
    }
}
