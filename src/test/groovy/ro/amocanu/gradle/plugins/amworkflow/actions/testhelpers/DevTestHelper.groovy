/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isFileInRepoOnBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

/**
 * TestHelper with just a dev repo.
 */
class DevTestHelper {

    private final TestHelper th

    DevTestHelper(TestEnv env, PluginExt ext, GitRepo devGit) {
        this.th = new TestHelper(env.devRepo2, null, env.centralRepo, ext, devGit, null)
    }
    
    void test_createFeature(String featureName, String featureBranch, String remoteFeatureBranch) {
        th.test_createFeature(featureName, featureBranch, remoteFeatureBranch)
    }

    void test_goToFeature(String featureName, String featureBranch) {
        th.test_goToFeature(featureName, featureBranch)
    }
    
    void test_workOnFeatureBranch(String featureBranch) {
        th.test_workOnFeatureBranch(featureBranch)
    }
    
    void test_workOnFeatureBranchLocally(String featureBranch) {
        th.test_workOnFeatureBranchLocally(featureBranch)
    }
    
    void test_deliverFeature(String featureBranch, String remoteFeatureBranch) {
        th.test_deliverFeature(featureBranch, remoteFeatureBranch)
    }
    
    void test_discardFeature(String featureName, String featureBranch, String remoteFeatureBranch) {
        th.test_discardFeature(featureName, featureBranch, remoteFeatureBranch)
    }

    void test_createMaintenanceBranch(int maintainedVersion, String maintenanceBranch, String remoteMaintenanceBranch) {
        th.test_createMaintenanceBranch(maintainedVersion, maintenanceBranch, remoteMaintenanceBranch)
    }
    
    void test_createMaintenanceFeature(int v, String maintenanceBranch, String featureName, String featureBranch, String remoteFeatureBranch) {
        th.test_createMaintenanceFeature(v, maintenanceBranch, featureName, featureBranch, remoteFeatureBranch)
    }

    void test_goToMaintenanceFeature(int v, String featureName, String featureBranch) {
        th.test_goToMaintenanceFeature(v, featureName, featureBranch)
    }

    void test_workOnMaintenanceFeatureBranch(int v, String featureBranch) {
        th.test_workOnMaintenanceFeatureBranch(v, featureBranch)
    }

    void test_deliverMainenanceFeature(int v, String featureBranch, String remoteFeatureBranch) {
        th.test_deliverMainenanceFeature(v, featureBranch, remoteFeatureBranch)
    }

    void test_discardMaintenanceFeature(int v, String featureName, String featureBranch, String remoteFeatureBranch) {
        th.test_discardMaintenanceFeature(v, featureName, featureBranch, remoteFeatureBranch)
    }
}
