/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

abstract class BaseAction implements Runnable {

    protected final Configuration conf
    protected final GitRepo git

    BaseAction(Configuration conf, GitRepo git) {
        this.conf = conf
        this.git = git
    }

    final protected void checkOutBranch(String branchName) {
        def remoteBranchName = "${conf.centralGitRepository}/${branchName}"
        if (git.isLocalBranch(branchName)) {
            if (!git.isLocalBranchTrackingRemoteBranch(branchName, remoteBranchName)) {
                println "WARNING: Branch '${branchName}' exists but doesn't track '${remoteBranchName}'!"
            }
        } else {
            git.createLocalBranchTrackingRemote(branchName, remoteBranchName)
        }

        git.checkoutBranch(branchName)
    }

    final protected void fetchFromCentralAndPrune() {
        def initialRemoteBranches = git.remoteTrackingCentralBranches()

        git.fetchAllBranchesAndPrune()

        def remoteBranchesAfterPrune = git.remoteTrackingCentralBranches()
        def removedRemoteBranches = initialRemoteBranches.minus(remoteBranchesAfterPrune)

        if (removedRemoteBranches) {
            if (removedRemoteBranches.size() == 1) {
                println "Pruned remote-tracking branch:"
            } else {
                println "Pruned remote-tracking branches:"
            }
            removedRemoteBranches.each {
                println "  ${it}"
            }
        }
    }

    final protected void mustHaveRemoteReleaseBranch() {
        if (!git.isCentralBranch(conf.releaseBranch)) {
            throw new GradleException("Branch '${conf.releaseBranch}' not found in central repository!")
        }
    }

    final protected void repoMustNotHaveChanges() {
        if (git.changes()) {
            throw new GradleException('Cannot perform task while repo has changes!')
        }
    }
}
