/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@RunWith(Parameterized.class)
class IllegalCentralBranchesTest extends BaseTest {

    private static final PluginExt ext1 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'support',
            maintenanceIntegrationURLTriggers: [:]))

    private TestEnv env
    private GitRepo devGit
    private GitRepo integratorGit
    private TestHelper helper

    private final PluginExt ext

    private final String central
    private final String featurePrefix
    private final String releaseBranch

    private static final String featureName = 'drinkPotion'
    private final String featureBranch
    private final String remoteFeatureBranch

    private static final String pseudoFeatureName = 'healParty'
    private final String pseudoFeatureBranch
    private final List<String> localOnlyPseudoFeatureBranches
    private final List<String> pseudoFeatureBranches

    private final String preReleaseBranch
    private final String pseudoPreReleaseBranch

    private static final String someRandomBranch = 'upgradeArmor'
    private static final List<String> localNonFeatureBranches = (1..3).collect { i -> "${someRandomBranch}-$i" as String }

    private Exception exc

    @Parameterized.Parameters
    public static Collection pluginExtensions() {
        return [ext1, ext2]
    }

    public IllegalCentralBranchesTest(PluginExt ext) {
        this.ext = ext

        String pseudoFeaturePrefix
        switch (ext.root.workflowType) {
            case WorkflowType.CONTINUOUS.toString():
                this.central = ext.continuous.centralGitRepository

                this.featurePrefix = ext.continuous.featureBranchPrefix
                pseudoFeaturePrefix = 'Feature'

                this.preReleaseBranch = ext.continuous.preReleaseBranch
                this.pseudoPreReleaseBranch = 'prerelease'

                this.releaseBranch = ext.continuous.releaseBranch

                break
            case WorkflowType.VERSIONED.toString():
                this.central = ext.versioned.centralGitRepository

                this.featurePrefix = ext.versioned.featureBranchPrefix
                pseudoFeaturePrefix = 'TASK'

                this.preReleaseBranch = ext.versioned.preReleaseBranch
                this.pseudoPreReleaseBranch = 'PRE'

                this.releaseBranch = ext.versioned.mainBranch

                break
            default:
                assert false
        }

        this.featureBranch = featureBranch(featureName)
        this.remoteFeatureBranch = remoteFeatureBranch(featureName)

        this.pseudoFeatureBranch = "${pseudoFeaturePrefix}/${pseudoFeatureName}"
        this.localOnlyPseudoFeatureBranches = (1..3).collect { i -> "${pseudoFeatureBranch}-$i" as String }
        this.pseudoFeatureBranches = (4..5).collect { i -> "${pseudoFeatureBranch}-$i" as String }
    }

    private String featureBranch(String featureName) {
        return "${featurePrefix}/${featureName}"
    }

    private String remoteFeatureBranch(String featureName) {
        return "${central}/${featurePrefix}/${featureName}"
    }

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        localNonFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        localOnlyPseudoFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        (pseudoFeatureBranches + [pseudoPreReleaseBranch]).each { branch ->
            env.devRepo.branch.add(name: branch)
            pushBranchToRemote(env.devRepo, central, branch)
        }

        // Preconditions
        // ---------------------------------------------------------------------
        String prefix = pseudoFeatureBranch[0..featurePrefix.size()]

        assert prefix != featurePrefix
        assert prefix.toLowerCase() != featurePrefix.toLowerCase()

        // 1. [dev, central] several local-only pseudo-feature-branches exist
        //    (they have the feature prefix but with a different case)
        assert localOnlyPseudoFeatureBranches.every { it.startsWith(prefix) }

        assert branchesOf(env.devRepo).asList().containsAll(localOnlyPseudoFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev, central] several other pseudo-feature-branches exist
        assert pseudoFeatureBranches.every { it.startsWith(prefix) }

        assert branchesOf(env.devRepo).asList().containsAll(pseudoFeatureBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoFeatureBranches)

        // 3. [dev, central] a pseudo-pre-release-branch exists
        assert pseudoPreReleaseBranch != preReleaseBranch
        assert pseudoPreReleaseBranch.equalsIgnoreCase(preReleaseBranch)

        assert branchesOf(env.devRepo).asList().contains(pseudoPreReleaseBranch)
        assert branchesOf(env.centralRepo).asList().contains(pseudoPreReleaseBranch)

        // 4. [dev, central] several local-only non-feature branches exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localNonFeatureBranches }

        // 5. [dev, central] no other local branches exist except the release branch
        assert [releaseBranch] ==
            branchesOf(env.devRepo).asList().minus(
                localOnlyPseudoFeatureBranches + pseudoFeatureBranches + [pseudoPreReleaseBranch] + localNonFeatureBranches)
    }

    @After
    public void tearDown() {
        devGit.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onCreateFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onGoToFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onDeliverFeature() {
        env.devRepo.checkout(branch: featureBranch, createBranch: true)

        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "The checked-out branch '${releaseBranch}' is not a feature branch, it cannot be delivered!" == exc.message

        // 2. sync results are as expected
        checkSyncResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onDiscardFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onSyncWithCentral() {
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
        checkSyncResults()
    }

    private void checkResults() {
        // 1. the error message is the correct one
        assert "Illegal pseudo-feature branches found on central: ${pseudoFeatureBranches}!" == exc.message

        checkSyncResults()
    }

    private void checkSyncResults() {
        // 1. [dev] all the local-only pseudo-feature-branches disappeared
        assert ! branchesOf(env.devRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev, central] the other pseudo-feature-branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(pseudoFeatureBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoFeatureBranches)

        // 3. [dev, central] the pseudo-pre-release-branch still exists
        assert branchesOf(env.devRepo).asList().containsAll(pseudoPreReleaseBranch)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoPreReleaseBranch)

        // 4. [dev] the other local branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
    }
}
