package ro.amocanu.gradle.plugins.amworkflow.actions.debug

import static org.junit.Assert.*

import org.junit.Ignore
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

/**
 * 
 * Not a real test class, was used for debugging some SSH-related stuff.
 * 
 * (Kept it in case we need to do more debugging in the future.)
 *
 */
class DebugSSH {

    // a repository that uses SSH as the transport protocol
    private static final SSH_REPO = "D:\\Temp\\testssh"
    
    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))
    
    @Ignore
    @Test
    public void test() {      
        GitRepo repo = GrGitRepo.open(SSH_REPO, ext.versioned.centralGitRepository)
        DeveloperActions.newSyncWithCentralAction(ext, repo).run()
    }

}
