/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions

import java.nio.file.Paths

import static groovy.io.FileType.*

import java.nio.file.Files
import java.nio.file.Paths

import org.ajoberstar.grgit.Grgit

class TestEnv {
    final Grgit devRepo
    final Grgit devRepo2
    final Grgit integratorRepo
    final Grgit centralRepo

    private static final String TEMPLATE_REPO_ROOT = "src/test/resources/test-repos"
    private static final String TEST_REPO_ROOT = "src/test/resources/tmp"
    
    private static final String DEV_REPO = "dev-repo"
    private static final String INTEGRATOR_REPO = "integrator-repo"
    private static final String CENTRAL_REPO = "central-repo"
    
    private static Grgit makeTestRepo(String sourceRepoName, String destinationRepoName) {
        def sourcePath = Paths.get("${TEMPLATE_REPO_ROOT}/${sourceRepoName}")
        def destinationPath = Paths.get("${TEST_REPO_ROOT}/${destinationRepoName}")
        def sourceRoot = new File(sourcePath.toString())

        Files.createDirectory(destinationPath)
        
        sourceRoot.traverse(type: DIRECTORIES) { File file ->
            def newDir = Paths.get(gitify(file.toString().replace(sourcePath.toString(), destinationPath.toString())))
            Files.createDirectory(newDir)
        }
        
        sourceRoot.traverse(type: FILES) { File file ->
            def from = file.toPath()
            def to = Paths.get(gitify(file.toString().replace(sourcePath.toString(), destinationPath.toString())))
            Files.copy(from, to)
        }
        
        return Grgit.open(dir: destinationPath.toString())
    }

    private static Grgit makeTestRepo(String sourceRepoName) {
        return makeTestRepo(sourceRepoName, sourceRepoName)
    }

    private static String gitify(String path) {
        return path.contains('.notgit') ? path.replace('.notgit', '.git') : path
    }
    
    private static void deleteDir(String dir) {
        def dirPath = Paths.get(dir)
        def dirRoot = new File(dirPath.toString())
        dirRoot.deleteDir()
    }
    
    TestEnv() {
        deleteDir(TEST_REPO_ROOT)
        Files.createDirectory(Paths.get(TEST_REPO_ROOT))
        
        // central repo
        centralRepo = makeTestRepo(CENTRAL_REPO)
        assert GitTestUtil.branchesOf(centralRepo).any { 'master' == it }

        // dev repos
        devRepo = makeTestRepo(DEV_REPO, 'bob-repo')
        checkRepo(devRepo)

        devRepo2 = makeTestRepo(DEV_REPO, 'alice-repo')
        checkRepo(devRepo2)

        // integrator repo
        integratorRepo = makeTestRepo(INTEGRATOR_REPO)
        checkRepo(integratorRepo)
    }
    
    private static void checkRepo(Grgit repo) {
        assert repo.remote.list().size() == 1
        
        def centralForDev = repo.remote.list().first()
        assert centralForDev.name == 'origin'
        assert centralForDev.url == '../central-repo/'

        assert GitTestUtil.branchesOf(repo).any { 'master' == it }       
    }

    void tearDown() {
        integratorRepo.close()
        devRepo.close()
        devRepo2.close()
        centralRepo.close()
        
        deleteDir(TEST_REPO_ROOT)
    }
}
