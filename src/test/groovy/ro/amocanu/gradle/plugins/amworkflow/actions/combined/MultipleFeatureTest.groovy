/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.combined

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf

import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.junit.runners.Parameterized

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
class MultipleFeatureTest extends BaseTest {

    private static final PluginExt ext1 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'support',
            maintenanceIntegrationURLTriggers: [:]))

    private static TestEnv env
    private static GitRepo devGit
    private static GitRepo integratorGit
    private static TestHelper helper

    private final PluginExt ext

    private String central
    private String releaseBranch
    private String remoteReleaseBranch
    private String featurePrefix

    private static final String featureName1 = 'makecoffee'
    private String featureBranch1
    private String remoteFeatureBranch1

    private static final String featureName2 = 'maketea'
    private String featureBranch2
    private String remoteFeatureBranch2

    private static final String featureName3 = 'makecookies'
    private String featureBranch3
    private String remoteFeatureBranch3

    private static final String featureName4 = 'make/partyhats'
    private String featureBranch4
    private String remoteFeatureBranch4
    
    @Parameterized.Parameters
    public static Collection pluginExtensions() {
        return [ext1, ext2]
    }

    public MultipleFeatureTest(PluginExt ext) {
        this.ext = ext

        switch (ext.root.workflowType) {
            case WorkflowType.CONTINUOUS.toString():
                this.central = ext.continuous.centralGitRepository
                this.featurePrefix = ext.continuous.featureBranchPrefix
                this.releaseBranch = ext.continuous.releaseBranch
                break
            case WorkflowType.VERSIONED.toString():
                this.central = ext.versioned.centralGitRepository
                this.featurePrefix = ext.versioned.featureBranchPrefix
                this.releaseBranch = ext.versioned.mainBranch
                break
            default:
                assert false
        }

        this.remoteReleaseBranch = "${central}/${releaseBranch}"
        
        this.featureBranch1 = featureBranch(featureName1)
        this.featureBranch2 = featureBranch(featureName2)
        this.featureBranch3 = featureBranch(featureName3)
        this.featureBranch4 = featureBranch(featureName4)

        this.remoteFeatureBranch1 = remoteFeatureBranch(featureName1)
        this.remoteFeatureBranch2 = remoteFeatureBranch(featureName2)
        this.remoteFeatureBranch3 = remoteFeatureBranch(featureName3)
        this.remoteFeatureBranch4 = remoteFeatureBranch(featureName4)
    }

    private String featureBranch(String featureName) {
        return "${featurePrefix}/${featureName}"
    }

    private String remoteFeatureBranch(String featureName) {
        return "${central}/${featurePrefix}/${featureName}"
    }

    @Test
    public void step00_setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @Test
    public void step01_createFeatures() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
    }

    @Test
    public void step02_workOnAllFeatures() {
        helper.test_goToFeature(featureName1, featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)

        helper.test_goToFeature(featureName2, featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)

        helper.test_goToFeature(featureName3, featureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
    }

    @Test
    public void step03_deliverFeature3() {
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)
    }

    @Test
    public void step04_integrateFeature3() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. all feature branches exist on central
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }
        assert branchesOf(env.centralRepo).any { featureBranch3 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch3 == it }

        // 2. the yet undelivered features still exist on central
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }
    }

    @Test
    public void step05_goToFeature1() {
        helper.test_goToFeature(featureName1, featureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. previously integrated feature3 was now pruned on dev
        assert ! branchesOf(env.devRepo).any { featureBranch3 == it }
        assert ! branchesOf(env.devRepo).any { remoteFeatureBranch3 == it }
    }

    @Test
    public void step06_mergeReleaseBranchIntoFeature1() {
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        pushBranchToRemote(env.devRepo, central, featureBranch1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. on central, feature1 is now a descendant of the release branch
        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, featureBranch1),
            branchTip(env.centralRepo, releaseBranch))
    }

    @Test
    public void step07_deliverAndIntegrateFeature1() {
        // deliver and integrate feature2
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the yet undelivered feature2 still exist on central
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }
    }

    @Test
    public void step08_goToFeature2() {
        helper.test_goToFeature(featureName2, featureBranch2)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. previously delivered feature1 was now pruned on dev
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert ! branchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
    }

    @Test
    public void step09_moreWorkOnFeature2() {
        mergeIntoCurrentBranch(env.devRepo, remoteReleaseBranch)
        helper.test_workOnFeatureBranch(featureBranch2)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. on central, feature2 is now a descendant of the release branch
        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, featureBranch2),
            branchTip(env.centralRepo, releaseBranch))
    }

    @Test
    public void step10_deliverAndIntegrateFeature2() {
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
        helper.test_integrateFeature()
    }

    @Test
    public void step11_failAtCreatingFeatureWhileOnIntegratedFeatureBranch() {
        env.devRepo.checkout(branch: featureBranch2)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature2 exists and is checked out on dev, but it no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch2 == it }
        assert branchesOf(env.devRepo).any { featureBranch2 == it }
        assert isCheckedOut(env.devRepo, featureBranch2)

        // 2. the feature4 branch does not exist on dev or central repos
        assert ! branchesOf(env.devRepo).any { featureBranch4 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch4 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch4 == it }

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName4).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is correct
        assert "Feature name cannot contain the '/' character!" == e.getMessage()

        // 2. the release branch is now checked out
        assert isCheckedOut(env.devRepo, releaseBranch)

        // 3. the feature4 branch does not exist on dev or central repos
        assert ! branchesOf(env.devRepo).any { featureBranch4 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch4 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch4 == it }
    }

    @Test
    public void step99_tearDown() {
        integratorGit.close()
        devGit.close()
        env.tearDown()
        env = null
    }

    @AfterClass
    public static void tearDownOnError() {
        if (env) {
            integratorGit.close()
            devGit.close()
            env.tearDown()
        }
    }
}
