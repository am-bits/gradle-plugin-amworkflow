/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isFileInRepoOnBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import groovy.transform.PackageScope

import org.ajoberstar.grgit.Grgit

import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.actions.integrator.IntegratorActions
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

/**
 * This class contains self-contained test methods for the Actions classes.
 * 
 * These methods can be grouped together in various ways in order to create  
 * complex test scenarios.
 * 
 * Only the nominal cases of the tested classes are covered.
 */
class TestHelper {

    private final PluginExt ext
    private final Configuration conf    
    
    private final Grgit devRepo
    private final Grgit integratorRepo
    private final Grgit centralRepo

    // only use these as arguments for the tested methods
    private final GitRepo devGit
    private final GitRepo integratorGit

    TestHelper(TestEnv env, PluginExt ext, GitRepo devGit, GitRepo integratorGit) {
        this.devRepo = env.devRepo
        this.integratorRepo = env.integratorRepo
        this.centralRepo = env.centralRepo
        
        this.ext = ext
        this.conf = ConfigurationFactory.fromExtension(ext)
        this.devGit = devGit
        this.integratorGit = integratorGit 
    }

    @PackageScope
    TestHelper(Grgit devRepo, Grgit integratorRepo, Grgit centralRepo, PluginExt ext, GitRepo devGit, GitRepo integratorGit) {
        this.devRepo = devRepo
        this.integratorRepo = integratorRepo
        this.centralRepo = centralRepo
        
        this.ext = ext
        this.conf = ConfigurationFactory.fromExtension(ext)
        this.devGit = devGit
        this.integratorGit = integratorGit
    }

    void test_createFeature(String featureName, String featureBranch, String remoteFeatureBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the feature branch does not exist
        assert ! branchesOf(centralRepo).any { featureBranch == it }
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. create the feature 
        DeveloperActions.newCreateFeatureAction(ext, devGit, featureName).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature branch now exists on both dev or central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }
        
        // 2. the feature branch on dev tracks the corresponding branch on central
        assert isTracking(devRepo, featureBranch, remoteFeatureBranch)

        // 3. on dev, the feature branch is checked out
        assert isCheckedOut(devRepo, featureBranch)
        
        // 4. on central, the feature branch is the same as the release branch
        assert branchTip(centralRepo, conf.releaseBranch) == branchTip(centralRepo, featureBranch)
    }

    void test_goToFeature(String featureName, String featureBranch) {
        def remoteFeatureBranch = "${conf.centralGitRepository}/${featureBranch}"

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists on central
        assert branchesOf(centralRepo).any { featureBranch == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. go to feature
        DeveloperActions.newGoToFeatureAction(ext, devGit, featureName).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [dev] the feature branch was created at the same commit as the remote feature branch
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchTip(devRepo, remoteFeatureBranch) == branchTip(devRepo, featureBranch)
        
        // 2. [dev] the feature branch is tracking the remote feature branch
        assert isTracking(devRepo, featureBranch, remoteFeatureBranch)

        // 3. [dev] the feature branch is checked-out
        assert isCheckedOut(devRepo, featureBranch)
    }
    
    void test_workOnFeatureBranch(String featureBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch is checked out
        assert isCheckedOut(devRepo, featureBranch)
        
        // Test
        // ---------------------------------------------------------------------
        // 1. work on feature
        commitSomething(devRepo)
        pushBranchToRemote(devRepo, conf.centralGitRepository, featureBranch)

        // Check results
        // ---------------------------------------------------------------------       
        // 1. on central, the feature branch is a descendant of the release branch
        assert isDescendant(centralRepo,
            branchTip(centralRepo, featureBranch),
            branchTip(centralRepo, conf.releaseBranch))
    }
    
    void test_workOnFeatureBranchLocally(String featureBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch is checked out
        assert isCheckedOut(devRepo, featureBranch)
        
        // Test
        // ---------------------------------------------------------------------
        // 1. work on feature
        commitSomething(devRepo)
    }
    
    void test_deliverFeature(String featureBranch, String remoteFeatureBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch to deliver is checked out 
        assert isCheckedOut(devRepo, featureBranch)
                
        // 2. the pre-release branch does not exist on central or dev repos
        assert ! branchesOf(devRepo).any { conf.preReleaseBranch == it }
        assert ! remoteBranchesOf(devRepo).any { "${conf.centralGitRepository}/${conf.preReleaseBranch}" == it }
        assert ! branchesOf(centralRepo).any { conf.preReleaseBranch == it }
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. deliver feature
        DeveloperActions.newDeliverFeatureAction(ext, devGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature branch still exists on dev and central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }

        // 2. the pre-release branch exists on central, but not on dev  
        assert branchesOf(centralRepo).any { conf.preReleaseBranch == it }
        assert remoteBranchesOf(devRepo).any { "${conf.centralGitRepository}/${conf.preReleaseBranch}" == it }
        assert ! branchesOf(devRepo).any { conf.preReleaseBranch == it }

        // 3. on dev, the release branch is checked out
        assert isCheckedOut(devRepo, conf.releaseBranch)
        
        // 4. on central, the pre-release branch is positioned on the delivered feature 
        assert branchTip(centralRepo, featureBranch) == branchTip(centralRepo, conf.preReleaseBranch)
    }

    void test_integrateFeature() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch exists on the central repo
        assert branchesOf(centralRepo).any { conf.preReleaseBranch == it }
        
        // 2. on central, the commit to integrate is a descendant of the release branch
        def commitToIntegrate = branchTip(centralRepo, conf.preReleaseBranch)
        assert isDescendant(centralRepo, commitToIntegrate, branchTip(centralRepo, conf.releaseBranch))
   
        def tagsBeforeIntegration = tagsOf(centralRepo).asList()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        
        // 2. integrate
        IntegratorActions.newIntegrateFeatureAction(ext, integratorGit).run()
        
        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(integratorRepo).any { conf.preReleaseBranch == it }
        assert ! remoteBranchesOf(integratorRepo).any { "${conf.centralGitRepository}/${conf.preReleaseBranch}" == it }
        assert ! branchesOf(centralRepo).any { conf.preReleaseBranch == it }

        // 2. on integrator, the release branch is checked out
        assert isCheckedOut(integratorRepo, conf.releaseBranch)
        
        // 3. on central, the release branch advanced to the commit that was to be integrated
        assert commitToIntegrate == branchTip(centralRepo, conf.releaseBranch)

        // 4. on central, no new tags appeared 
        assert tagsBeforeIntegration == tagsOf(centralRepo).asList()
    }
    
    void test_discardFeature(String featureName, String featureBranch, String remoteFeatureBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists on dev and central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }

        // 2. the release branch exists in the central repo
        assert branchesOf(centralRepo).any { conf.releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. discard the feature
        DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature no longer exists on dev and central repos
        assert ! branchesOf(devRepo).any { featureBranch == it }
        assert ! remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert ! branchesOf(centralRepo).any { featureBranch == it }
        
        // 2. on dev, the release branch is now checked-out
        assert isCheckedOut(devRepo, conf.releaseBranch)
    }

    void test_continuousIntegrateFeature() {
        // prevent the creationf two release tags with the same timestamp
        Thread.sleep(1000)
        
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch exists on the central repo
        assert branchesOf(centralRepo).any { conf.preReleaseBranch == it }
        
        // 2. on central, the commit to integrate is a descendant of the release branch
        def commitToIntegrate = branchTip(centralRepo, conf.preReleaseBranch)
        assert isDescendant(centralRepo, commitToIntegrate, branchTip(centralRepo, conf.releaseBranch))

        def tagsBeforeIntegration = tagsOf(centralRepo).asList()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        
        // 2. integrate
        IntegratorActions.newContinuousIntegrateFeatureAction(ext, integratorGit).run()
        
        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(integratorRepo).any { conf.preReleaseBranch == it }
        assert ! remoteBranchesOf(integratorRepo).any { "${conf.centralGitRepository}/${conf.preReleaseBranch}" == it }
        assert ! branchesOf(centralRepo).any { conf.preReleaseBranch == it }

        // 2. on integrator, the release branch is checked out
        assert isCheckedOut(integratorRepo, conf.releaseBranch)
        
        // 3. on central, the release branch advanced to the commit that was to be integrated
        assert commitToIntegrate == branchTip(centralRepo, conf.releaseBranch)

        // 4. central has a new tag with the appropriate prefix
        def tagsAfterIntegration = tagsOf(centralRepo).asList().minus(tagsBeforeIntegration)
        assert 1 == tagsAfterIntegration.size()
        assert conf.isReleaseName(tagsAfterIntegration.first())
    }

    void test_createFirstVersion() {
        createAndCommitNewFile(integratorRepo, conf.versioned.versionFile, "version='0.0.0'")
        pushBranchToRemote(integratorRepo, conf.centralGitRepository, conf.releaseBranch)
        test_createNewVersion('minor', '0.1.0')
    }

    void test_createNewVersion(String versionType, String expectedVersion) {
        def mainBranch = ext.versioned.mainBranch
        def remoteMainBranch = "${ext.versioned.centralGitRepository}/${mainBranch}"
        def expectedVersionTag = ext.versioned.versionPrefix + expectedVersion
        def expectedCommitMessage = ext.versioned.bumpVersionCommitMessage(expectedVersion)
        def versionFile = ext.versioned.versionFile

        def randomInitialBranch = 'randomBranch-' + System.currentTimeMillis()

        // Setup
        // ---------------------------------------------------------------------
        // 1. delay to prevent random failure; see log of commit 2cf06e4 for an explanation
        Thread.sleep(1000)
        
        // 2. [integrator] create and checkout a random branch
        integratorRepo.checkout(branch: randomInitialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the version file exists on the main branch
        //assert isFileInRepoOnBranch(centralRepo, versionFile, mainBranch) // This doesn't work in bare repositories!

        // 2. [integrator] repo has no staged changes
        assert stagingAreaOf(integratorRepo).asList().isEmpty()

        // 3. [integrator] the version tag does not exist
        def integratorTagsBeforeTest = tagsOf(integratorRepo).asList()
        assert ! integratorTagsBeforeTest.any { expectedVersionTag == it }

        // 4. [central] the version tag does not exist
        def centralTagsBeforeTest = tagsOf(centralRepo).asList()
        assert ! centralTagsBeforeTest.any { expectedVersionTag == it }

        // 5. [integrator] a random branch is checked out
        assert isCheckedOut(integratorRepo, randomInitialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. create the new version
        IntegratorActions.newCreateNewVersionAction(ext, integratorGit, versionType).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator] the version tag was created
        def newIntegratorTags = tagsOf(integratorRepo).asList().minus(integratorTagsBeforeTest)

        assert 1 == newIntegratorTags.size()
        assert expectedVersionTag == newIntegratorTags.first()

        // 2. [integrator] the main branch is checked out
        assert isCheckedOut(integratorRepo, mainBranch)
            
        // 3. [integrator] the commit with the version tag contains the version file
        def versionFileContains = fileContentsOnTag(integratorRepo, versionFile, expectedVersionTag)
        assert versionFileContains != null

        // 4. [integrator] the version file in this commit contains the expected version number
        assert versionFileContains.any { "version='${expectedVersion}'" == it }

        // 5. [integrator] the commit with the version tag has the correct commit message
        assert expectedCommitMessage == integratorRepo.log(maxCommits: 1).fullMessage.first()

        // 6. [integrator] the version tag is at the tip of the main branch
        assert commitIdOfTag(integratorRepo, expectedVersionTag) == branchTip(integratorRepo, mainBranch).abbreviatedId

        // 7. [integrator, central] the main branch is synchronized correctly 
        assert remoteBranchesOf(integratorRepo).any { remoteMainBranch == it }
        assert remoteBranchTip(integratorRepo, remoteMainBranch) == branchTip(integratorRepo, mainBranch)

        assert branchesOf(centralRepo).any { mainBranch == it }
        assert remoteBranchTip(integratorRepo, remoteMainBranch) == branchTip(centralRepo, mainBranch)

        // 8. [central] the version tag was created
        def newCentralTags = tagsOf(centralRepo).asList().minus(centralTagsBeforeTest)

        assert 1 == newCentralTags.size()
        assert expectedVersionTag == newCentralTags.first()

        // Clean-up
        // ---------------------------------------------------------------------      
        // 1. [integrator] remove the random branch
        integratorRepo.branch.remove(names: [randomInitialBranch], force: true)
    }

    void test_createMaintenanceBranch(int maintainedVersion, String maintenanceBranch, String remoteMaintenanceBranch) {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the maintenance branch does not exist on dev or central repos
        assert ! branchesOf(devRepo).any { maintenanceBranch == it }
        assert ! remoteBranchesOf(devRepo).any { remoteMaintenanceBranch == it }
        assert ! branchesOf(centralRepo).any { maintenanceBranch == it }
        
        // 2. on central, there is at least one tag with its major segment equal to the maintained version
        def tagsOfMaintainedVersion = tagsOf(centralRepo).findAll { tag -> 
            conf.isReleaseName(tag) &&
            conf.releaseIdFromReleaseName(tag).startsWith("${maintainedVersion}.") }
        assert tagsOfMaintainedVersion.size() >= 1

        // 3. on central, there is at least one tag with its major segment equal to the next version
        def nextVersion = maintainedVersion + 1
        def tagsOfNextVersion = tagsOf(centralRepo).findAll { 
            tag -> conf.isReleaseName(tag) &&
            conf.releaseIdFromReleaseName(tag).startsWith("${nextVersion}.") }
        assert tagsOfNextVersion.size() >= 1

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. create the maintenance branch
        DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, maintainedVersion as String).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the maintenance branch now exists on both dev or central repos
        assert branchesOf(devRepo).any { maintenanceBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteMaintenanceBranch == it }
        assert branchesOf(centralRepo).any { maintenanceBranch == it }
        
        // 2. the maintenance branch on dev tracks the corresponding branch on central
        assert isTracking(devRepo, maintenanceBranch, remoteMaintenanceBranch)

        // 3. on central, the maintenance branch is the same as highest tag of the maintained version
        def highestTag = tagsOfMaintainedVersion.sort().last()
        assert commitIdOfTag(centralRepo, highestTag) == branchTip(centralRepo, maintenanceBranch).abbreviatedId
    }
    
    void test_createMaintenanceFeature(int v, String maintenanceBranch, String featureName, String featureBranch, String remoteFeatureBranch) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the maintenance branch exists on central repo
        assert branchesOf(centralRepo).any { maintenanceBranch == it }
        
        // 2. the feature branch does not exist on dev or central repos
        assert ! branchesOf(devRepo).any { featureBranch == it }
        assert ! remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert ! branchesOf(centralRepo).any { featureBranch == it }
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. create the feature
        DeveloperActions.newCreateMaintenanceFeatureAction(ext, devGit, featureName, v as String).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature branch now exists on both dev or central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }
        
        // 2. the feature branch on dev tracks the corresponding branch on central
        assert isTracking(devRepo, featureBranch, remoteFeatureBranch)

        // 3. on dev, the feature branch is checked out
        assert isCheckedOut(devRepo, featureBranch)
        
        // 4. on central, the feature branch is the same as the release branch
        assert branchTip(centralRepo, conf_v.releaseBranch) == branchTip(centralRepo, featureBranch)
    }

    void test_goToMaintenanceFeature(int v, String featureName, String featureBranch) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. go to feature
        DeveloperActions.newGoToMaintenanceFeatureAction(ext, devGit, featureName, v as String).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. on dev, the feature branch is now checked out
        assert isCheckedOut(devRepo, featureBranch)
    }

    void test_workOnMaintenanceFeatureBranch(int v, String featureBranch) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch is checked out
        assert isCheckedOut(devRepo, featureBranch)
        
        // Test
        // ---------------------------------------------------------------------
        // 1. work on feature
        commitSomething(devRepo)
        pushBranchToRemote(devRepo, conf_v.centralGitRepository, featureBranch)

        // Check results
        // ---------------------------------------------------------------------
        // 1. on central, the feature branch is a descendant of the release branch
        assert isDescendant(centralRepo,
            branchTip(centralRepo, featureBranch),
            branchTip(centralRepo, conf_v.releaseBranch))
    }

    void test_deliverMainenanceFeature(int v, String featureBranch, String remoteFeatureBranch) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch to deliver is checked out
        assert isCheckedOut(devRepo, featureBranch)
                
        // 2. the pre-release branch does not exist on central or dev repos
        assert ! branchesOf(devRepo).any { conf_v.preReleaseBranch == it }
        assert ! remoteBranchesOf(devRepo).any { "${conf_v.centralGitRepository}/${conf_v.preReleaseBranch}" == it }
        assert ! branchesOf(centralRepo).any { conf_v.preReleaseBranch == it }
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. deliver feature
        DeveloperActions.newDeliverFeatureAction(ext, devGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature branch still exists on dev and central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }

        // 2. the pre-release branch exists on central, but not on dev
        assert branchesOf(centralRepo).any { conf_v.preReleaseBranch == it }
        assert remoteBranchesOf(devRepo).any { "${conf_v.centralGitRepository}/${conf_v.preReleaseBranch}" == it }
        assert ! branchesOf(devRepo).any { conf_v.preReleaseBranch == it }

        // 3. on dev, the release branch is checked out
        assert isCheckedOut(devRepo, conf_v.releaseBranch)
        
        // 4. on central, the pre-release branch is positioned on the delivered feature
        assert branchTip(centralRepo, featureBranch) == branchTip(centralRepo, conf_v.preReleaseBranch)
    }
    
    void test_integrateMaintenenceFeature(int v) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch exists on the central repo
        assert branchesOf(centralRepo).any { conf_v.preReleaseBranch == it }
        
        // 2. on central, the commit to integrate is a descendant of the release branch
        def commitToIntegrate = branchTip(centralRepo, conf_v.preReleaseBranch)
        assert isDescendant(centralRepo, commitToIntegrate, branchTip(centralRepo, conf_v.releaseBranch))
   
        def tagsBeforeIntegration = tagsOf(centralRepo).asList()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        IntegratorActions.newPrepareMaintenanceFeatureIntegrationAction(ext, integratorGit, v as String).run()
        
        // 2. integrate
        IntegratorActions.newIntegrateMaintenanceFeatureAction(ext, integratorGit, v as String).run()
        
        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(integratorRepo).any { conf_v.preReleaseBranch == it }
        assert ! remoteBranchesOf(integratorRepo).any { "${conf_v.centralGitRepository}/${conf_v.preReleaseBranch}" == it }
        assert ! branchesOf(centralRepo).any { conf_v.preReleaseBranch == it }

        // 2. on integrator, the release branch is checked out
        assert isCheckedOut(integratorRepo, conf_v.releaseBranch)
        
        // 3. on central, the release branch advanced to the commit that was to be integrated
        assert commitToIntegrate == branchTip(centralRepo, conf_v.releaseBranch)

        // 4. on central, no new tags appeared
        assert tagsBeforeIntegration == tagsOf(centralRepo).asList()
    }

    void test_discardMaintenanceFeature(int v, String featureName, String featureBranch, String remoteFeatureBranch) {
        def conf_v = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, v as String)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists on dev and central repos
        assert branchesOf(devRepo).any { featureBranch == it }
        assert remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert branchesOf(centralRepo).any { featureBranch == it }

        // 2. the release branch exists in the central repo
        assert branchesOf(centralRepo).any { conf_v.releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. discard the feature
        DeveloperActions.newDiscardMaintenanceFeatureAction(ext, devGit, featureName, v as String).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature no longer exists on dev and central repos
        assert ! branchesOf(devRepo).any { featureBranch == it }
        assert ! remoteBranchesOf(devRepo).any { remoteFeatureBranch == it }
        assert ! branchesOf(centralRepo).any { featureBranch == it }
        
        // 2. on dev, the release branch is now checked-out
        assert isCheckedOut(devRepo, conf_v.releaseBranch)
    }

    void test_createNewMaintenanceVersion(int v, String versionType, String expectedVersion) {
        def mainBranch = ext.versioned.mainBranch
        def maintenanceBranch = ext.versioned.maintenanceBranchPrefix + '_' + ext.versioned.versionPrefix + v as String
        def remoteMaintenanceBranch = "${ext.versioned.centralGitRepository}/${maintenanceBranch}"
        def expectedVersionTag = ext.versioned.versionPrefix + expectedVersion
        def expectedCommitMessage = ext.versioned.bumpVersionCommitMessage(expectedVersion)
        def versionFile = ext.versioned.versionFile

        def randomInitialBranch = 'randomBranch-' + System.currentTimeMillis()

        // Setup
        // ---------------------------------------------------------------------
        // 1. delay to prevent random failure; see log of commit 2cf06e4 for an explanation
        Thread.sleep(1000)
        
        // 2. [integrator] create and checkout a random branch
        integratorRepo.checkout(branch: randomInitialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the version file exists on the maintenance branch
        assert isFileInRepoOnBranch(integratorRepo, versionFile, maintenanceBranch)

        // 2. [integrator] repo has no staged changes
        assert stagingAreaOf(integratorRepo).asList().isEmpty()

        // 3. [integrator] the version tag does not exist
        def integratorTagsBeforeTest = tagsOf(integratorRepo).asList()
        assert ! integratorTagsBeforeTest.any { expectedVersionTag == it }

        // 4. [central] the version tag does not exist
        def centralTagsBeforeTest = tagsOf(centralRepo).asList()
        assert ! centralTagsBeforeTest.any { expectedVersionTag == it }

        // 5. [integrator] a random branch is checked out
        assert isCheckedOut(integratorRepo, randomInitialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. create the new version
        IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v as String, versionType).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator] the version tag was created
        def newIntegratorTags = tagsOf(integratorRepo).asList().minus(integratorTagsBeforeTest)

        assert 1 == newIntegratorTags.size()
        assert expectedVersionTag == newIntegratorTags.first()

        // 2. [integrator] the maintenance branch is checked out
        assert isCheckedOut(integratorRepo, maintenanceBranch)
            
        // 3. [integrator] the commit with the version tag contains the version file
        def versionFileContents = fileContentsOnTag(integratorRepo, versionFile, expectedVersionTag)
        assert versionFileContents != null

        // 4. [integrator] the version file in this commit contains the expected version number
        assert versionFileContents.any { "version='${expectedVersion}'" == it }

        // 5. [integrator] the commit with the version tag has the correct commit message
        assert expectedCommitMessage == integratorRepo.log(maxCommits: 1).fullMessage.first()

        // 6. [integrator] the version tag is at the tip of the maintenance branch
        assert commitIdOfTag(integratorRepo, expectedVersionTag) == branchTip(integratorRepo, maintenanceBranch).abbreviatedId

        // 7. [integrator, central] the maintenance branch is synchronized correctly 
        assert remoteBranchesOf(integratorRepo).any { remoteMaintenanceBranch == it }
        assert remoteBranchTip(integratorRepo, remoteMaintenanceBranch) == branchTip(integratorRepo, maintenanceBranch)

        assert branchesOf(centralRepo).any { maintenanceBranch == it }
        assert remoteBranchTip(integratorRepo, remoteMaintenanceBranch) == branchTip(centralRepo, maintenanceBranch)

        // 8. [central] the version tag was created
        def newCentralTags = tagsOf(centralRepo).asList().minus(centralTagsBeforeTest)

        assert 1 == newCentralTags.size()
        assert expectedVersionTag == newCentralTags.first()

        // Clean-up
        // ---------------------------------------------------------------------
        // 1. [integrator] checkout the main branch 
        integratorRepo.checkout(branch: mainBranch)
        
        // 2. [integrator] remove the random branch
        integratorRepo.branch.remove(names: [randomInitialBranch], force: true)
    }
}
