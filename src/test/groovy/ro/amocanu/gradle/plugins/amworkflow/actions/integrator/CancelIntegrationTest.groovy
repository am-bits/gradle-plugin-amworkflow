/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class CancelIntegrationTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'water-the-cactus'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String someRandomBranch = 'feed-the-cat'

    private static final String releaseBranch = ext.continuous.releaseBranch
    private static final String preReleaseBranch = ext.continuous.preReleaseBranch
    private static final String remotePreReleaseBranch = "${ext.continuous.centralGitRepository}/${preReleaseBranch}"

    private static final String pseudoPreReleaseBranch = 'prerelease'

    private TestEnv env

    private GitRepo devGit
    private GitRepo integratorGit

    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        integratorGit.close()
        devGit.close()

        env.tearDown()
    }

    @Test
    void test_cancelIntegration_whenAFeatureIsDelivered() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def releaseBranchBeforeTest_integrator = branchTip(env.integratorRepo, releaseBranch)
        def releaseBranchBeforeTest_central = branchTip(env.centralRepo, releaseBranch)
        
        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 3. the release branch is the same as before the test
        assert releaseBranchBeforeTest_integrator == branchTip(env.integratorRepo, releaseBranch)
        assert releaseBranchBeforeTest_central == branchTip(env.centralRepo, releaseBranch)
    }

    @Test
    public void test_cancelIntegration_whenNoFeatureIsDelivered() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. cancel integration
        def e = shouldFail {
            IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'No features delivered for release found in the central repository!' == e.getMessage()

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_cancelIntegration_whenNoReleaseBranchOnCentral_andRandomBranchIsCheckedOut() {
        def ext2 = new PluginExt(
            root: new RootExtension(
            workflowType: ext.root.workflowType),
            continuous: new ContinuousExtension(
            centralGitRepository: ext.continuous.centralGitRepository,
            featureBranchPrefix: ext.continuous.featureBranchPrefix,
            preReleaseBranch: ext.continuous.preReleaseBranch,
            releaseBranch: 'release',
            releaseTagPrefix: ext.continuous.releaseTagPrefix,
            integrationURLTrigger: ext.continuous.integrationURLTrigger,
            ciUsername: ext.continuous.ciUsername,
            ciPassword: ext.continuous.ciPassword))

        def releaseBranch2 = ext2.continuous.releaseBranch
        def preReleaseBranch2 = ext2.continuous.preReleaseBranch
        def remoteReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${releaseBranch2}"
        def remotePreReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${preReleaseBranch2}"

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. cancel integration
        IntegratorActions.newCancelIntegrationAction(ext2, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the pre-release branch was removed on the central repo
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch2 == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch2 == it }

        // 2. on integrator repo, the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_cancelIntegration_whenNoReleaseBranchOnCentral_andPreReleaseBranchIsCheckedOut() {
        def ext2 = new PluginExt(
            root: new RootExtension(
            workflowType: ext.root.workflowType),
            continuous: new ContinuousExtension(
            centralGitRepository: ext.continuous.centralGitRepository,
            featureBranchPrefix: ext.continuous.featureBranchPrefix,
            preReleaseBranch: ext.continuous.preReleaseBranch,
            releaseBranch: 'release',
            releaseTagPrefix: ext.continuous.releaseTagPrefix,
            integrationURLTrigger: ext.continuous.integrationURLTrigger,
            ciUsername: ext.continuous.ciUsername,
            ciPassword: ext.continuous.ciPassword))

        def releaseBranch2 = ext2.continuous.releaseBranch
        def preReleaseBranch2 = ext2.continuous.preReleaseBranch
        def remoteReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${releaseBranch2}"
        def remotePreReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${preReleaseBranch2}"

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }

        // 3. on integrator repo, the pre-release branch is checked-out
        env.integratorRepo.checkout(branch: preReleaseBranch2, createBranch: true)

        assert isCheckedOut(env.integratorRepo, preReleaseBranch2)

        // Test
        // ---------------------------------------------------------------------
        // 1. cancel integration
        def e = shouldFail {
            IntegratorActions.newCancelIntegrationAction(ext2, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Branch '${releaseBranch2}' not found in central repository!" == e.getMessage()

        // 2. the pre-release branch continues to exist on the central repo
        assert branchesOf(env.centralRepo).any { preReleaseBranch2 == it }
        assert remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch2 == it }

        // 3. on integrator repo, the preRelease branch is still checked out
        assert isCheckedOut(env.integratorRepo, preReleaseBranch2)
    }

    @Test
    void test_cancelIntegration_whenLocalPreReleaseBranch_butNoLocalReleaseBranch_andRandomBranchIsCheckedOut() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the integrator repo has the local pre-release branch
        env.integratorRepo.branch.add(name: preReleaseBranch)
        assert branchesOf(env.integratorRepo).asList().contains(preReleaseBranch)

        // 3. the integrator repo has a random branch checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 4. the integrator repo doesn't have local release branch
        env.integratorRepo.branch.remove(names: [releaseBranch])
        assert ! branchesOf(env.integratorRepo).asList().contains(releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        def releaseBranchBeforeTest_central = branchTip(env.centralRepo, releaseBranch)

        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
        
        // 3. on central repo, the release branch is the same as before the test
        assert releaseBranchBeforeTest_central == branchTip(env.centralRepo, releaseBranch)
    }

    @Test
    void test_cancelIntegration_whenLocalPreReleaseBranch_butNoLocalReleaseBranch_andPreReleaseBranchIsCheckedOut() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the integrator repo has a random branch
        env.integratorRepo.checkout(branch: preReleaseBranch, createBranch: true)
        assert isCheckedOut(env.integratorRepo, preReleaseBranch)

        // 3. the integrator repo doesn't have local release branch
        env.integratorRepo.branch.remove(names: [releaseBranch])
        assert ! branchesOf(env.integratorRepo).asList().contains(releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        def releaseBranchBeforeTest_central = branchTip(env.centralRepo, releaseBranch)

        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, the release branch exists and is checked out
        assert branchesOf(env.integratorRepo).asList().contains(releaseBranch)
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        
        // 3. on central repo, the release branch is the same as before the test
        assert releaseBranchBeforeTest_central == branchTip(env.centralRepo, releaseBranch)
    }

    @Test
    void test_cancelIntegration_whenRepoHasChanges_butPreReleaseBranchIsNotCheckedOut() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 3. integrator repo has unstaged changes
        createEmptyFileInRepo(env.integratorRepo)
        assert ! workingDirectoryOf(env.integratorRepo).asList().isEmpty()
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        def releaseBranchBeforeTest_integrator = branchTip(env.integratorRepo, releaseBranch)
        def releaseBranchBeforeTest_central = branchTip(env.centralRepo, releaseBranch)

        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 3. the release branch is the same as before the test
        assert releaseBranchBeforeTest_integrator == branchTip(env.integratorRepo, releaseBranch)
        assert releaseBranchBeforeTest_central == branchTip(env.centralRepo, releaseBranch)
    }

    @Test
    void test_cancelIntegration_whenRepoHasChanges_andPreReleaseBranchIsCheckedOut() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, the pre-release branch is checked-out
        def initialBranch = preReleaseBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 3. integrator repo has unstaged changes
        createEmptyFileInRepo(env.integratorRepo)
        assert ! workingDirectoryOf(env.integratorRepo).asList().isEmpty()
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        // 1. cancel integration
        def e = shouldFail {
            IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot cancel integration because you are on the pre-release branch and there are changes!' == e.getMessage()

        // 2. the pre-release branch continues to exist on integrator and central repos
        assert branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    void test_cancelIntegration_whenRepoHasNoChanges_andPreReleaseBranchIsCheckedOut() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, the pre-release branch is checked-out
        def initialBranch = preReleaseBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 3. integrator repo does not have changes
        assert workingDirectoryOf(env.integratorRepo).asList().isEmpty()
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        def releaseBranchBeforeTest_integrator = branchTip(env.integratorRepo, releaseBranch)
        def releaseBranchBeforeTest_central = branchTip(env.centralRepo, releaseBranch)

        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 3. the release branch is the same as before the test
        assert releaseBranchBeforeTest_integrator == branchTip(env.integratorRepo, releaseBranch)
        assert releaseBranchBeforeTest_central == branchTip(env.centralRepo, releaseBranch)
    }

    @Test
    public void test_cancelIntegration_whenPreReleaseBranchExistsWithADifferentCase() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create and push the pseudo-pre-release branch
        env.devRepo.branch.add(name: pseudoPreReleaseBranch)
        pushBranchToRemote(env.devRepo, ext.continuous.centralGitRepository, pseudoPreReleaseBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists with a different case
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        assert preReleaseBranch != pseudoPreReleaseBranch
        assert preReleaseBranch.equalsIgnoreCase(pseudoPreReleaseBranch)

        // 2. [integrator] a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. [integrator] cancel integration
        def e = shouldFail {
            IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'No features delivered for release found in the central repository!' == e.getMessage()

        // 2. [integrator, central] the pre-release branch still does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. [central] the pseudo-pre-release branch still exists
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }
        
        // 4. [integrator] the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }
}
