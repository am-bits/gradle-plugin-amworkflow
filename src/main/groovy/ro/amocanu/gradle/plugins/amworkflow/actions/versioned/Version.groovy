/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.versioned

import groovy.transform.Immutable
import groovy.transform.Sortable

@Immutable
@Sortable(includes=['major','minor','patch'])
class Version {

    static final VERSION_FORMAT = '^(0|[1-9][0-9]*).(0|[1-9][0-9]*).(0|[1-9][0-9]*)$'

    int major
    int minor
    int patch

    static final makeNewMajorFrom = { Version version -> 
        new Version(version.major + 1, 0, 0) }
    
    static final makeNewMinorFrom = { Version version -> 
        new Version(version.major, version.minor + 1, 0) 
    }
    
    static final makeNewPatchFrom = { Version version -> 
        new Version(version.major, version.minor, version.patch + 1) 
    }

    static final Version fromString(String versionString) {
        assert isVersionString(versionString)

        def versionTokens = versionString.tokenize('.')
        assert versionTokens.size() == 3

        def newVersion = new Version(
            major: versionTokens.get(0) as int,
            minor: versionTokens.get(1) as int,
            patch: versionTokens.get(2) as int)

        return newVersion
    }
    
    @Override
    String toString() {
        def versionString = "$major.$minor.$patch"
        assert isVersionString(versionString)

        return versionString
    }

    static isVersionString(String versionString) { 
        return versionString ==~ /$VERSION_FORMAT/ 
    }
    
    boolean isConsecutiveTo(Version version) {
        def isConsecutive = 
            (version == null) || // any Version is consecutive to null 
            this == makeNewMajorFrom(version) ||
            this == makeNewMinorFrom(version) ||
            this == makeNewPatchFrom(version)
            
        return isConsecutive
    }
}
