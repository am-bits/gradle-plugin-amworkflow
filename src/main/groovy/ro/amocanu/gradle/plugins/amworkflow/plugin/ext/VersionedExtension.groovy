/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.plugin.ext
import groovy.transform.Canonical

@Canonical
class VersionedExtension {
    String centralGitRepository = 'origin'
    String gitUsername = ''
    String gitPassword = ''
    String featureBranchPrefix = 'feature'
    String preReleaseBranch = 'preRelease'
    String mainBranch = 'master'
    String versionFile = 'version.properties'
    String versionPrefix = 'v'
    String maintenanceBranchPrefix = 'maintenance'
    Closure<String> bumpVersionCommitMessage = { version -> "Bump version number to $version" }
    String mainIntegrationURLTrigger = ''
    Map<String,String> maintenanceIntegrationURLTriggers = [:]
    String ciUsername = ''
    String ciPassword = ''
}
