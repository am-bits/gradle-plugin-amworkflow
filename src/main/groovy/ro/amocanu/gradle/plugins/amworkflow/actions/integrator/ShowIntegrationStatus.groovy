/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope
import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
class ShowIntegrationStatus extends BaseAction {

    ShowIntegrationStatus (Configuration conf, GitRepo git) {
        super(conf, git)
    }

    @Override
    void run() {
        fetchFromCentralAndPrune()

        def remotePreReleaseBranch = "${conf.centralGitRepository}/${conf.preReleaseBranch}"
        if (git.isCentralBranch(conf.preReleaseBranch)) {
            def commitId = git.commitIdOfRemoteBranch(remotePreReleaseBranch)
            println "Found a feature delivered for integration with commit id ${commitId}"
        } else {
            println 'No feature delivered for release'

            def remotePseudoPreReleaseBranch = git.remoteTrackingCentralBranches().find { remotePreReleaseBranch.equalsIgnoreCase(it) }
            if (remotePseudoPreReleaseBranch) {
                def commitId = git.commitIdOfRemoteBranch(remotePseudoPreReleaseBranch)
                println "WARNING: Branch '${remotePseudoPreReleaseBranch}' on commit '${commitId}' shadows the pre-release branch! No feature can be delivered unless branch '${remotePseudoPreReleaseBranch}' is removed."
            }
        }
    }
}
