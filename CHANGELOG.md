# Change Log
All notable changes to this project will be documented in this file.

## [1.1] - 2016-05-26
### Added
- Add new developer task 'syncWithCentral'

### Changed
- All developer tasks now depend on 'syncWithCentral'
- Consolidate messages displayed at console

### Fixed
- Fix NullPointerException when running 'deliverFeature' on a feature branch already delivered (by another developer) and integrated
- Fail tasks explicitly when detecting branches/tags that differ by case alone from the amworkflow branches/tags
- Fix unpredictable behaviour in case of branch/tag ambiguity (e.g. when both the branch 'preRelease' and the tag 'preRelease' exist)
- Fix triggering of maintenance integration job by URL

## [1.0] - 2016-05-14
- The first version
