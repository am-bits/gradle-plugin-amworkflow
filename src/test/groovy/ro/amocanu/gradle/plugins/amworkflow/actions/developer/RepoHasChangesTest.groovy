/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class RepoHasChangesTest extends BaseTest {

    private static final ERROR_MESSAGE__REPO_HAS_CHANGES = 'Cannot perform task while repo has changes!'

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'use_the_Force'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = 'blow_out_the_Death_Star'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String someRandomBranch = 'do_or_do_not'

    private static final String releaseBranch = ext.continuous.releaseBranch

    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, null)
    }

    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createFeature_whileOnRandomBranch_withDirtyAndConflictualWorkingDirectory() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a random branch, descendant of the release branch, is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(
            branch: initialBranch,
            startPoint: releaseBranch,
            createBranch: true)

        // 2. there are changes in the working directory conflicting with release branch
        preconditions_workingDirectoryConflictsWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    @Test
    public void test_createFeature_whileOnAnotherFeatureBranch_withDirtyButNonConflictualStagingArea() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. another feature branch is checked-out
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_goToFeature(featureName2, featureBranch2)

        def initialBranch = featureBranch2

        // 2. there are changes in the staging area, but they are not conflicting with the release branch
        preconditions_stagingAreaIsDirty_butDoesNotConflictWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    @Test
    void test_goToFeature_whileOnReleaseBranch_withDirtyButNonConflictualWorkingDirectory() {
        // 1. feature1 exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. the release branch is checked-out
        def initialBranch = releaseBranch
        env.devRepo.checkout(branch: initialBranch)

        // 3. there are changes in the working directory, but they are not conflicting with the release branch
        preconditions_workingDirectoryIsDirty_butDoesNotConflictWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    @Test
    public void test_goToFeature_whileOnRandomBranch_withDirtyAndConflictualStagingArea() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. a random branch, descendant of the release branch, is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(
            branch: initialBranch,
            startPoint: releaseBranch,
            createBranch: true)

        // 3. there are changes in the staging area conflicting with release branch
        preconditions_stagingAreaConflictsWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    @Test
    public void test_deliverFeature_withDirtyAndConflictualWorkingDirectory() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists and is checked out
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        def initialBranch = featureBranch1

        // 2. there are changes in the working directory conflicting with release branch
        preconditions_workingDirectoryConflictsWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    @Test
    public void test_discardFeature_withDirtyAndConflictualStagingArea() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists and is checked out
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        def initialBranch = featureBranch1

        // 2. there are changes in the staging area conflicting with the release branch
        preconditions_stagingAreaConflictsWithReleaseBranch(env.devRepo, initialBranch)

        def workingDirectoryBeforeTest = workingDirectoryOf(env.devRepo)
        def stagingAreaBeforeTest = stagingAreaOf(env.devRepo)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().is(ERROR_MESSAGE__REPO_HAS_CHANGES)

        // 2. the dev repository state is the same as before the test
        results_repoIsUnchangedAfterTest(env.devRepo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest)
    }

    private static void preconditions_workingDirectoryConflictsWithReleaseBranch(repo, initialBranch) {
        // we work on initialBranch
        assert isCheckedOut(repo, initialBranch)

        // ... which is the same as the release branch, in the beginning
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // commit a file
        def someFile = 'readme.txt'
        createAndCommitNewFile(repo, someFile, 'A long time ago in a galaxy far, far away...')

        // ... then change it
        changeFile(repo, someFile, 'It is a period of civil war.')

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. initialBranch is a descendant of the release branch
        assert isDescendant(repo,
            branchTip(repo, initialBranch),
            branchTip(repo, releaseBranch))

        // 2. the working directory contains some changes
        assert [someFile] == workingDirectoryOf(repo).asList()

        // 3. the staging area is clean
        assert stagingAreaOf(repo).asList().isEmpty()
    }

    private static void preconditions_stagingAreaConflictsWithReleaseBranch(repo, initialBranch) {
        // we work on initialBranch
        assert isCheckedOut(repo, initialBranch)

        // ... which is the same as the release branch, in the beginning
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // commit a file
        def someFile = 'readme.txt'
        createAndCommitNewFile(repo, someFile, 'A long time ago in a galaxy far, far away...')

        // ... change it
        changeFile(repo, someFile, 'It is a dark time for the Rebellion.')

        // ... and add it to the staging area
        stageFile(repo, someFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. initialBranch is a descendant of the release branch
        assert isDescendant(repo,
        branchTip(repo, initialBranch),
        branchTip(repo, releaseBranch))

        // 3. the staging area contains some changes
        assert [someFile] == stagingAreaOf(repo).asList()

        // 2. the working directory is clean
        assert workingDirectoryOf(repo).asList().isEmpty()
    }

    private static void preconditions_workingDirectoryIsDirty_butDoesNotConflictWithReleaseBranch(repo, initialBranch) {
        // working on initialBranch...
        assert isCheckedOut(repo, initialBranch)

        // ... which is the same as the release branch, in the beginning
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // create a new file in the repo
        def someFile = createEmptyFileInRepo(repo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. initialBranch is on the same commit as the release branch
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // 2. the working directory contains some changes
        assert [someFile] == workingDirectoryOf(repo).asList()

        // 3. the staging area is clean
        assert stagingAreaOf(repo).asList().isEmpty()
    }

    private static void preconditions_stagingAreaIsDirty_butDoesNotConflictWithReleaseBranch(repo, initialBranch) {
        // working on initialBranch...
        assert isCheckedOut(repo, initialBranch)

        // ... which is the same as the central release branch, in the beginning
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // create a new file in the repo
        def someFile = createEmptyFileInRepo(repo)

        // ... and add it to the staging area
        stageFile(repo, someFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. initialBranch is on the same commit as the release branch
        assert branchTip(repo, initialBranch) == branchTip(repo, releaseBranch)

        // 2. the staging area contains some changes
        assert [someFile] == stagingAreaOf(repo).asList()

        // 3. the working directory is clean
        assert workingDirectoryOf(repo).asList().isEmpty()
    }

    private static void results_repoIsUnchangedAfterTest(repo, initialBranch, workingDirectoryBeforeTest, stagingAreaBeforeTest) {
        // Check results
        // ---------------------------------------------------------------------
        // 1. initial branch is still checked out
        assert isCheckedOut(repo, initialBranch)

        // 2. working directory and staging area are the same as before the test
        assert workingDirectoryBeforeTest == workingDirectoryOf(repo)
        assert stagingAreaBeforeTest == stagingAreaOf(repo)
    }
}
