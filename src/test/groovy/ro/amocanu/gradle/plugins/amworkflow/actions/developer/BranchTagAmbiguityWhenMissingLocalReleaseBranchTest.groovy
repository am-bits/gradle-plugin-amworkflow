/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitAndPushSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class BranchTagAmbiguityWhenMissingLocalReleaseBranchTest extends BaseTest {

    private static final ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL = { releaseBranch -> "Branch '${releaseBranch}' not found on central repository!" }

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'stealtheblueprints'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String someRandomBranch = 'makealotofnoise'

    private static final String central = ext.continuous.centralGitRepository
    private static final String releaseBranch = ext.continuous.releaseBranch
    private static final String remoteReleaseBranch = "${central}/${releaseBranch}"

    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, null)
    }

    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createFeatureWhenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        env.devRepo.fetch(remote: central)

        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)
        env.devRepo.tag.add(name: remoteReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_goToFeatureWhenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)
        env.devRepo.tag.add(name: remoteReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_discardFeatureWhenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)
        env.devRepo.tag.add(name: remoteReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_deliverFeatureWhenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)

        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)
        env.devRepo.tag.add(name: remoteReleaseBranch)

        env.devRepo.checkout(branch: featureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag(featureBranch1)

        // Test
        // ---------------------------------------------------------------------
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_createFeatureWhenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        env.devRepo.fetch(remote: central)
        commitAndPushSomething(env.devRepo, central, releaseBranch)

        def behindReleaseBranch = branchTip(env.devRepo, releaseBranch).abbreviatedId + '~1'
        env.devRepo.tag.add(name: remoteReleaseBranch, pointsTo: behindReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_goToFeatureWhenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        commitAndPushSomething(env.devRepo, central, releaseBranch)

        def behindReleaseBranch = branchTip(env.devRepo, releaseBranch).abbreviatedId + '~1'
        env.devRepo.tag.add(name: remoteReleaseBranch, pointsTo: behindReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_discardFeatureWhenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        commitAndPushSomething(env.devRepo, central, releaseBranch)

        def behindReleaseBranch = branchTip(env.devRepo, releaseBranch).abbreviatedId + '~1'
        env.devRepo.tag.add(name: remoteReleaseBranch, pointsTo: behindReleaseBranch)

        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag(someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_deliverFeatureWhenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        commitAndPushSomething(env.devRepo, central, releaseBranch)
        
        def behindReleaseBranch = branchTip(env.devRepo, releaseBranch).abbreviatedId + '~1'
        env.devRepo.tag.add(name: remoteReleaseBranch, pointsTo: behindReleaseBranch)

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)

        env.devRepo.checkout(branch: featureBranch1)
        env.devRepo.branch.remove(names: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag(featureBranch1)

        // Test
        // ---------------------------------------------------------------------
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    private void pre_descendantTag(String initialBranch) {
        // 1. [dev] only the remote-tracking release branch exists
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 2. [central] the release branch exists
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 3. [dev] initialBranch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 4. [dev] a tag named like the remote release branch exists
        assert tagsOf(env.devRepo).any { remoteReleaseBranch == it }

        // 5. [dev] ...on a descendant commit of the remote release branch
        assert isDescendant(env.devRepo,
            commitOfTag(env.devRepo, remoteReleaseBranch),
            remoteBranchTip(env.devRepo, remoteReleaseBranch))
    }

    private void pre_ancestorTag(String initialBranch) {
        // 1. [dev] only the remote-tracking release branch exists
        assert ! branchesOf(env.devRepo).any { releaseBranch == it }
        assert remoteBranchesOf(env.devRepo).any { remoteReleaseBranch == it }

        // 2. [central] the release branch exists
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 3. [dev] initialBranch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 4. [dev] a tag named like the remote release branch exists
        assert tagsOf(env.devRepo).any { remoteReleaseBranch == it }

        // 5. [dev] ...on an ancestor commit of the remote release branch
        assert isAncestor(env.devRepo,
            commitOfTag(env.devRepo, remoteReleaseBranch),
            remoteBranchTip(env.devRepo, remoteReleaseBranch))
    }

    private void checkResults() {
        // 1. [dev] the local release branch was created at the same commit as the remote release branch
        assert branchesOf(env.devRepo).any { releaseBranch == it }
        assert remoteBranchTip(env.devRepo, remoteReleaseBranch) == branchTip(env.devRepo, releaseBranch)

        // 2. [dev] ...and it tracks the remote release branch
        assert isTracking(env.devRepo, releaseBranch, remoteReleaseBranch)
    }
}
