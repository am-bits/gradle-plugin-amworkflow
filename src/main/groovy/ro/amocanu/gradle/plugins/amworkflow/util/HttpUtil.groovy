/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.util

import groovy.transform.Immutable

class HttpUtil {

    private static final LAST_GOOD_CODE = 399

    @Immutable
    static class HttpResponse {
        Integer code
        String message
    }

    static HttpResponse httpGet(String url, String username, String password) {
        final String authString = "${username}:${password}"
        final String authStringEnc = authString.bytes.encodeBase64().toString()

        final HttpURLConnection connection = url.toURL().openConnection()

        connection.setRequestMethod("GET")
        connection.setRequestProperty("Authorization", "Basic ${authStringEnc}")

        connection.connect()

        def response = new HttpResponse(code: connection.responseCode, message: connection.responseMessage)

        connection.disconnect()

        return response
    }

    static boolean isError(HttpResponse response) {
        return response.code > LAST_GOOD_CODE
    }
}
