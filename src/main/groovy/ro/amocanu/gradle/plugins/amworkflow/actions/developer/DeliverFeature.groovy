/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType
import ro.amocanu.gradle.plugins.amworkflow.util.HttpUtil

@PackageScope
class DeliverFeature implements Runnable {

    private final PluginExt ext
    private final GitRepo git

    private Configuration conf
    private String featureName

    DeliverFeature(PluginExt ext, GitRepo git) {
        this.ext = ext
        this.git = git
    }

    @Override
    public void run() {
        updateContextFromCheckedOutBranch()
        new DeliverFeatureWorker(conf, git, featureName).run()
    }

    private String updateContextFromCheckedOutBranch() {
        def isFeatureBranch = false
        def currentBranch = git.currentBranch()
        if (currentBranch == null) {
            throw new GradleException ('Cannot perform task while in detached head!')
        }

        conf = ConfigurationFactory.fromExtension(ext)
        isFeatureBranch = conf.isFeatureBranch(currentBranch)

        if (!isFeatureBranch && ext.root.workflowType == WorkflowType.VERSIONED.toString()) {
            def tempConf = ConfigurationFactory.fromVersionedExtension(ext.versioned)
            if (tempConf.versioned.isMaintenanceBranch(currentBranch)) {
                def maintainedVersion = tempConf.versioned.maintainedVersionFromMaintenanceBranch(currentBranch)
                conf = ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, maintainedVersion)
                isFeatureBranch = conf.isFeatureBranch(currentBranch)
            }
        }

        if (isFeatureBranch) {
            featureName = conf.featureNameFromFeatureBranch(currentBranch)
        } else {
            throw new GradleException("The checked-out branch '${currentBranch}' is not a feature branch, it cannot be delivered!")
        }
    }

    private class DeliverFeatureWorker extends AbstractFeatureAction {
        DeliverFeatureWorker(Configuration conf, GitRepo git, String featureName) {
            super(conf, git, featureName)
        }

        @Override
        protected void runFeatureAction() {
            featureMustExist()
            featureBranchMustBeInSyncWithUpstreamBranch()
            featureBranchMustBeInSyncWithReleaseBranch()
            noReleaseMustBeInProgress()
            pseudoPreReleaseBranchMustNotExist()

            createAndPushPreReleaseBranch()
            removeLocalPreReleaseBranch()

            triggerCIJob()
        }

        private void featureMustExist() {
            if (!git.isCentralBranch(featureBranch)) {
                String remoteFeatureBranch = "${conf.centralGitRepository}/${featureBranch}"
                String potentialRemoteFeatureBranch = git.remoteTrackingCentralBranches().find {
                    remoteFeatureBranch.equalsIgnoreCase(it)
                }

                if (potentialRemoteFeatureBranch) {
                    String potentialFeatureBranch = potentialRemoteFeatureBranch[conf.centralGitRepository.size()+1..-1]
                    throw new GradleException(
                        "Feature '${featureName}' does not exist in the central repo! (Did you mean to check out the feature branch '${potentialFeatureBranch}' instead?)")
                }

                throw new GradleException("Feature '${featureName}' does not exist in the central repo!")
            }
        }

        private void featureBranchMustBeInSyncWithUpstreamBranch() {
            def remoteName = conf.centralGitRepository
            def remoteFeatureBranch = "${remoteName}/${featureBranch}"
            def localToUpstream = git.relation(
                git.commitIdOfBranch(featureBranch),
                git.commitIdOfRemoteBranch(remoteFeatureBranch))

            switch (localToUpstream) {
                case Relation.SAME:
                    break

                case [Relation.ANCESTOR, Relation.NONE]:
                    throw new GradleException ("To deliver the feature, you first need to merge '${remoteFeatureBranch}' to '${featureBranch}'!")

                case Relation.DESCENDANT:
                    throw new GradleException ("To deliver the feature, you first need to push changes on branch '${featureBranch}' to the central repository!")

                default:
                    assert false
            }
        }

        private void featureBranchMustBeInSyncWithReleaseBranch() {
            def remoteName = conf.centralGitRepository
            def remoteReleaseBranch = "${remoteName}/${conf.releaseBranch}"
            def localToRemoteRelease = git.relation(
                git.commitIdOfBranch(featureBranch), 
                git.commitIdOfRemoteBranch(remoteReleaseBranch))

            switch (localToRemoteRelease) {
                case Relation.SAME:
                    throw new GradleException('Nothing to deliver!')
                    break

                case Relation.DESCENDANT:
                    break

                case [Relation.ANCESTOR, Relation.NONE]:
                    throw new GradleException ("To deliver the feature, you first need to merge '${remoteReleaseBranch}' to '${featureBranch}'!")

                default:
                    assert false
            }
        }

        private void noReleaseMustBeInProgress() {
            if (git.isCentralBranch(conf.preReleaseBranch)) {
                throw new GradleException("Another release is in progress... Try again later.")
            }
        }

        private void pseudoPreReleaseBranchMustNotExist() {
            def remotePreReleaseBranch = "${conf.centralGitRepository}/${conf.preReleaseBranch}"
            def remotePseudoPreReleaseBranch = git.remoteTrackingCentralBranches().find {
                remotePreReleaseBranch != it &&
                remotePreReleaseBranch.equalsIgnoreCase(it)
            }
            if (remotePseudoPreReleaseBranch) {
                def commitId = git.commitIdOfRemoteBranch(remotePseudoPreReleaseBranch)
                throw new GradleException("Branch '${remotePseudoPreReleaseBranch}' on commit '${commitId}' shadows the pre-release branch! No feature can be delivered unless branch '${remotePseudoPreReleaseBranch}' is removed.")
            }
        }

        private void createAndPushPreReleaseBranch() {
            git.createLocalAndRemoteBranchAtBranch(conf.preReleaseBranch, featureBranch)
            println "Feature '${featureName}' was successfully delivered"
        }

        private void removeLocalPreReleaseBranch() {
            git.checkoutBranch(conf.releaseBranch)
            git.removeBranch(conf.preReleaseBranch)
        }

        private void triggerCIJob() {
            def url = conf.releaseCIJobURL

            if (url) {
                def response = HttpUtil.httpGet(url, conf.ciUsername, conf.ciPassword)

                if (HttpUtil.isError(response)) {
                    throw new GradleException("Received error code on triggering CI server: ${response.code}\n${response.message}")
                }

                println "Triggered CI release job at '${url}'"
            }
        }
    }
}
