/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.operation.FetchOp
import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class BranchTagAmbiguityOnCreateMaintenanceBranchTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))
    
    private static final int v0 = 0

    private static final int v1 = 1
    private static final String v1_maintenanceBranch = 'maintenance_v1'
    private static final String v1_remoteMaintenanceBranch = 'origin/maintenance_v1'

    private static final String featureName1 = 'so_long_and_thanks_for_all_the_fish'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = "don't_panic"
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String featureName3 = "oh_no_not_again"
    private static final String featureBranch3 = "feature/${featureName3}"
    private static final String remoteFeatureBranch3 = "origin/feature/${featureName3}"

    private static final String someRandomBranch = 'the_answer_is_42'

    private static final String releaseBranch = ext.versioned.mainBranch
    private static final String central = ext.versioned.centralGitRepository

    private static final String v1Name = 'v1.0.0'
    private static final String v2Name = 'v2.0.0'

    private TestEnv env

    private GitRepo devGit
    private GitRepo devGit2
    private GitRepo integratorGit

    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        devGit2 = GrGitRepo.open(env.devRepo2.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)

        helper = new TestHelper(env, ext, devGit, integratorGit)

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. [integrator, central] integrate the feature
        helper.test_integrateFeature()
        
        // 3. [integrator, central] create the first version
        helper.test_createFirstVersion()

        // 4. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 5. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 6. [integrator, central] create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')

        // 7. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)

        // 8. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 9. [integrator, central] create a new version 2.0.0
        helper.test_createNewVersion('major', '2.0.0')

    }

    @After
    public void tearDown() {
        devGit.close()
        devGit2.close()
        integratorGit.close()

        env.tearDown()
    }

    @Test
    public void test_createMaintenanceBranch_whenBranch_v1_0_0_existsInsteadOfTag_v1_0_0() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] replace v1.0.0 tag with a branch
        env.devRepo.branch.add(name: v1Name, startPoint: v1Name)
        env.devRepo.tag.remove(names: [v1Name])
        env.devRepo.push(remote: central, refsOrSpecs: [v1Name, ":refs/tags/${v1Name}"])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev, central] v1.0.0 tag does not exist
        assert ! tagsOf(env.devRepo).asList().contains(v1Name)
        assert ! tagsOf(env.centralRepo).asList().contains(v1Name)

        // 2. [dev, central] a branch named v1.0.0 exists
        assert branchesOf(env.devRepo).asList().contains(v1Name)
        assert branchesOf(env.centralRepo).asList().contains(v1Name)

        // 3. [dev, central] the v1 maintenance branch does not exist
        assert ! branchesOf(env.devRepo).asList().contains(v1_maintenanceBranch)
        assert ! remoteBranchesOf(env.devRepo).asList().contains(v1_remoteMaintenanceBranch)
        assert ! branchesOf(env.centralRepo).asList().contains(v1_maintenanceBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Maintenance branch '${v1_maintenanceBranch}' cannot be created because no appropriate version tag was found!" == e.getMessage()
        
        // 2. [dev, central] the v1 maintenance branch still does not exist
        assert ! branchesOf(env.devRepo).asList().contains(v1_maintenanceBranch)
        assert ! remoteBranchesOf(env.devRepo).asList().contains(v1_remoteMaintenanceBranch)
        assert ! branchesOf(env.centralRepo).asList().contains(v1_maintenanceBranch)
    }

    @Test
    public void test_createMaintenanceBranch_whenBranch_v1_0_0_isAncestorOfTag_v1_0_0_andAPseudoRemoteMaintenanceBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create branch v1.0.0 one commit behind tag v1.0.0
        def startPoint = commitIdOfTag(env.devRepo, v1Name) + '~1'
        env.devRepo.branch.add(name: v1Name, startPoint: startPoint)
        env.devRepo.branch.add(name: v1_remoteMaintenanceBranch, startPoint: startPoint)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev, central] v1.0.0 tag exists
        assert tagsOf(env.devRepo).asList().contains(v1Name)
        assert tagsOf(env.centralRepo).asList().contains(v1Name)

        // 2. [dev] a branch named v1.0.0 exists
        assert branchesOf(env.devRepo).asList().contains(v1Name)
        assert ! branchesOf(env.centralRepo).asList().contains(v1Name)

        // 3. [dev] branch v1.0.0 is ancestor of tag v1.0.0
        assert isAncestor(env.devRepo, 
            branchTip(env.devRepo, v1Name),
            commitOfTag(env.devRepo, v1Name))

        // 4. [dev] a branch named named like the remote-tracking maintenance branch exists
        assert branchesOf(env.devRepo).asList().contains(v1_remoteMaintenanceBranch)

        // 5. [dev] the pseudo-remote-tracking maintenance branch is also an ancestor of tag v1.0.0
        assert isAncestor(env.devRepo,
            branchTip(env.devRepo, v1_remoteMaintenanceBranch),
            commitOfTag(env.devRepo, v1Name))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)
    }

    @Test
    public void test_createMaintenanceBranch_whenBranch_v1_0_0_isDescendantOfTag_v1_0_0() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create branch v1.0.0 one commit behind tag v2.0.0
        env.devRepo.fetch(remote: central, tagMode: FetchOp.TagMode.ALL)
        def startPoint = commitIdOfTag(env.integratorRepo, v2Name) + '~1'
        env.devRepo.branch.add(name: v1Name, startPoint: startPoint)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev, central] v1.0.0 tag exists
        assert tagsOf(env.devRepo).asList().contains(v1Name)
        assert tagsOf(env.centralRepo).asList().contains(v1Name)

        // 2. [dev] a branch named v1.0.0 exists
        assert branchesOf(env.devRepo).asList().contains(v1Name)
        assert ! branchesOf(env.centralRepo).asList().contains(v1Name)

        // 3. [dev] branch v1.0.0 is descendant of tag v1.0.0
        assert isDescendant(env.devRepo,
            branchTip(env.devRepo, v1Name),
            commitOfTag(env.devRepo, v1Name))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)
    }
}
