/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation

@PackageScope
class GoToFeature extends AbstractFeatureAction {

    GoToFeature(Configuration conf, GitRepo git, String featureName) {
        super(conf, git, featureName)
    }

    @Override
    protected void runFeatureAction() {
        featureMustExist()
        checkOutLocalFeatureBranch()
        syncWithUpstreamFeatureBranch()
    }
    
    private void featureMustExist() {
        if (!git.isCentralBranch(featureBranch)) {

            String remotePrefix = conf.centralGitRepository + '/'
            String remoteFeatureBranch = remotePrefix + featureBranch
            String potentialRemoteFeatureBranch = git.remoteTrackingCentralBranches().find { remoteFeatureBranch.equalsIgnoreCase(it) }

            if (!potentialRemoteFeatureBranch) {
                throw new GradleException("Feature '${featureName}' does not exist!")
            }
            
            String potentialFeatureBranch = potentialRemoteFeatureBranch[remotePrefix.size()..-1]
            String potentialFeature = conf.featureNameFromFeatureBranch(potentialFeatureBranch)
            throw new GradleException("Feature '${featureName}' does not exist! (Did you mean '${potentialFeature}'?)")
        }
    }
    
    private void checkOutLocalFeatureBranch() {
        checkOutBranch(featureBranch)
    }

    private void syncWithUpstreamFeatureBranch() {
        def centralName = conf.centralGitRepository
        def upstreamBranchName = "${centralName}/${featureBranch}"
        def localToUpstreamRelation = git.relation(
            git.commitIdOfBranch(featureBranch),
            git.commitIdOfRemoteBranch(upstreamBranchName))

        switch (localToUpstreamRelation) {
            case Relation.SAME:
                // already sync-ed
                break
            case Relation.ANCESTOR:
                git.fastForwardCurrentBranchTo(upstreamBranchName)
                break
            case Relation.NONE:
                println "You need to merge '${upstreamBranchName}' to '${featureBranch}' before continuing work on this feature!"
                break
            case Relation.DESCENDANT:
                println "There are changes on branch '${featureBranch}' not yet pushed to '${centralName}'!"
                break
            default:
                assert false 
        }
    }
}
