# Versioned delivery

This page describes how to use *am*workflow for versioned delivery.

!!! note
    The *am*workflow tasks are quite verbose. If you use a Unix shell, consider [defining and using aliases](../extra/#helpful-aliases) instead of invoking the tasks directly.

___

## The version file

*am*workflow keeps a [properties file](https://en.wikipedia.org/wiki/.properties) in your repository (called `version.properties` by default) containing the latest version number of the project.

Don't edit the version file manually, *am*workflow will handle it!

!!! warning "Attention"
    You must create the initial version of the version file, with the following contents: `version='0.0.0'`

    Commit the file on the main branch and push it to the central repo.

___

## The developer workflow on the main branch

The developer workflow for versioned delivery on the main branch is **exactly the same** as [the continuous delivery workflow](continuous-delivery/#the-developer-workflow).

The *main branch* of the versioned delivery workflow is equivalent to the *release branch* of the continuous delivery workflow.

___

## The integrator workflow on the main branch

The integrator workflow for versioned delivery on the main branch is **mostly the same** as the [continuous delivery workflow](continuous-delivery/#the-integrator-workflow):

**1. Trigger the integration.**

<small>Same as in [continuous delivery](continuous-delivery/#integrator1).</small>

<hr class="gradient_divider"/>

**1a. Check the integration status.**

<small>Same as in [continuous delivery](continuous-delivery/#integrator1a).</small>

<hr class="gradient_divider"/>

**2. Prepare the integration.**

<small>Same as in [continuous delivery](continuous-delivery/#integrator2).</small>

<hr class="gradient_divider"/>

**3. Build and test the pre-release branch.**

<small>Same as in [continuous delivery](continuous-delivery/#integrator3).</small>

<hr class="gradient_divider"/>

**4. Integrate the pre-release branch.**

The same as in [continuous delivery](continuous-delivery/#integrator4) **except no release tag is created.** This allows for several feature branches to be integrated before a new project version is released.

<hr class="gradient_divider"/>

**4a. Cancel the integration.**

<small>Same as in [continuous delivery](continuous-delivery/#integrator4a).</small>

<hr class="gradient_divider"/>

**5. Repeat steps 1-4.**

Integrate several feature branches before releasing a new project version.

<hr class="gradient_divider"/>

**6. Create a new version.**

```no-highlight
gradle createNewVersion -Prole=INTEGRATOR -PversionType={major|minor|patch}
```

The `createNewVersion` task creates the appropriate version tag depending on the `versionType`.

This is always a manual step. The integrator must choose which version segment to increment.

___

## The developer workflow on a maintenance branch

**1. Start a new maintenance branch.**

```no-highlight
gradle createMaintenanceBranch -PmaintainedVersion=1
```

!!! note
    This task should be run only once, when the maintenance of version 1 begins.

The task creates the `maintenance_v1` branch pointing at the commit with the highest `1.x.x` tag.

The `createMaintenanceBranch` will work only if version `2.0.0` exists already.

You may, of course, substitute version number *1* in the example above for any other number *n*. You may create a maintenance branch for version *n* only if the next major version, i.e. *(n+1).0.0*, already exists.

<hr class="gradient_divider"/>

**2. Start a new feature.**

```no-highlight
gradle createMaintenanceFeature -PmaintainedVersion=1 -PfeatureName=myFeature
```

The task creates and checks out a branch named `maintenance_v1_feature/myFeature` from the latest commit on `maintenance_v1`.

The maintenance-branch prefix ("maintenance"), the feature-branch prefix ("feature") and the version prefix ("v1") can be [changed](#configuration-maint) to suit your needs. (The full maintenance feature branch prefix, "maintenance_v1_feature", is composed from these three.)

<hr class="gradient_divider"/>

**2a. Start working on an existing feature. <small>Alternative to step 2.</small>**

```no-highlight
gradle goToMaintenanceFeature -PmaintainedVersion=1 -PfeatureName=myFeature
```

The task `goToMaintenanceFeature` checks out an existing maintenance feature branch. The branch must exist on the central repo, but not necessarily on the developer's repo (e.g. if another developer started the feature).

<hr class="gradient_divider"/>

**3. Develop the feature.**

Write code, commit and push on branch `maintenance_v1_feature/myFeature`.

<hr class="gradient_divider"/>

**4. Deliver the feature.**

```no-highlight
gradle deliverFeature
```

The task delivers the currently checked-out maintenance feature branch for integration.

This is the same task that is used in the main branch workflow. But, when executed on a maintenance feature branch, `deliverFeature` creates the `maintenance_v1_preRelease` branch.

<hr class="gradient_divider"/>

**4a. Discard the feature. <small>Alternative to step 4.</small>**

```no-highlight
gradle discardMaintenanceFeature -PmaintainedVersion=1 -PfeatureName=myFeature
```

The task `discardMaintenanceFeature` removes a maintenace feature without delivering it.

The maintenance feature branch is deleted from both repos, local and central. If other developers also have the branch locally, it will be removed from their repo by `syncWithCentral` when they run any of the tasks on this page.

<hr class="gradient_divider"/>

**5. Clean-up your local repository**

Same as in the [continuous delivery workflow](continuous-delivery/#developer4).

___

## The integrator workflow on a maintenance branch

**1. Trigger the integration.**

If you use a CI server that supports <a href="https://wiki.jenkins-ci.org/display/JENKINS/Remote+access+API">URL triggers</a>, you can [configure](#jenkins-url-trigger-configuration-maint) *am*workflow to trigger the integration build automatically for each maintenance branch, when a maintenance feature is delivered.

**1a. Check the integration status. <small>Alternative to step 1 when the integrator is a person, not a machine.</small>**

```no-highlight
gradle showMaintenanceIntegrationStatus -Prole=INTEGRATOR -PmaintainedVersion=1
```
The task displays whether there is any feature branch to integrate.

**2. Prepare the integration.**

```no-highlight
gradle prepareMaintenanceFeatureIntegration -Prole=INTEGRATOR -PmaintainedVersion=1
```

Similar to `prepareFeatureIntegration` of the [continous delivery workflow](continuous-delivery/#integrator2).

<hr class="gradient_divider"/>

**3. Build and test the pre-release branch.**

Build the code, run the tests. If everything is in order, integrate the pre-release branch. Otherwise, cancel the integration.

<hr class="gradient_divider"/>

**4. Integrate the pre-release branch.**

```no-highlight
gradle integrateMaintenanceFeature -Prole=INTEGRATOR -PmaintainedVersion=1
```

This task merges the `maintenance_v1_preRelease` branch into `maintenance_v1`, pushes the changes to the central repo, then removes the pre-release branch and the feature branch on both the local and the central repositories.

<hr class="gradient_divider"/>

**4a. Cancel the integration. <small>Alternative to step 4.</small>**

```no-highlight
gradle cancelMaintenanceIntegration -Prole=INTEGRATOR -PmaintainedVersion=1
```

Similar to `cancelIntegration` of the [continous delivery workflow](continuous-delivery/#integrator4a).

<hr class="gradient_divider"/>

**5. Repeat steps 1-4.**

You may integrate as many feature branches as you need before a new project version is released.

<hr class="gradient_divider"/>

**6. Create a new maintenance version.**

```no-highlight
gradle createNewMaintenanceVersion -Prole=INTEGRATOR -PmaintainedVersion=1 -PversionType={minor|patch}
```

The `createNewMaintenanceVersion` task creates the appropriate version tag depending on the `versionType`. Only minor or patch versions may be created on a maintenance branch.

This is always a manual step. The integrator must choose which version segment to increment.

___

## The integrator switch

Same as in the [continuous delivery workflow](continuous-delivery/#the-integrator-switch).

___

## Configuration

*am*workflow comes with sensible defaults, but it is also flexible. To change the defaults or to activate optional features (such as integration with your CI server) add an `amworkflow` configuration block to your `build.gradle` file as described below.

The values in the example below are the default values.

```no-highlight
amworkflow {
  workflowType = 'VERSIONED'
  versioned {
    // if not specified, Git credentials are interactive
    // otherwise, they should be defined in each team member's private gradle.properties file
    gitUsername = ''
    gitPassword = ''

    // the remote name for the central repo
    centralGitRepository = 'origin'

    mainBranch = 'master'
    maintenanceBranchPrefix = 'maintenance'
    featureBranchPrefix = 'feature'
    preReleaseBranch = 'preRelease'
    versionPrefix = 'v'
    versionFile = 'version.properties'
    bumpVersionCommitMessage = { version -> "Bump version number to $version" }

    // the URL for triggering the integration job on the CI server
    // when the developer delivers a feature on the main branch
    mainIntegrationURLTrigger = ''

    // a map of versions-to-URLs;
    // each URL triggers an integration job on the CI server
    // when the developer delivers a feature on the
    // corresponding maintenance branch
    maintenanceIntegrationURLTriggers = [:]

    // CI server credentials, only needed if you use the URL trigger
    // if used, they should be defined in each team member's private gradle.properties file
    ciUsername = ''
    ciPassword = ''
  }
}
```

<h3 id="configuration-maint"></h3>
**Maintenance branch configuration example:**

```no-highlight
amworkflow {
  workflowType = 'VERSIONED'
  versioned {
    maintenanceBranchPrefix = 'maint'
    featureBranchPrefix = 'feature'
    preReleaseBranch = 'pre'
    versionPrefix = 'ver'
  }
}
```

With this configuration we have:

  - `maint_ver1` = the maintenance branch for version 1
  - `maint_ver1_feature/someFeature` = a maintenance feature branch for version 1
  - `maint_ver1_pre` = the pre-release branch for version 1
  - `ver1.1.0` = a release tag

<h3 id="jenkins-url-trigger-configuration-maint"></h3>
**Jenkins URL trigger configuration example:**

Triggers are configured for the main branch and two maintenance branches:

```no-highlight
amworkflow {
  workflowType = 'VERSIONED'
  versioned {
    // triggers for maintenance branches, v1 and v2
    maintenanceIntegrationURLTriggers = [
      1:'http://ci.example.org/job/CoolProjectV1-release/build?token=token456', // v1
      2:'http://ci.example.org/job/CoolProjectV2-release/build?token=token789'  // v2
    ]
    // trigger current version, v3
    mainIntegrationURLTrigger = 'http://192.168.0.1:8080/job/CoolProject-release/build?token=token123'
    // Jenkins credentials, defined in each team member's private gradle.properties file
    ciUsername = myJenkinsUsername
    ciPassword = myJenkinsPassword
  }
}
```
