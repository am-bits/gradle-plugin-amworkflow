/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitAndPushSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.DevTestHelper
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@RunWith(Parameterized.class)
class CreateFeatureTest extends BaseTest {

    private static final PluginExt ext1 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'support',
            maintenanceIntegrationURLTriggers: [:]))

    private static TestEnv env
    private static GitRepo devGit
    private static GitRepo devGit2
    private static GitRepo integratorGit
    private static TestHelper helper

    private final PluginExt ext

    private final String central
    private final String featurePrefix
    private final String releaseBranch
    private final String remoteReleaseBranch

    private static final String featureName1 = 'wingardium'
    private final String featureBranch1
    private final String remoteFeatureBranch1

    private static final String featureName2 = 'wingardium/leviosa'
    private final String featureBranch2
    private final String remoteFeatureBranch2

    private static final String featureName3 = 'wingardium-leviosa'
    private final String featureBranch3
    private final String remoteFeatureBranch3

    private static final String featureName3a = 'Wingardium-Leviosa'
    private final String featureBranch3a
    private final String remoteFeatureBranch3a

    private static final String someRandomBranch = 'avada/kedavra'

    @Parameterized.Parameters
    public static Collection pluginExtensions() {
        return [ext1, ext2]
    }

    public CreateFeatureTest(PluginExt ext) {
        this.ext = ext

        switch (ext.root.workflowType) {
            case WorkflowType.CONTINUOUS.toString():
                this.central = ext.continuous.centralGitRepository
                this.featurePrefix = ext.continuous.featureBranchPrefix
                this.releaseBranch = ext.continuous.releaseBranch
                break
            case WorkflowType.VERSIONED.toString():
                this.central = ext.versioned.centralGitRepository
                this.featurePrefix = ext.versioned.featureBranchPrefix
                this.releaseBranch = ext.versioned.mainBranch
                break
            default:
                assert false
        }

        this.remoteReleaseBranch = "${central}/${releaseBranch}"

        this.featureBranch1 = featureBranch(featureName1)
        this.featureBranch2 = featureBranch(featureName2)
        this.featureBranch3 = featureBranch(featureName3)
        this.featureBranch3a = featureBranch(featureName3a)

        this.remoteFeatureBranch1 = remoteFeatureBranch(featureName1)
        this.remoteFeatureBranch2 = remoteFeatureBranch(featureName2)
        this.remoteFeatureBranch3 = remoteFeatureBranch(featureName3)
        this.remoteFeatureBranch3a = remoteFeatureBranch(featureName3a)
    }

    private String featureBranch(String featureName) {
        return "${featurePrefix}/${featureName}"
    }

    private String remoteFeatureBranch(String featureName) {
        return "${central}/${featurePrefix}/${featureName}"
    }

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        devGit2 = GrGitRepo.open(env.devRepo2.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        devGit.close()
        devGit2.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_createSameFeatureTwice() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. try to create the same feature again
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1}' already exists!" == e.getMessage()

        // 2. the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_createFeatureWhenFeatureExistsOnlyOnCentral() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.devRepo.branch.remove(names: [featureBranch1])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists only on central
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 2. a random branch is checked-out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1}' already exists!" == e.getMessage()

        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_createLocallyConflictingFeatureBranch() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create a conflicting branch in the dev and central repos
        def conflictingBranch = featureBranch2
        def remoteConflictingBranch = remoteFeatureBranch2

        env.devRepo.branch.add(name: conflictingBranch)
        env.devRepo.push(refsOrSpecs: ["refs/heads/${conflictingBranch}:refs/heads/${conflictingBranch}"])

        assert branchesOf(env.devRepo).asList().contains(conflictingBranch)
        assert remoteBranchesOf(env.devRepo).asList().contains(remoteConflictingBranch)
        assert branchesOf(env.centralRepo).asList().contains(conflictingBranch)

        assert ! featureName1.contains('/')
        assert conflictingBranch.startsWith(featureBranch1 + '/')

        // 2. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. try to create the feature branch
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature branch '${featureBranch1}' cannot be created, it conflicts with existing branch '${conflictingBranch}'!" == e.getMessage()

        // 2. the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_createFeatureWhenFeatureNameContainsASlash() {
        // Test
        // ---------------------------------------------------------------------
        // 1. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 2. try to create the feature branch with '/' in the name
        assert featureName2.contains('/')
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName2).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature name cannot contain the '/' character!" == e.getMessage()

        // 2. the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_createRemotelyConflictingFeatureBranch() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create a conflicting branch, only in the central repo
        def conflictingBranch = featureBranch2
        def remoteConflictingBranch = remoteFeatureBranch2

        env.devRepo.branch.add(name: conflictingBranch)
        env.devRepo.push(refsOrSpecs: ["refs/heads/${conflictingBranch}:refs/heads/${conflictingBranch}"])
        env.devRepo.branch.remove(names: [conflictingBranch])

        assert ! branchesOf(env.devRepo).asList().contains(conflictingBranch)
        assert remoteBranchesOf(env.devRepo).asList().contains(remoteConflictingBranch)
        assert branchesOf(env.centralRepo).asList().contains(conflictingBranch)

        assert ! featureName1.contains('/')
        assert conflictingBranch.startsWith(featureBranch1 + '/')

        // 2. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. try to create the feature branch
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature branch '${featureBranch1}' cannot be created, it conflicts with existing branch '${remoteConflictingBranch}'!" == e.getMessage()

        // 2. the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_createFeatureWhenABranchNamedFeatureExists() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create a branch named 'feature' in the dev repo
        env.devRepo.branch.add(name: featurePrefix)

        assert featureBranch1.startsWith(featurePrefix)

        // 2. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. try to create the feature branch
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature branch '${featureBranch1}' cannot be created, it conflicts with existing branch '${featurePrefix}'!" == e.getMessage()

        // 2. the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }
    
    @Test
    public void test_createFeatureWithNameIncludedInTheNameOfExistingFeature() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)

        // 2. go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 3. create another feature with a name included in the first feature
        assert ! featureName1.contains('/')
        assert featureName3.startsWith(featureName1)

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void test_recreateIntegratedFeature() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature delivered from the dev repo
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        
        // 2. the feature is integrated
        helper.test_integrateFeature()

        // 3. the feature branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        // 4. ... but it exists in the dev repo
        assert branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        
        // 5. on dev repo, a random branch is checked out
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. create the feature 
        DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature branch now exists on both dev or central repos
        assert branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        // 2. the feature branch on dev tracks the corresponding branch on central
        assert isTracking(env.devRepo, featureBranch1, remoteFeatureBranch1)

        // 3. on dev, the feature branch is checked out
        assert isCheckedOut(env.devRepo, featureBranch1)
        
        // 4. on central, the feature branch is the same as the release branch
        assert branchTip(env.centralRepo, releaseBranch) == branchTip(env.centralRepo, featureBranch1)
    }

    @Test
    public void test_createSameFeatureTwice_withCaseDifferencesInFeatureName() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the new feature name differs only by case from the existing feature
        assert featureName3 != featureName3a
        assert featureName3.toLowerCase() == featureName3a.toLowerCase()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. [dev] go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 2. [dev] try to create the feature
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName3a).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature branch '${featureBranch3a}' already exists with a different case: '${featureBranch3}'!" == e.getMessage()

        // 2. [dev] the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_sameFeatureCreatedByDifferentDevelopers_withCaseDifferencesInFeatureName() {
        DevTestHelper helper2 = new DevTestHelper(env, ext, devGit2)
        
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev2, central] create a feature
        helper2.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the new feature name differs only by case from the existing feature
        assert featureName3 != featureName3a
        assert featureName3.toLowerCase() == featureName3a.toLowerCase()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. [dev] go to a random branch
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)
        assert isCheckedOut(env.devRepo, someRandomBranch)

        // 2. [dev] try to create the feature
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName3a).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Remote-tracking feature branch '${remoteFeatureBranch3a}' already exists with a different case: '${remoteFeatureBranch3}'!" == e.getMessage()

        // 2. [dev] the random branch is still checked-out
        assert isCheckedOut(env.devRepo, someRandomBranch)
    }

    @Test
    public void test_createFeature_whenDescendantTagIsNamedLikeTheRemoteReleaseBranch() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] commit something on the release branch
        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)

        // 2. [dev] create a tag named like the remote release branch
        env.devRepo.tag.add(name: remoteReleaseBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a tag named like the remote release branch exists
        assert tagsOf(env.devRepo).asList().contains(remoteReleaseBranch)

        // 2. [dev] ...and it is placed on a commit descending from the remote release branch
        assert isDescendant(env.devRepo,
            commitOfTag(env.devRepo, remoteReleaseBranch),
            remoteBranchTip(env.devRepo, remoteReleaseBranch))
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void test_createFeature_whenAncestorTagIsNamedLikeTheRemoteReleaseBranch() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create a tag named like the remote release branch
        env.devRepo.checkout(branch: releaseBranch)
        env.devRepo.tag.add(name: remoteReleaseBranch)

        // 2. [dev, central] commit and push something on the release branch
        commitAndPushSomething(env.devRepo, central, releaseBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a tag named like the remote release branch exists
        assert tagsOf(env.devRepo).asList().contains(remoteReleaseBranch)

        // 2. [dev] ...and it is placed on an ancestor commit to the remote release branch
        assert isAncestor(env.devRepo,
            commitOfTag(env.devRepo, remoteReleaseBranch),
            remoteBranchTip(env.devRepo, remoteReleaseBranch))
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
    }
}
