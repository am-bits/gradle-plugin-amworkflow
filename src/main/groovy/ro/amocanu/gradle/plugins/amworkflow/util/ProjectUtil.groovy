/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.util

import org.gradle.api.GradleException
import org.gradle.api.Project

class ProjectUtil {

    static String getProperty(Project project, String key) {
        assert project != null

        def value = null

        if (project.hasProperty(key)) {
            value = project.property(key)
        }

        if (!value) {
            throw new GradleException("This task requires the following property: ${key}")
        }

        return value
    }

    static String getOptionalProperty(Project project, String key) {
        assert project != null

        def value = null

        if (project.hasProperty(key)) {
            value = project.property(key)
        }

        return value
    }
}
