/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitAndPushSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.Commit
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.operation.BranchAddOp
import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class CreateNewMaintenanceVersionTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'to-integrate',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'version-',
            versionFile: '.version',
            bumpVersionCommitMessage: { version -> "Version ${version}" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName1 = 'WatchPot'
    private static final String featureBranch1 = "maintenance_version-1_feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/maintenance_version-1_feature/${featureName1}"

    private static final String randomBranch = 'Stare_At_Ceiling'

    private static final String mainBranch = ext.versioned.mainBranch

    private static final int v1 = 1
    private static final String v1_releaseBranch = 'maintenance_version-1'
    private static final String v1_remoteReleaseBranch = "${ext.versioned.centralGitRepository}/${v1_releaseBranch}"
    private static final String v1_preReleaseBranch = 'maintenance_version-1_to-integrate'
    private static final String v1_remotePreReleaseBranch = "${ext.versioned.centralGitRepository}/${v1_preReleaseBranch}"

    private static final int v2 = 2
    private static final String v2_releaseBranch = 'maintenance_version-2'
    private static final String v2_remoteReleaseBranch = "${ext.versioned.centralGitRepository}/${v2_releaseBranch}"
    private static final String v2_preReleaseBranch = 'maintenance_version-2_to-integrate'
    private static final String v2_remotePreReleaseBranch = "${ext.versioned.centralGitRepository}/${v2_preReleaseBranch}"

    private static final String versionFile = ext.versioned.versionFile
    private static final String versionPrefix = ext.versioned.versionPrefix

    private TestEnv env
    private GitRepo integratorGit
    private GitRepo devGit
    private TestHelper helper

    private String integratorBranchBeforeTest
    private List integratorTagsBeforeTest
    private Commit integratorReleaseBranchBeforeTest
    private List centralTagsBeforeTest
    private Commit centralReleaseBranchBeforeTest

    @Before
    public void setUp() {
        env = new TestEnv()
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        integratorBranchBeforeTest = null
        integratorTagsBeforeTest = null
        integratorReleaseBranchBeforeTest = null
        centralTagsBeforeTest = null
        centralReleaseBranchBeforeTest = null

        // Common test setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions in the range [1.0.0, 3.0.0]
        helper.test_createFirstVersion() // 0.1.0

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('minor', '1.1.0')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('patch', '1.1.1')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('major', '2.0.0')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('patch', '2.0.1')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('patch', '2.0.2')

        commitAndPushSomething(env.integratorRepo, mainBranch)
        helper.test_createNewVersion('major', '3.0.0')

        // 2. [dev, central] create maintenance branches for v1 and v2
        helper.test_createMaintenanceBranch(v1, v1_releaseBranch, v1_remoteReleaseBranch)
        helper.test_createMaintenanceBranch(v2, v2_releaseBranch, v2_remoteReleaseBranch)

        // 3. [integrator] checked out the main branch
        env.integratorRepo.checkout(branch: mainBranch)

        // 4. [integrator] sync with central
        env.integratorRepo.fetch()
        assert remoteBranchesOf(env.integratorRepo).asList().containsAll([v1_remoteReleaseBranch, v2_remoteReleaseBranch])

        env.integratorRepo.branch.add(name: v1_releaseBranch, startPoint: v1_remoteReleaseBranch, mode: BranchAddOp.Mode.TRACK)
        env.integratorRepo.branch.add(name: v2_releaseBranch, startPoint: v2_remoteReleaseBranch, mode: BranchAddOp.Mode.TRACK)
        assert branchesOf(env.integratorRepo).asList().containsAll([v1_releaseBranch, v2_releaseBranch])
    }

    @After
    public void tearDown() {
        integratorGit.close()
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createNewVersion_withUnknownVersionType() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the main branch is checked out
        assert isCheckedOut(env.integratorRepo, mainBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v1)

        def invalidType = 'Patch'
        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v1 as String, invalidType).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Invalid value for 'versionType' property: '${invalidType}', must be one of: major, minor or patch" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v1)
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchDoesNotExist() {
        def newVersionType = 'minor'
        def newVersion = '1.2.0'
        def newVersionTag = 'version-1.2.0'
        def newCommitMessage = 'Version 1.2.0'

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v1_releaseBranch)

        // 2. [integrator] check out a random branch
        env.integratorRepo.checkout(branch: randomBranch, startPoint: mainBranch, createBranch: true)

        // 3. [integrator] remove the maintenance branch
        env.integratorRepo.branch.remove(names: [v1_releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] a random branch is checked out
        def initialBranch = randomBranch
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 2. [integrator] the maintenace branch does not exist
        assert ! branchesOf(env.integratorRepo).asList().contains(v1_releaseBranch)

        // 3. [central] the maintenance branch exists
        assert branchesOf(env.centralRepo).asList().contains(v1_releaseBranch)

        // 4. [integrator] repo has no staged changes
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // 5. [integrator] the version tag does not exist
        def integratorTagsBeforeTest = tagsOf(env.integratorRepo).asList()
        assert ! integratorTagsBeforeTest.contains(newVersionTag)

        // 6. [central] the version tag does not exist
        def centralTagsBeforeTest = tagsOf(env.centralRepo).asList()
        assert ! centralTagsBeforeTest.contains(newVersionTag)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v1 as String, newVersionType).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator] the version tag was created
        def newIntegratorTags = tagsOf(env.integratorRepo).asList().minus(integratorTagsBeforeTest)

        assert 1 == newIntegratorTags.size()
        assert newVersionTag == newIntegratorTags.first()

        // 2. [integrator] the maintenance branch is checked out
        assert isCheckedOut(env.integratorRepo, v1_releaseBranch)

        // 3. [integrator] the commit with the version tag contains the version file
        def versionFileContains = fileContentsOnTag(env.integratorRepo, versionFile, newVersionTag)
        assert versionFileContains != null

        // 4. [integrator] the version file in this commit contains the expected version number
        assert versionFileContains.any { "version='${newVersion}'" == it }

        // 5. [integrator] the commit with the version tag has the correct commit message
        assert newCommitMessage == env.integratorRepo.log(maxCommits: 1).fullMessage.first()

        // 6. [integrator] the version tag is at the tip of the maintenance branch
        assert commitIdOfTag(env.integratorRepo, newVersionTag) == branchTip(env.integratorRepo, v1_releaseBranch).abbreviatedId

        // 7. [integrator, central] the maintenance branch is synchronized correctly
        assert remoteBranchesOf(env.integratorRepo).any { v1_remoteReleaseBranch }
        assert remoteBranchTip(env.integratorRepo, v1_remoteReleaseBranch) == branchTip(env.integratorRepo, v1_releaseBranch)

        assert branchesOf(env.centralRepo).any { v1_releaseBranch }
        assert remoteBranchTip(env.integratorRepo, v1_remoteReleaseBranch) == branchTip(env.centralRepo, v1_releaseBranch)

        // 8. [central] the version tag was created
        def newCentralTags = tagsOf(env.centralRepo).asList().minus(centralTagsBeforeTest)

        assert 1 == newCentralTags.size()
        assert newVersionTag == newCentralTags.first()
    }

    @Test
    public void test_createNewVersion_whenCentralReleaseBranchDoesNotExist() {
        def releaseBranch2 = 'maint_version-1'
        def PluginExt ext2 = new PluginExt(
            root: new RootExtension(
                workflowType: WorkflowType.VERSIONED.toString()),
            versioned: new VersionedExtension(
                centralGitRepository: ext.versioned.centralGitRepository,
                featureBranchPrefix: ext.versioned.featureBranchPrefix,
                preReleaseBranch: ext.versioned.preReleaseBranch,
                mainBranch: ext.versioned.mainBranch,
                mainIntegrationURLTrigger: ext.versioned.mainIntegrationURLTrigger,
                ciUsername: ext.versioned.ciUsername,
                ciPassword: ext.versioned.ciPassword,
                versionPrefix: ext.versioned.versionPrefix,
                versionFile: ext.versioned.versionFile,
                bumpVersionCommitMessage: ext.versioned.bumpVersionCommitMessage,
                maintenanceBranchPrefix: 'maint',
                maintenanceIntegrationURLTriggers: ext.versioned.maintenanceIntegrationURLTriggers))

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v1_releaseBranch)

        // 2. [integrator]check out a random branch
        env.integratorRepo.checkout(branch: randomBranch, startPoint: v2_releaseBranch, createBranch: true)

        // 3. [integrator] create a new release branch
        env.integratorRepo.branch.add(name: releaseBranch2)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] a random branch is checked out
        assert isCheckedOut(env.integratorRepo, randomBranch)

        // 2. [integrator] the release branch exists
        assert branchesOf(env.integratorRepo).asList().contains(releaseBranch2)

        // 3. [central] the release branch does not exist
        assert ! branchesOf(env.centralRepo).asList().contains(releaseBranch2)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v1)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext2, integratorGit, v1 as String, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Branch '${releaseBranch2}' not found in central repository!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v1)
    }

    @Test
    public void test_createNewVersion_whenWorkingDirectoryNotClean() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v1_releaseBranch)

        // 2. [integrator] make some uncomitted changes
        assert isCheckedOut(env.integratorRepo, mainBranch)
        createEmptyFileInRepo(env.integratorRepo)
        createEmptyFileInRepo(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the working directory contains some changes
        assert ! workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // 2. [integrator] the staging area is clean
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v1)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v1 as String, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v1)
    }

    @Test
    public void test_createNewVersion_whenStagingAreaNotClean() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v1_releaseBranch)

        // 2. [integrator] stage some changes
        assert isCheckedOut(env.integratorRepo, mainBranch)
        def someFile = createEmptyFileInRepo(env.integratorRepo)
        stageFile(env.integratorRepo, someFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the working directory is clean
        assert workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // 2. [integrator] the staging area contains some changes
        assert ! stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v1)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v1 as String, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v1)
    }

    @Test
    public void test_createNewVersion_whenVersionFileIsModified() {
        def newVersionType = 'minor'
        def newVersion = '2.1.0'
        def newVersionTag = 'version-2.1.0'
        def newCommitMessage = 'Version 2.1.0'

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v2_releaseBranch)

        // 2. [integrator] change the version file
        changeFile(env.integratorRepo, versionFile, "version='2.5.6'")
        assert [versionFile] == workingDirectoryOf(env.integratorRepo).asList()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v2)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v2 as String, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v2)
    }

    @Test
    public void test_createNewVersion_whenOnlyTheVersionFileIsStaged() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] make some changes on the maintenance branch
        commitAndPushSomething(env.integratorRepo, v2_releaseBranch)

        // 2. [integrator] modify and stage the version file
        changeFile(env.integratorRepo, versionFile, 'version=2.1.0')
        stageFile(env.integratorRepo, versionFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the main branch is checked out
        assert isCheckedOut(env.integratorRepo, mainBranch)

        // 2. [integrator] the staging area contains the version file
        assert [versionFile] == stagingAreaOf(env.integratorRepo).asList()

        // 3. [integrator] the working directory is clean
        assert workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v2)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v2 as String, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v2)
    }

    @Test
    public void test_createNewVersion_whenNoChanges() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the main branch is checked out
        assert isCheckedOut(env.integratorRepo, mainBranch)

        // 2. [integrator, central] the tip of maintenance branch is at the last version
        assert branchTip(env.centralRepo, v1_releaseBranch) == branchTip(env.integratorRepo, v1_releaseBranch)
        assert branchTip(env.integratorRepo, v1_releaseBranch) == commitOfTag(env.integratorRepo, 'version-1.1.1')
        assert branchTip(env.centralRepo, v1_releaseBranch) == commitOfTag(env.centralRepo, 'version-1.1.1')

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest(v1)

        def e = shouldFail {
            IntegratorActions.newCreateNewMaintenanceVersionAction(ext, integratorGit, v1 as String, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Cannot create new version because there are no changes since version '1.1.1'!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges(v1)
    }

    private static void createAndCommitFirstVersionFile(Grgit repo) {
        createAndCommitNewFile(repo, versionFile, "version='0.0.0'")
    }

    private static void commitAndPushSomething(Grgit repo, String branch) {
        commitAndPushSomething(repo, ext.versioned.centralGitRepository, branch)
    }

    private static final releaseBranches = [
        1: v1_releaseBranch,
        2: v2_releaseBranch
    ]

    private void captureRepoStateBeforeTest(int v) {
        assert v in (1..2)

        integratorBranchBeforeTest = env.integratorRepo.branch.current.name

        integratorTagsBeforeTest = tagsOf(env.integratorRepo)
        integratorReleaseBranchBeforeTest = branchTip(env.integratorRepo, releaseBranches[v])

        centralReleaseBranchBeforeTest = branchTip(env.centralRepo, releaseBranches[v])
        centralTagsBeforeTest = tagsOf(env.centralRepo)
    }

    private void checkResult_noRepoChanges(int v) {
        assert v in (1..2)

        // 1. [integrator] the same branch is checked out as before the test
        assert isCheckedOut(env.integratorRepo, integratorBranchBeforeTest)

        // 2. [integrator] the release branch is unchanged
        assert integratorReleaseBranchBeforeTest != null
        assert integratorReleaseBranchBeforeTest == branchTip(env.integratorRepo, releaseBranches[v])

        // 3. [integrator] no new tag was created
        assert integratorTagsBeforeTest != null
        assert tagsOf(env.integratorRepo).asList().minus(integratorTagsBeforeTest).isEmpty()

        // 4. [central] the release branch is unchanged
        assert centralReleaseBranchBeforeTest != null
        assert centralReleaseBranchBeforeTest == branchTip(env.centralRepo, releaseBranches[v])

        // 5. [central] no new tag was created
        assert centralTagsBeforeTest != null
        assert tagsOf(env.centralRepo).asList().minus(centralTagsBeforeTest).isEmpty()
    }
}
