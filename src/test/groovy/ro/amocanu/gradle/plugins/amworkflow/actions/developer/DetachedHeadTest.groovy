/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class DetachedHeadTest extends BaseTest {

    private static final ERROR_MESSAGE__DETACHED_HEAD = 'Cannot perform task while in detached head!'

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'use_the_Force'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = 'blow_out_the_Death_Star'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String someRandomBranch = 'do_or_do_not'

    private static final String releaseBranch = ext.continuous.releaseBranch

    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, null)
    }

    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createFeature_inDetachedHead() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the dev repository is in detached head
        def initialBranch = 'HEAD'
        env.devRepo.checkout(branch: initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert ERROR_MESSAGE__DETACHED_HEAD == e.getMessage()

        // 2. the dev repository is still in detached head
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    void test_goToFeature_inDetachedHead() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. the dev repository is in detached head
        def initialBranch = 'HEAD'
        env.devRepo.checkout(branch: initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert ERROR_MESSAGE__DETACHED_HEAD == e.getMessage()

        // 2. the dev repository is still in detached head
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_deliverFeature_inDetachedHead() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. the dev repository is in detached head
        def initialBranch = 'HEAD'
        env.devRepo.checkout(branch: initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert ERROR_MESSAGE__DETACHED_HEAD == e.getMessage()

        // 2. the dev repository is still in detached head
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_discardFeature_inDetachedHead() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature1 exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. the dev repository is in detached head
        def initialBranch = 'HEAD'
        env.devRepo.checkout(branch: initialBranch)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert ERROR_MESSAGE__DETACHED_HEAD == e.getMessage()

        // 2. the dev repository is still in detached head
        assert isCheckedOut(env.devRepo, initialBranch)
    }
}
