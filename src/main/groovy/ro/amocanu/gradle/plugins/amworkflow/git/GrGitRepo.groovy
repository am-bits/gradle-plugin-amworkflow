/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.git

import org.ajoberstar.grgit.Branch
import org.ajoberstar.grgit.Commit
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.Tag
import org.ajoberstar.grgit.operation.BranchAddOp
import org.ajoberstar.grgit.operation.BranchChangeOp
import org.ajoberstar.grgit.operation.BranchListOp
import org.ajoberstar.grgit.operation.MergeOp
import org.ajoberstar.grgit.operation.ResetOp
import org.ajoberstar.grgit.util.JGitUtil

import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation

/**
 * 
 * A Grgit implementation of the GitRepo interface
 *
 */
class GrGitRepo implements GitRepo {

    private static final DETACHED_HEAD = 'HEAD'

    private static final GRGIT_PROP_USERNAME = 'org.ajoberstar.grgit.auth.username'
    private static final GRGIT_PROP_PASSWORD = 'org.ajoberstar.grgit.auth.password'

    private final Grgit grgit
    private final String centralRepoName
    private final boolean hasCredentials

    static open(String repoDir, String centralRepoName) {
        return new GrGitRepo(repoDir, centralRepoName)
    }

    static open(String repoDir, String centralRepoName, String username, String password) {
        System.setProperty(GRGIT_PROP_USERNAME, username)
        System.setProperty(GRGIT_PROP_PASSWORD, password)
        return new GrGitRepo(repoDir, centralRepoName, true)
    }

    private GrGitRepo(String repoDir, String centralRepoName, boolean hasCredentials=false) {
        grgit = Grgit.open(dir: repoDir)
        this.centralRepoName = centralRepoName
    }

    private GrGitRepo(String centralRepoName) {
        grgit = Grgit.open()
        this.centralRepoName = centralRepoName
    }

    String rootDir() {
        return grgit.repository.rootDir.canonicalPath
    }

    void close() {
        grgit.close()

        if (hasCredentials) {
            System.clearProperty(GRGIT_PROP_USERNAME)
            System.clearProperty(GRGIT_PROP_PASSWORD)
        }
    }

    Iterable<String> allTags() {
        return grgit.tag.list()*.name
    }

    Iterable<String> allAncestorTags() {
        def resolver = grgit.resolve
        def head = grgit.head()
        def ancestorTags = grgit.tag.list().findAll { tag ->
            grgit.isAncestorOf(resolver.toCommit(tag), head)
        }

        return ancestorTags*.name
    }

    boolean isTag(String tagName) {
        return allTags().contains(tagName)
    }

    Iterable<String> stagedChanges() {
        return grgit.status().staged.allChanges
    }

    Iterable<String> unstagedChanges() {
        return grgit.status().unstaged.allChanges
    }

    Iterable<String> changes() {
        def all = []

        def status = grgit.status()
        all.addAll(status.staged.allChanges)
        all.addAll(status.unstaged.allChanges)

        return all
    }

    void commitSingleFile(String file, String commitMessage) {
        assert file in changes()
        assert stagedChanges().minus(file).isEmpty()

        if (file in unstagedChanges()) {
            grgit.add(patterns: [file])
        }

        grgit.commit(message: commitMessage)
    }

    void createTag(String tagName) {
        assert !(tagName in allTags())

        grgit.tag.add(name: tagName, annotate: false)
    }

    String commitIdOfTag(String tagName) {
        assert tagName in allTags()
        
        Tag tag = grgit.resolve.toTag(tagName)
        return grgit.resolve.toCommit(tag).abbreviatedId
    }

    String commitIdOfBranch(String branchName) {
        assert branchName in localBranches()

        Branch branch = grgit.resolve.toBranch("refs/heads/${branchName}")
        return grgit.resolve.toCommit(branch).abbreviatedId
    }

    String commitIdOfRemoteBranch(String remoteBranchName) {
        assert remoteBranchName in remoteTrackingCentralBranches()

        Branch branch = grgit.resolve.toBranch("refs/remotes/${remoteBranchName}")
        return grgit.resolve.toCommit(branch).abbreviatedId
    }

    String currentBranch() {
        def branch = grgit.branch.current.name
        return branch != DETACHED_HEAD ? branch : null
    }

    boolean isDetachedHead() {
        return grgit.branch.current.name == DETACHED_HEAD
    }

    void checkoutBranch(String branchName) {
        grgit.checkout(branch: branchName)
    }

    void createLocalAndRemoteBranchAtBranch(String branchName, String startPointBranch) {
        def startPoint = commitIdOfBranch(startPointBranch)
        createLocalAndRemoteBranch(branchName, startPoint)
    }

    void createLocalAndRemoteBranchAtRemoteBranch(String branchName, String startPointRemoteBranch) {
        def startPoint = commitIdOfRemoteBranch(startPointRemoteBranch)
        createLocalAndRemoteBranch(branchName, startPoint)
    }

    void createLocalAndRemoteBranchAtTag(String branchName, String startPointTag) {
        def startPoint = commitIdOfTag(startPointTag)
        createLocalAndRemoteBranch(branchName, startPoint)
    }

    private void createLocalAndRemoteBranch(String branchName, String startPoint) {
        assert startPoint != null
        assert ! isLocalBranch(branchName)
        assert ! isCentralBranch(branchName)

        grgit.branch.add(name: branchName, startPoint: startPoint, mode: BranchAddOp.Mode.NO_TRACK)
        grgit.push(remote: centralRepoName, refsOrSpecs: [branchName])

        def remoteBranchFullName = "refs/remotes/${centralRepoName}/${branchName}" 
        grgit.branch.change(name: branchName, startPoint: remoteBranchFullName, mode: BranchChangeOp.Mode.TRACK)
    }

    void createLocalBranchTrackingRemote(String branchName, String remoteBranchName) {
        assert ! isLocalBranch(branchName)

        Branch remoteBranchObj = grgit.resolve.toBranch("refs/remotes/${remoteBranchName}")
        assert remoteBranchObj != null

        grgit.branch.add(name: branchName, startPoint: remoteBranchObj.fullName, mode: BranchAddOp.Mode.TRACK)
    }

    @Override
    public boolean isLocalBranchTrackingRemoteBranch(String branchName, String remoteBranchName) {
        assert branchName in localBranches()
        assert remoteBranchName in remoteTrackingCentralBranches()

        Branch branch = grgit.resolve.toBranch(branchName)
        String remoteBranchFullName = "refs/remotes/${remoteBranchName}"
        return branch.trackingBranch ? (branch.trackingBranch.fullName == remoteBranchFullName) : false
    }

    void fetchAllBranchesAndPrune() {
        /*
         * Note: 
         * (Assuming centralRepo='origin' for readability)
         * 
         * Fetch with 'prune: true' removes the branch pointed at 
         * by origin/HEAD. (By default, this is the origin/master branch.)
         *  
         * This seems to be a JGit issue (JGit 4.4.0), which Grgit inherits.
         * TODO - Submit a JGit bug report!
         * 
         * The second fetch is a workaround to bring the origin/master branch back. 
         */
        grgit.fetch(remote: centralRepoName, prune: true)
        grgit.fetch(remote: centralRepoName)
    }

    void fetchAllTags() {
        grgit.fetch(remote: centralRepoName, refSpecs: ['refs/tags/*:refs/tags/*'])
    }

    void removeBranch(String branchName) {
        assert branchName in localBranches()
        grgit.branch.remove(names: [branchName], force: true)
    }

    void removeBranchOnCentral(String branchName) {
        assert isCentralBranch(branchName)
        grgit.push(remote: centralRepoName, refsOrSpecs: [":refs/heads/${branchName}"])
    }

    Iterable<String> localBranches() {
        return grgit.branch.list(mode: BranchListOp.Mode.LOCAL).collect { it.name }
    }

    Iterable<String> remoteTrackingCentralBranches() {
        return grgit.branch.list(mode: BranchListOp.Mode.REMOTE).collect { it.name }.findAll { it ==~ "${centralRepoName}/.+" }
    }

    boolean isLocalBranch(String branchName) {
        return branchName in localBranches()
    }

    boolean isCentralBranch(String branchName) {
        String remoteTrackingBranchName = "${centralRepoName}/${branchName}"
        return remoteTrackingBranchName in remoteTrackingCentralBranches()
    }

    void pushBranchToCentral(String branchName) {
        assert branchName in localBranches()
        grgit.push(remote: centralRepoName, refsOrSpecs: [branchName])
    }

    void pushTagsToCentral() {
        grgit.push(remote: centralRepoName, tags: true)
    }

    void pushBranchToCentralWithTags(String branchName) {
        assert branchName in localBranches()
        grgit.push(remote: centralRepoName, refsOrSpecs: [branchName], tags: true)
    }

    Relation relation(String firstCommitId, String secondCommitId) {
        assert firstCommitId != null
        assert secondCommitId != null

        if (firstCommitId == secondCommitId) {
            return Relation.SAME
        }

        commitIdIsUnambiguous(firstCommitId)
        commitIdIsUnambiguous(secondCommitId)

        Commit firstCommit = grgit.resolve.toCommit(firstCommitId)
        Commit secondCommit = grgit.resolve.toCommit(secondCommitId)

        assert firstCommit != null
        assert secondCommit != null

        if (JGitUtil.isAncestorOf(grgit.repository, firstCommit, secondCommit)) {
            return Relation.ANCESTOR
        }

        if (JGitUtil.isAncestorOf(grgit.repository, secondCommit, firstCommit)) {
            return Relation.DESCENDANT
        }

        return Relation.NONE
    }

    private void commitIdIsUnambiguous(String commitId) {
        assert ! isLocalBranch(commitId) : "A branch is named like a commit id: '${commitId}'!"
        assert ! isTag(commitId) : "A tag is named like a commit id: '${commitId}'!"
    }

    void fastForwardCurrentBranchTo(String someOtherBranch) {
        assert (someOtherBranch in localBranches()) || (someOtherBranch in remoteTrackingCentralBranches()) 
        grgit.merge(head: someOtherBranch, mode: MergeOp.Mode.ONLY_FF)
    }

    void resetCurrentBranchToCommit(String commitId) {
        grgit.reset(commit: commitId, mode: ResetOp.Mode.HARD)
    }
}
