package ro.amocanu.gradle.plugins.amworkflow.actions.debug

import static org.junit.Assert.*

import org.ajoberstar.grgit.Grgit
import org.junit.Ignore
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil
import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.actions.integrator.IntegratorActions
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

/**
 * 
 * Not a real test class, was used for debugging 
 * <a href="https://bitbucket.org/am-bits/gradle-plugin-amworkflow/issues/1">this</a> issue.
 * 
 * (Kept it in case we need to do more debugging in the future.)
 *
 */
class DebugIssue1 {

    // a repository with a central that blocks pushes on 'master'
    private static final REPO = "D:\\Repositories\\amworkflow-test-repo"
    
    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            //gitUsername: '',
            //gitPassword: '',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))
    
    @Ignore
    @Test
    public void test() {      
        GitRepo repo = GrGitRepo.open(REPO, ext.versioned.centralGitRepository)
        
        Grgit grgit = Grgit.open(dir: REPO)
        //System.setProperty('org.ajoberstar.grgit.auth.username', '')
        //System.setProperty('org.ajoberstar.grgit.auth.password', '')
        
        String featureName = System.currentTimeMillis()
        
        DeveloperActions.newCreateFeatureAction(ext, repo, featureName).run()
        GitTestUtil.commitAndPushSomething(grgit, 'origin', 'feature/' + featureName)
        DeveloperActions.newDeliverFeatureAction(ext, repo).run()
        
        IntegratorActions.newPrepareFeatureIntegrationAction(ext, repo).run()
        IntegratorActions.newIntegrateFeatureAction(ext, repo).run()
    }

}
