/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
class CreateFeature extends AbstractFeatureAction {

    CreateFeature(Configuration conf, GitRepo git, String featureName) {
        super(conf, git, featureName)
    }

    @Override
    protected void runFeatureAction() {
        featureMustNotExist()
        featureBranchMustNotExistWithADifferentCase()
        featureBranchMustNotConflictWithOtherBranches()
        createLocalFeatureBranch()
        pushFeatureBranchToCentral()
    }

    private void featureMustNotExist() {
        def exception = new GradleException("Feature '${featureName}' already exists!")

        if (git.isLocalBranch(featureBranch)) {
            throw exception
        }

        if (git.isCentralBranch(featureBranch)) {
            throw exception
        }
    }

    private void featureBranchMustNotExistWithADifferentCase() {
        def branch = git.localBranches().find { featureBranch.equalsIgnoreCase(it) }
        if (branch) {
            throw new GradleException("Feature branch '${featureBranch}' already exists with a different case: '${branch}'!")
        }

        def remoteFeatureBranch = "${conf.centralGitRepository}/${featureBranch}"
        def remoteBranch = git.remoteTrackingCentralBranches().find { remoteFeatureBranch.equalsIgnoreCase(it) }
        if (remoteBranch) {
            throw new GradleException("Remote-tracking feature branch '${remoteFeatureBranch}' already exists with a different case: '${remoteBranch}'!")
        }
    }

    private void featureBranchMustNotConflictWithOtherBranches() {
        def errorMessage = { conflictingBranch ->
            "Feature branch '${featureBranch}' cannot be created, it conflicts with existing branch '${conflictingBranch}'!"
        }

        assert featureBranch.contains('/')

        def conflictingLocalBranch1 = featureBranch[0..featureBranch.lastIndexOf('/')-1]
        def conflictingLocalBranch2 = featureBranch + '/'
        
        def conflictingLocalBranch = git.localBranches().find { branch -> 
            branch == conflictingLocalBranch1 ||
            branch.startsWith(conflictingLocalBranch2)
        }

        if (conflictingLocalBranch) {
            throw new GradleException(errorMessage(conflictingLocalBranch))
        }

        def conflictingRemoteBranch1 = "${conf.centralGitRepository}/${conflictingLocalBranch1}"
        def conflictingRemoteBranch2 = "${conf.centralGitRepository}/${conflictingLocalBranch2}"
        
        def conflictingRemoteBranch = git.remoteTrackingCentralBranches().find { branch -> 
            branch == conflictingRemoteBranch1 ||
            branch.startsWith(conflictingRemoteBranch2)
        }

        if (conflictingRemoteBranch) {
            throw new GradleException(errorMessage(conflictingRemoteBranch))
        }
    }

    private void createLocalFeatureBranch() {
        def remoteReleaseBranch = "${conf.centralGitRepository}/${conf.releaseBranch}"

        git.createLocalAndRemoteBranchAtRemoteBranch(featureBranch, remoteReleaseBranch)
        git.checkoutBranch(featureBranch)

        println "Created branch '${featureBranch}' from '${remoteReleaseBranch}'"
    }

    private void pushFeatureBranchToCentral() {
        def remoteName = conf.centralGitRepository
        git.pushBranchToCentral(featureBranch)

        println "Pushed branch '${featureBranch}' to '${remoteName}'"
    }
}
