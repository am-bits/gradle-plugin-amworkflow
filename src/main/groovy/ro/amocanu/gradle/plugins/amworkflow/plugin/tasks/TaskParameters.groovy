/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.plugin.tasks

class TaskParameters {
    public static final FEATURE_NAME = [
        id: 'featureName',
        usage: '-PfeatureName=<myFeature>'
    ]

    public static final MAINTAINED_VERSION = [
        id: 'maintainedVersion',
        usage: '-PmaintainedVersion=<versionNumber>'
    ]

    public static final VERSION_TYPE = [
        id: 'versionType',
        usage: '-PversionType=major|minor|patch',
        usage_maint: '-PversionType=minor|patch'
    ]

    public static final ROLE = [
        id: 'role',
        value: 'INTEGRATOR',
        usage: '-Prole=INTEGRATOR'
    ]
}
