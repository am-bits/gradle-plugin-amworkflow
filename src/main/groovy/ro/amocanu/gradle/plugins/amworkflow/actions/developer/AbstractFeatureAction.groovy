/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
abstract class AbstractFeatureAction extends AbstractDeveloperAction {

    protected final String featureName
    protected final String featureBranch

    AbstractFeatureAction(Configuration conf, GitRepo git, String featureName) {
        super (conf, git)
        this.featureName = featureName
        this.featureBranch = conf.featureBranchFromFeatureName(featureName)
    }

    protected void runDeveloperAction() {
        if (featureName.contains('/')) {
            throw new GradleException("Feature name cannot contain the '/' character!")
        }

        runFeatureAction()
    }
    
    abstract protected void runFeatureAction()
}
