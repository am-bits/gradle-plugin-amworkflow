/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
class DiscardFeature extends AbstractFeatureAction {

    DiscardFeature(Configuration conf, GitRepo git, String featureName) {
        super(conf, git, featureName)
    }

    @Override
    protected void runFeatureAction() {
        featureMustExist()
        featureMustNotBeDelivered()
        deleteFeatureBranch()
    }

    private void featureMustExist() {
        if (!git.isCentralBranch(featureBranch)) {

            String remotePrefix = conf.centralGitRepository + '/'
            String remoteFeatureBranch = remotePrefix + featureBranch
            String potentialRemoteFeatureBranch = git.remoteTrackingCentralBranches().find { remoteFeatureBranch.equalsIgnoreCase(it) }

            if (!potentialRemoteFeatureBranch) {
                throw new GradleException("Feature '${featureName}' does not exist!")
            }
            
            String potentialFeatureBranch = potentialRemoteFeatureBranch[remotePrefix.size()..-1]
            String potentialFeature = conf.featureNameFromFeatureBranch(potentialFeatureBranch)
            throw new GradleException("Feature '${featureName}' does not exist! (Did you mean '${potentialFeature}'?)")
        }
    }

    private void featureMustNotBeDelivered() {
        if (git.isCentralBranch(conf.preReleaseBranch)) {
            def remoteFeatureBranch = "${conf.centralGitRepository}/${featureBranch}"
            def remotePreReleaseBranch = "${conf.centralGitRepository}/${conf.preReleaseBranch}"

            if (git.commitIdOfRemoteBranch(remoteFeatureBranch) == git.commitIdOfRemoteBranch(remotePreReleaseBranch)) {
                throw new GradleException("Cannot delete branch '${featureBranch}' while the feature is delivered but not yet integrated!")
            }
        }
    }

    private void deleteFeatureBranch() {
        git.removeBranchOnCentral(featureBranch)

        if (git.isLocalBranch(featureBranch)) {
            git.removeBranch(featureBranch)
        }
    }
}
