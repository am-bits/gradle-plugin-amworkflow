/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope

import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
class ContinuousIntegrateFeature extends IntegrateFeature {

    ContinuousIntegrateFeature(Configuration conf, GitRepo git) {
        super(conf, git)
    }
    
    @Override
    protected void postActions() {
        git.fetchAllTags()

        def tag = conf.releaseNameFromReleaseId(timestamp())

        assert !(tag in git.allTags())

        git.createTag(tag)
        git.pushTagsToCentral()
    }

    private String timestamp() {
        DateTimeFormatter timestampFormat = DateTimeFormatter.ofPattern("yyyyMMddhhmmss")
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC)

        return now.format(timestampFormat)
    }
}
