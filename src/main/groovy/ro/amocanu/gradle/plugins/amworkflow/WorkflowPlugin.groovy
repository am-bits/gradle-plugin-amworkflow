/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project

import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.TaskParameters
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.CreateFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.CreateMaintenanceBranchTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.CreateMaintenanceFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.DeliverFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.DiscardFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.DiscardMaintenanceFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.GoToFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.GoToMaintenanceFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer.SyncWithCentralTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.CancelIntegrationTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.CancelMaintenanceIntegrationTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.ContinuousIntegrateFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.CreateNewMaintenanceVersionTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.CreateNewVersionTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.IntegrateFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.IntegrateMaintenanceFeatureTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.PrepareFeatureIntegrationTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.PrepareMaintenanceFeatureIntegrationTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.ShowIntegrationStatusTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.integrator.ShowMaintenanceIntegrationStatusTask

class WorkflowPlugin implements Plugin<Project> {

    private static final String ROOT_EXTENSION = 'amworkflow'
    private static final String CONTINUOUS_EXTENSION = 'continuous'
    private static final String VERSIONED_EXTENSION = 'versioned'

    private static final String TASK_GROUP_INTEGRATOR = 'Integrator Workflow'
    private static final String TASK_GROUP_CONTINUOUS = 'Continuous Workflow'
    private static final String TASK_GROUP_VERSIONED = 'Versioned Workflow'

    private static final String PROPERTY_ROLE = TaskParameters.ROLE.id
    private static final String ROLE_INTEGRATOR = TaskParameters.ROLE.value

    void apply(Project project) {
        createExtensions(project)
        project.afterEvaluate {
            addTasks(project)
        }
    }
    
    private void createExtensions(Project project) {
        project.extensions.create(ROOT_EXTENSION, RootExtension)
        project."${ROOT_EXTENSION}".extensions.create(CONTINUOUS_EXTENSION, ContinuousExtension)
        project."${ROOT_EXTENSION}".extensions.create(VERSIONED_EXTENSION, VersionedExtension)
    }

    private void addTasks(Project project) {
        if (project.hasProperty(PROPERTY_ROLE)) {
            def role = project.property(PROPERTY_ROLE)
            if (role != ROLE_INTEGRATOR) {
                throw new GradleException("Unknown role: '${role}'")
            }
            addIntegratorTasks(project)
        } else {
            addDeveloperTasks(project)
        }
    }

    private void addIntegratorTasks(Project project) {
        project.task(PrepareFeatureIntegrationTask.NAME, type: PrepareFeatureIntegrationTask, group: TASK_GROUP_INTEGRATOR)
        project.task(CancelIntegrationTask.NAME, type: CancelIntegrationTask, group: TASK_GROUP_INTEGRATOR)
        project.task(ShowIntegrationStatusTask.NAME, type: ShowIntegrationStatusTask, group: TASK_GROUP_INTEGRATOR)

        def wt = PluginExt.workflowTypeOf(project)
        switch (wt) {
            case WorkflowType.CONTINUOUS:
                project.task(IntegrateFeatureTask.NAME, type: ContinuousIntegrateFeatureTask, group: TASK_GROUP_INTEGRATOR, dependsOn: [PrepareFeatureIntegrationTask.NAME])
                break
            case WorkflowType.VERSIONED:
                project.task(IntegrateFeatureTask.NAME, type: IntegrateFeatureTask, group: TASK_GROUP_INTEGRATOR, dependsOn: [PrepareFeatureIntegrationTask.NAME])
                project.task(CreateNewVersionTask.NAME, type: CreateNewVersionTask, group: TASK_GROUP_INTEGRATOR)
                project.task(CreateNewMaintenanceVersionTask.NAME, type: CreateNewMaintenanceVersionTask, group: TASK_GROUP_INTEGRATOR)
                project.task(PrepareMaintenanceFeatureIntegrationTask.NAME, type: PrepareMaintenanceFeatureIntegrationTask, group: TASK_GROUP_INTEGRATOR)
                project.task(IntegrateMaintenanceFeatureTask.NAME, type: IntegrateMaintenanceFeatureTask, group: TASK_GROUP_INTEGRATOR, dependsOn: [PrepareMaintenanceFeatureIntegrationTask.NAME])
                project.task(CancelMaintenanceIntegrationTask.NAME, type: CancelMaintenanceIntegrationTask, group: TASK_GROUP_INTEGRATOR)
                project.task(ShowMaintenanceIntegrationStatusTask.NAME, type: ShowMaintenanceIntegrationStatusTask, group: TASK_GROUP_INTEGRATOR)
                break
            default:
                assert false
        }
    }

    private void addDeveloperTasks(Project project) {
        def wt = PluginExt.workflowTypeOf(project)
        switch (wt) {
            case WorkflowType.CONTINUOUS:
                project.task(SyncWithCentralTask.NAME, type: SyncWithCentralTask, group: TASK_GROUP_CONTINUOUS)
                project.task(CreateFeatureTask.NAME, type: CreateFeatureTask, group: TASK_GROUP_CONTINUOUS, dependsOn: [SyncWithCentralTask.NAME])
                project.task(GoToFeatureTask.NAME, type: GoToFeatureTask, group: TASK_GROUP_CONTINUOUS, dependsOn: [SyncWithCentralTask.NAME])
                project.task(DeliverFeatureTask.NAME, type: DeliverFeatureTask, group: TASK_GROUP_CONTINUOUS, dependsOn: [SyncWithCentralTask.NAME])
                project.task(DiscardFeatureTask.NAME, type: DiscardFeatureTask, group: TASK_GROUP_CONTINUOUS, dependsOn: [SyncWithCentralTask.NAME])
                break
            case WorkflowType.VERSIONED:
                project.task(SyncWithCentralTask.NAME, type: SyncWithCentralTask, group: TASK_GROUP_VERSIONED)
                project.task(CreateFeatureTask.NAME, type: CreateFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(GoToFeatureTask.NAME, type: GoToFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(DeliverFeatureTask.NAME, type: DeliverFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(DiscardFeatureTask.NAME, type: DiscardFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(CreateMaintenanceBranchTask.NAME, type: CreateMaintenanceBranchTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(CreateMaintenanceFeatureTask.NAME, type: CreateMaintenanceFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(GoToMaintenanceFeatureTask.NAME, type: GoToMaintenanceFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                project.task(DiscardMaintenanceFeatureTask.NAME, type: DiscardMaintenanceFeatureTask, group: TASK_GROUP_VERSIONED, dependsOn: [SyncWithCentralTask.NAME])
                break
            default:
                assert false
        }
    }
}
