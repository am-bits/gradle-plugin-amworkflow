/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.operation.ResetOp
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.DevTestHelper
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@RunWith(Parameterized.class)
class DeliverFeatureTest extends BaseTest {

    private static final PluginExt ext1 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'support',
            maintenanceIntegrationURLTriggers: [:]))

    private TestEnv env
    private GitRepo devGit
    private GitRepo devGit2
    private GitRepo integratorGit   
    private TestHelper helper
    private DevTestHelper helper2

    private PluginExt ext
   
    private final String central
    private final String featurePrefix
    private final String releaseBranch
    private final String remoteReleaseBranch

    private final String preReleaseBranch
    private final String remotePreReleaseBranch

    private static final String featureName1 = 'yellow-$ubmarine'
    private final String featureBranch1
    private final String remoteFeatureBranch1

    private static final String featureName2 = 'heyJude!'
    private final String featureBranch2
    private final String remoteFeatureBranch2

    private static final String someRandomBranch = 'lucyInTheSkyWithDi@monds'
    
    private final String pseudoFeaturePrefix
    
    private final String pseudoPreReleaseBranch
    private final String remotePseudoPreReleaseBranch

    @Parameterized.Parameters
    public static Collection pluginExtensions() {
        return [ext1, ext2]
    }

    public DeliverFeatureTest(PluginExt ext) {
        this.ext = ext

        switch (ext.root.workflowType) {
            case WorkflowType.CONTINUOUS.toString():
                this.central = ext.continuous.centralGitRepository
                this.featurePrefix = ext.continuous.featureBranchPrefix
                this.releaseBranch = ext.continuous.releaseBranch
                this.preReleaseBranch = ext.continuous.preReleaseBranch
                this.pseudoPreReleaseBranch = 'PRErELEASE'
                this.pseudoFeaturePrefix = "featurE"
                break
            case WorkflowType.VERSIONED.toString():
                this.central = ext.versioned.centralGitRepository
                this.featurePrefix = ext.versioned.featureBranchPrefix
                this.releaseBranch = ext.versioned.mainBranch
                this.preReleaseBranch = ext.versioned.preReleaseBranch
                this.pseudoPreReleaseBranch = 'Pre'
                this.pseudoFeaturePrefix = "tASk"
                break
            default:
                assert false
        }

        this.remoteReleaseBranch = "${central}/${releaseBranch}"
        this.remotePreReleaseBranch = "${central}/${preReleaseBranch}"
        this.remotePseudoPreReleaseBranch = "${central}/${pseudoPreReleaseBranch}"

        this.featureBranch1 = featureBranch(featureName1)
        this.featureBranch2 = featureBranch(featureName2)

        this.remoteFeatureBranch1 = remoteFeatureBranch(featureName1)
        this.remoteFeatureBranch2 = remoteFeatureBranch(featureName2)
    }

    private String featureBranch(String featureName) {
        return "${featurePrefix}/${featureName}"
    }

    private String remoteFeatureBranch(String featureName) {
        return "${central}/${featurePrefix}/${featureName}"
    }

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)
        
        devGit2 = GrGitRepo.open(env.devRepo2.repository.rootDir.absolutePath, central)
        helper2 = new DevTestHelper(env, ext, devGit2)
    }

    @After
    public void tearDown() {
        devGit2.close()
        integratorGit.close()
        devGit.close()

        env.tearDown()
    }

    @Test
    public void test_deliverFromNonFeatureBranch() {
        env.devRepo.checkout(branch: someRandomBranch, createBranch: true)

        // Preconditions
        // 1. a non-feature branch is checked-out
        def initialBranch = someRandomBranch
        assert isCheckedOut(env.devRepo, initialBranch)
        assert !(initialBranch ==~ "${featurePrefix}/.+")

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "The checked-out branch '${initialBranch}' is not a feature branch, it cannot be delivered!" == e.getMessage()

        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_deliverIncipientFeature() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. before committing anything on the feature branch, try to deliver the feature
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert 'Nothing to deliver!' == e.getMessage()

        // 2. the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
    }

    @Test
    public void test_deliverSameFeatureTwice() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. work on feature
        helper.test_workOnFeatureBranch(featureBranch1)

        // 3. deliver feature
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 4. go back to feature branch
        helper.test_goToFeature(featureName1, featureBranch1)

        // 5. ... and try to deliver it again
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Another release is in progress... Try again later." == e.getMessage()
        
        // 2. the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
    }

    @Test
    public void test_deliverTwoDifferentFeaturesAtTheSameTime() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create, develop and deliver feature1
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. create and develop feature2
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)

        // 3. try to deliver feature2
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Another release is in progress... Try again later." == e.getMessage()
    }

    @Test
    public void test_deliverFeatureWhenAheadOfUpstream() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranchLocally(featureBranch1)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch is ahead of its upstream
        assert isDescendant(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))

        // Test
        // ---------------------------------------------------------------------
        def featureBranchBeforeTest = branchTip(env.devRepo, featureBranch1)
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "To deliver the feature, you first need to push changes on branch '${featureBranch1}' to the central repository!" == e.getMessage()

        // 2. on dev, the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
        
        // 3. on dev, the feature branch did not move
        assert featureBranchBeforeTest == branchTip(env.devRepo, featureBranch1)
    }

    @Test
    public void test_deliverFeatureWhenBehindUpstream() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch is behind its upstream
        assert isAncestor(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))

        // Test
        // ---------------------------------------------------------------------
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void test_deliverFeatureWhenParallelWithUpstream() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        helper.test_workOnFeatureBranchLocally(featureBranch1)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the local feature branch and its upstream evolved in parallel
        assert ! isAncestor(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))

        assert ! isDescendant(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))

        assert branchTip(env.devRepo, featureBranch1) != branchTip(env.devRepo, remoteFeatureBranch1)

        // Test
        // ---------------------------------------------------------------------
        def featureBranchBeforeTest = branchTip(env.devRepo, featureBranch1)
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "To deliver the feature, you first need to merge '${remoteFeatureBranch1}' to '${featureBranch1}'!" == e.getMessage()

        // 2. on dev, the feature branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
        
        // 3. on dev, the feature branch did not move
        assert featureBranchBeforeTest == branchTip(env.devRepo, featureBranch1)
    }

    @Test
    public void test_deliverFeaturesWhenReleaseBranchNeedsMerging() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create and develop feature1 and feature2 at the same time
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)

        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        
        // 2. deliver and integrate feature1
        helper.test_goToFeature(featureName1, featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        // 3. try to deliver feature2
        helper.test_goToFeature(featureName2, featureBranch2)

        def feature2BranchBeforeTest = branchTip(env.devRepo, featureBranch2)
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "To deliver the feature, you first need to merge '${remoteReleaseBranch}' to '${featureBranch2}'!" == e.getMessage()
        
        // 2. on dev, the feature2 branch is still checked-out
        assert isCheckedOut(env.devRepo, featureBranch2)
        
        // 3. on dev, the feature2 branch did not move
        assert feature2BranchBeforeTest == branchTip(env.devRepo, featureBranch2)
    }

    @Test
    public void test_deliverFeature_whenAlreadyDeliveredBySomeoneElseAndIntegrated() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] the first developer creates and implements the feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)

        // 2. [dev2] the second developer then checks out and delivers the feature
        helper2.test_goToFeature(featureName1, featureBranch1)
        helper2.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 3. [integrator, central] the feature is integrated
        helper.test_integrateFeature()

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a local feature branch exists and is checked-out
        assert branchesOf(env.devRepo).asList().contains(featureBranch1)
        assert isCheckedOut(env.devRepo, featureBranch1)

        // 2. [dev] a the remote-tracking feature branch exists
        assert remoteBranchesOf(env.devRepo).asList().contains(remoteFeatureBranch1)

        // 3. [central] the feature branch doesn't exist
        assert ! branchesOf(env.centralRepo).asList().contains(featureBranch1)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. [dev] the correct error message is thrown
        assert "The checked-out branch '${releaseBranch}' is not a feature branch, it cannot be delivered!" == e.getMessage()

        // 2. [dev] the release branch is checked-out
        assert isCheckedOut(env.devRepo, releaseBranch)
    }

    @Test
    public void test_deliverFeatures_whenPseudoPreReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create and push the pseudo-pre-release branch
        env.devRepo.branch.add(name: pseudoPreReleaseBranch)
        pushBranchToRemote(env.devRepo, central, pseudoPreReleaseBranch)
        
        // 2. [dev] create and develop feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists with a different case
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        assert preReleaseBranch != pseudoPreReleaseBranch
        assert preReleaseBranch.equalsIgnoreCase(pseudoPreReleaseBranch)

        // 2. [dev] the feature branch is checked out
        assert isCheckedOut(env.devRepo, featureBranch2)
        
        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        def commitId = branchTip(env.centralRepo, pseudoPreReleaseBranch).abbreviatedId
        assert "Branch '${remotePseudoPreReleaseBranch}' on commit '${commitId}' shadows the pre-release branch! No feature can be delivered unless branch '${remotePseudoPreReleaseBranch}' is removed." == e.message

        // 2. [central] the pre-release branch still does not exist
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. [central] the pseudo-pre-release branch still exists
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        // 4. [dev] the feature branch is still checked out
        assert isCheckedOut(env.devRepo, featureBranch2)
    }

    @Test
    public void test_deliverFromPseudoFeatureBranch() {
        String pseudoFeatureBranch = "${pseudoFeaturePrefix}/${featureName2}"
        
        def initialBranch = pseudoFeatureBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a branch is checked-out that has the feature prefix with the wrong case
        assert isCheckedOut(env.devRepo, initialBranch)
        
        String fullFeaturePrefix = featurePrefix + '/'
        assert initialBranch.size() > fullFeaturePrefix.size()
       
        String prefix = initialBranch[0..fullFeaturePrefix.size()-1]
        assert fullFeaturePrefix != prefix
        assert fullFeaturePrefix.equalsIgnoreCase(prefix) 

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "The checked-out branch '${releaseBranch}' is not a feature branch, it cannot be delivered!" == e.getMessage()

        // 2. the release branch is now checked-out
        assert isCheckedOut(env.devRepo, releaseBranch)

        // 3. the initial branch has been removed
        assert ! branchesOf(env.devRepo).asList().contains(initialBranch)
    }

    @Test
    public void test_deliverFromFeatureBranch_whenFeatureNameHasWrongCase() {
        String wrongCaseFeatureName2 = "heyjude!"
        String wrongCaseFeatureBranch2 = "${featurePrefix}/${wrongCaseFeatureName2}"
        
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)

        def initialBranch = wrongCaseFeatureBranch2
        env.devRepo.checkout(branch: initialBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a branch is checked-out that has the feature name with the wrong case
        assert isCheckedOut(env.devRepo, initialBranch)
        
        String fullFeaturePrefix = featurePrefix + '/'
        assert initialBranch.size() > fullFeaturePrefix.size()
       
        String prefix = initialBranch[0..fullFeaturePrefix.size()-1]
        assert fullFeaturePrefix == prefix

        String featureName = initialBranch[fullFeaturePrefix.size()..-1]
        assert featureName2 != featureName
        assert featureName2.equalsIgnoreCase(featureName)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName}' does not exist in the central repo! (Did you mean to check out the feature branch '${featureBranch2}' instead?)" == e.getMessage()

        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }
}
