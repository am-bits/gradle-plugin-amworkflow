/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

@PackageScope
class CancelIntegration extends BaseAction {

    CancelIntegration (Configuration conf, GitRepo git) {
        super(conf, git)
    }

    @Override
    void run() {
        fetchFromCentralAndPrune()
        if (!(git.isCentralBranch(conf.preReleaseBranch))) {
            throw new GradleException('No features delivered for release found in the central repository!')
        }

        deletePreReleaseBranch()
    }

    private void deletePreReleaseBranch() {
        def preRelease = conf.preReleaseBranch

        if (git.isLocalBranch(preRelease)) {
            if (git.currentBranch() == preRelease) {
                mustHaveRemoteReleaseBranch()
                repoMustNotHaveChangesOnPreReleaseBranch()
                checkOutBranch(conf.releaseBranch)
            }
            git.removeBranch(preRelease)
        }

        git.removeBranchOnCentral(preRelease)
    }

    private void repoMustNotHaveChangesOnPreReleaseBranch() {
        if (git.changes()) {
            throw new GradleException('Cannot cancel integration because you are on the pre-release branch and there are changes!')
        }
    }
}
