/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitAndPushSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class BranchTagAmbiguityWhenMissingLocalReleaseBranchTest extends BaseTest {

    private static final ERROR_MESSAGE__NO_RELEASE_ON_CENTRAL = { releaseBranch -> "Branch '${releaseBranch}' not found on central repository!" }

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'support',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName1 = 'stealtheblueprints'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String someRandomBranch = 'makealotofnoise'

    private static final String central = ext.versioned.centralGitRepository
    private static final String releaseBranch = ext.versioned.mainBranch
    private static final String remoteReleaseBranch = "${central}/${releaseBranch}"
    private static final String preReleaseBranch = ext.versioned.preReleaseBranch

    private TestEnv env
    private GitRepo devGit
    private GitRepo integratorGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        devGit.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_prepareFeatureIntegration_whenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        env.integratorRepo.fetch(remote: central)

        env.integratorRepo.checkout(branch: releaseBranch)
        commitSomething(env.integratorRepo)
        env.integratorRepo.tag.add(name: remoteReleaseBranch)

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag()

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateFeature()

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_cancelIntegration_whenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        env.integratorRepo.checkout(branch: releaseBranch)
        commitSomething(env.integratorRepo)
        env.integratorRepo.tag.add(name: remoteReleaseBranch)

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag()

        // [integrator] the local pre-release branch exists and is checked out
        env.integratorRepo.checkout(branch: preReleaseBranch, createBranch: true)

        assert branchesOf(env.integratorRepo).asList().contains(preReleaseBranch)
        assert isCheckedOut(env.integratorRepo, preReleaseBranch)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchIsMissing_butADescendantTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        helper.test_createFirstVersion()
        
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        env.integratorRepo.checkout(branch: releaseBranch)
        commitSomething(env.integratorRepo)
        env.integratorRepo.tag.add(name: remoteReleaseBranch)
        env.integratorRepo.push(remote: central, tags: true)

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_descendantTag()

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.2.0')

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_prepareFeatureIntegration_whenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        env.integratorRepo.tag.add(name: remoteReleaseBranch)

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag()

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateFeature()

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_cancelIntegration_whenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        env.integratorRepo.tag.add(name: remoteReleaseBranch)

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag()

        // [integrator] the local pre-release branch exists and is checked out
        env.integratorRepo.checkout(branch: preReleaseBranch, createBranch: true)

        assert branchesOf(env.integratorRepo).asList().contains(preReleaseBranch)
        assert isCheckedOut(env.integratorRepo, preReleaseBranch)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newCancelIntegrationAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchIsMissing_butAnAncestorTagNamedLikeTheRemoteReleaseBranchExists() {
        // Setup
        // ---------------------------------------------------------------------
        def markerTag = 'here!'
        env.integratorRepo.tag.add(name: markerTag)
        env.integratorRepo.push(remote: central, tags: true)

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        helper.test_createFirstVersion()

        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        env.integratorRepo.checkout(branch: someRandomBranch, createBranch: true)
        env.integratorRepo.branch.remove(names: [releaseBranch])

        env.integratorRepo.tag.add(name: remoteReleaseBranch, pointsTo: markerTag)
        env.integratorRepo.push(remote: central, tags: true)

        // Preconditions
        // ---------------------------------------------------------------------
        pre_ancestorTag()

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.2.0')

        // Check results
        // ---------------------------------------------------------------------
        checkResults()
    }

    private void pre_descendantTag() {
        // 1. [integrator] only the remote-tracking release branch exists
        assert ! branchesOf(env.integratorRepo).any { releaseBranch == it }
        assert remoteBranchesOf(env.integratorRepo).any { remoteReleaseBranch == it }

        // 2. [central] the release branch exists
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 3. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, someRandomBranch)

        // 4. [integrator] a tag named like the remote release branch exists
        assert tagsOf(env.integratorRepo).any { remoteReleaseBranch == it }

        // 5. [integrator] ...on a descendant commit of the remote release branch
        assert isDescendant(env.integratorRepo,
            commitOfTag(env.integratorRepo, remoteReleaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))
    }

    private void pre_ancestorTag() {
        // 1. [integrator] only the remote-tracking release branch exists
        assert ! branchesOf(env.integratorRepo).any { releaseBranch == it }
        assert remoteBranchesOf(env.integratorRepo).any { remoteReleaseBranch == it }

        // 2. [central] the release branch exists
        assert branchesOf(env.centralRepo).any { releaseBranch == it }

        // 3. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, someRandomBranch)

        // 4. [integrator] a tag named like the remote release branch exists
        assert tagsOf(env.integratorRepo).any { remoteReleaseBranch == it }

        // 5. [integrator] ...on an ancestor commit of the remote release branch
        assert isAncestor(env.integratorRepo,
            commitOfTag(env.integratorRepo, remoteReleaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))
    }

    private void checkResults() {
        // 1. [integrator] the local release branch was created at the same commit as the remote release branch
        assert branchesOf(env.integratorRepo).any { releaseBranch == it }
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        // 2. [integrator] ...and it tracks the remote release branch
        assert isTracking(env.integratorRepo, releaseBranch, remoteReleaseBranch)
    }
}
