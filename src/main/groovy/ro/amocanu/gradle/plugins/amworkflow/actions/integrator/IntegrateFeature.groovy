/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope
import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

/**
 * Note: It is assumed IntegrateFeature is run only if PrepareFeatureIntegration
 *       was run and finished successfully. This is enforced by a 'dependsOn' 
 *       relation between IntegrateFeatureTask and PrepareFeatureIntegrationTask.
 */
@PackageScope
class IntegrateFeature extends BaseAction {

    IntegrateFeature(Configuration conf, GitRepo git) {
        super(conf, git)
    }

    @Override
    void run() {
        try {
            fastForwardReleaseBranchToPreRelease()
            pushReleaseToCentral()
            deleteIntegratedFeature()
            postActions()
        } catch (any) {
            throw any
        } finally {
            deletePreReleaseBranch()
        }
    }

    protected void postActions() {
        // default implementation: do nothing
    }

    private void fastForwardReleaseBranchToPreRelease() {
        git.checkoutBranch(conf.releaseBranch)
        git.fastForwardCurrentBranchTo(conf.preReleaseBranch)
    }

    private void pushReleaseToCentral() {
        git.pushBranchToCentral(conf.releaseBranch)
    }

    private void deleteIntegratedFeature() {
        def remotePreReleaseBranch = "${conf.centralGitRepository}/${conf.preReleaseBranch}"
        git.remoteTrackingCentralBranches()
            .each { remoteBranch ->
                def featureBranchMatcher = (remoteBranch =~ "${conf.centralGitRepository}/(${conf.featureBranchFromFeatureName('.+')})")
                if (featureBranchMatcher.matches()) {
                    String remoteBranchTip = git.commitIdOfRemoteBranch(remoteBranch)
                    String remotePreReleaseBranchTip = git.commitIdOfRemoteBranch(remotePreReleaseBranch)
                    if (remoteBranchTip == remotePreReleaseBranchTip) {
                        git.removeBranchOnCentral(featureBranchMatcher[0][1])
                    }
                }
            }
    }

    private void deletePreReleaseBranch() {
        git.checkoutBranch(conf.releaseBranch)
        git.removeBranch(conf.preReleaseBranch)
        git.removeBranchOnCentral(conf.preReleaseBranch)
    }
}
