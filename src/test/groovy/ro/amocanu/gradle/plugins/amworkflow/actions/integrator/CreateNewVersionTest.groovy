/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitAndPushSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.fileContentsOnTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isFileInRepoOnBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isFileInRepoOnTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import java.nio.file.Paths

import org.ajoberstar.grgit.Commit
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.operation.ResetOp
import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class CreateNewVersionTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'to-integrate',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'version-',
            versionFile: '.version',
            bumpVersionCommitMessage: { version -> "Version ${version}" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName1 = 'CountSheep'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String randomBranch = 'Hide/TheClock'

    private static final String releaseBranch = ext.versioned.mainBranch
    private static final String remoteReleaseBranch = "${ext.versioned.centralGitRepository}/${releaseBranch}"
    private static final String preReleaseBranch = ext.versioned.preReleaseBranch
    private static final String remotePreReleaseBranch = "${ext.versioned.centralGitRepository}/${preReleaseBranch}"
    private static final String versionFile = ext.versioned.versionFile
    private static final String versionPrefix = ext.versioned.versionPrefix

    private TestEnv env
    private GitRepo integratorGit
    private GitRepo devGit
    private TestHelper helper

    private String integratorBranchBeforeTest
    private List integratorTagsBeforeTest
    private Commit integratorReleaseBranchBeforeTest
    private List centralTagsBeforeTest
    private Commit centralReleaseBranchBeforeTest

    @Before
    public void setUp() {
        env = new TestEnv()
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        integratorBranchBeforeTest = null
        integratorTagsBeforeTest = null
        integratorReleaseBranchBeforeTest = null
        centralTagsBeforeTest = null
        centralReleaseBranchBeforeTest = null
    }

    @After
    public void tearDown() {
        integratorGit.close()
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_createNewVersion_withUnknownVersionType() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 2. [integrator, central] the version file exists
        createAndCommitFirstVersionFile(env.integratorRepo)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        // 1. try creating a new version with a type that is not valid
        def invalidType = 'Patch'
        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, invalidType).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Invalid value for 'versionType' property: '${invalidType}', must be one of: major, minor or patch" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenOnFeatureBranch() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.3.0')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] make a feature branch and commit something on it
        DeveloperActions.newCreateFeatureAction(ext, integratorGit, featureName1).run()
        commitSomething(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature branch is checked out
        assert isCheckedOut(env.integratorRepo, featureBranch1)

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.4.0')
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchDoesNotExist() {
        def newVersionType = 'minor'
        def newVersion = '1.1.0'
        def newVersionTag = 'version-1.1.0'
        def newCommitMessage = 'Version 1.1.0'

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '1.0.1')

        // 2. [integrator, central] make some changes on the release branch
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] checkout a random branch
        env.integratorRepo.checkout(branch: randomBranch, createBranch: true)

        // 4. [integrator] remove the release branch
        env.integratorRepo.branch.remove(names: [releaseBranch])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] a random branch is checked out
        def initialBranch = randomBranch
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // 2. [integrator] the release branch does not exist
        assert ! branchesOf(env.integratorRepo).asList().contains(releaseBranch)

        // 3. [central] the release branch exists
        assert branchesOf(env.centralRepo).asList().contains(releaseBranch)

        // 4. [integrator] repo has no staged changes
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // 5. [integrator] the version tag does not exist
        def integratorTagsBeforeTest = tagsOf(env.integratorRepo).asList()
        assert ! integratorTagsBeforeTest.contains(newVersionTag)

        // 6. [central] the version tag does not exist
        def centralTagsBeforeTest = tagsOf(env.centralRepo).asList()
        assert ! centralTagsBeforeTest.contains(newVersionTag)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newCreateNewVersionAction(ext, integratorGit, newVersionType).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator] the version tag was created
        def newIntegratorTags = tagsOf(env.integratorRepo).asList().minus(integratorTagsBeforeTest)

        assert 1 == newIntegratorTags.size()
        assert newVersionTag == newIntegratorTags.first()

        // 2. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 3. [integrator] the commit with the version tag contains the version file
        def versionFileContains = fileContentsOnTag(env.integratorRepo, versionFile, newVersionTag)
        assert versionFileContains != null

        // 4. [integrator] the version file in this commit contains the expected version number
        assert versionFileContains.any { "version='${newVersion}'" == it }

        // 5. [integrator] the commit with the version tag has the correct commit message
        assert newCommitMessage == env.integratorRepo.log(maxCommits: 1).fullMessage.first()

        // 6. [integrator] the version tag is at the tip of the main branch
        assert commitIdOfTag(env.integratorRepo, newVersionTag) == branchTip(env.integratorRepo, releaseBranch).abbreviatedId

        // 7. [integrator, central] the main branch is synchronized correctly
        assert remoteBranchesOf(env.integratorRepo).any { remoteReleaseBranch }
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        assert branchesOf(env.centralRepo).any { releaseBranch }
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 8. [central] the version tag was created
        def newCentralTags = tagsOf(env.centralRepo).asList().minus(centralTagsBeforeTest)

        assert 1 == newCentralTags.size()
        assert newVersionTag == newCentralTags.first()
    }

    @Test
    public void test_createNewVersion_whenCentralReleaseBranchDoesNotExist() {
        def releaseBranch2 = 'master2'
        def PluginExt ext2 = new PluginExt(
            root: new RootExtension(
                workflowType: WorkflowType.VERSIONED.toString()),
            versioned: new VersionedExtension(
                centralGitRepository: ext.versioned.centralGitRepository,
                featureBranchPrefix: ext.versioned.featureBranchPrefix,
                preReleaseBranch: ext.versioned.preReleaseBranch,
                mainBranch: releaseBranch2,
                mainIntegrationURLTrigger: ext.versioned.mainIntegrationURLTrigger,
                ciUsername: ext.versioned.ciUsername,
                ciPassword: ext.versioned.ciPassword,
                versionPrefix: ext.versioned.versionPrefix,
                versionFile: ext.versioned.versionFile,
                bumpVersionCommitMessage: ext.versioned.bumpVersionCommitMessage,
                maintenanceBranchPrefix: ext.versioned.maintenanceBranchPrefix,
                maintenanceIntegrationURLTriggers: ext.versioned.maintenanceIntegrationURLTriggers))

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '1.0.1')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] create a new release branch
        env.integratorRepo.branch.add(name: releaseBranch2)

        // 4. [integrator] checkout a random branch
        env.integratorRepo.checkout(branch: randomBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a random branch is checked out
        assert isCheckedOut(env.integratorRepo, randomBranch)

        // 2. the release branch exists on the integrator repo
        assert branchesOf(env.integratorRepo).asList().contains(releaseBranch2)

        // 3. the release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).asList().contains(releaseBranch2)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        // 1. try creating a new version
        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext2, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Branch '${releaseBranch2}' not found in central repository!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenWorkingDirectoryNotClean() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.1.1')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.2.1')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.2.2')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] make some uncommitted changes
        createEmptyFileInRepo(env.integratorRepo)
        createEmptyFileInRepo(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the working directory contains some changes
        assert ! workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // 3. [integrator] the staging area is clean
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        // 1. try creating a new version
        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'major').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenStagingAreaNotClean() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.1.1')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.1.2')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.1.3')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] add some changes to the staging area
        def someFile = createEmptyFileInRepo(env.integratorRepo)
        stageFile(env.integratorRepo, someFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the staging area contains some changes
        assert ! stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // 2. [integrator] the working directory is clean
        assert workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionFileIsModified() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '2.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '3.0.0')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] modify the version file
        changeFile(env.integratorRepo, versionFile, "version='2.5.6'")

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the working directory contains the version file
        assert [versionFile] == workingDirectoryOf(env.integratorRepo).asList()

        // 2. [integrator] the staging area is clean
        assert stagingAreaOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'major').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenOnlyTheVersionFileIsStaged() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.3.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.4.0')

        // 2. [integrator] modify and stage the version file
        changeFile(env.integratorRepo, versionFile, 'version=2.5.6')
        stageFile(env.integratorRepo, versionFile)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the staging area contains the version file
        assert [versionFile] == stagingAreaOf(env.integratorRepo).asList()

        // 2. [integrator] the working directory is clean
        assert workingDirectoryOf(env.integratorRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        // 1. try creating a new version
        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Cannot perform task while repo has changes!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createFirstVersionAsPatch() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 2. [integrator] the version file exists
        createAndCommitNewFile(env.integratorRepo, versionFile, "version='0.0.0'")

        // 3. [integrator, central] no version tags exists (no tags exist at all)
        assert tagsOf(env.integratorRepo).asList().isEmpty()
        assert tagsOf(env.centralRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        // 1. try creating a patch version
        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert 'Version number less than 0.1.0 is not allowed!' == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createAlreadyExistingVersion() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.3.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.4.0')

        // 2. [integrator, central] reset the release branch to a previous version
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        env.integratorRepo.reset(commit: commitIdOfTag(env.integratorRepo, 'version-0.3.0'), mode: ResetOp.Mode.HARD)
        env.integratorRepo.push(refsOrSpecs: [releaseBranch], force: true)

        // 3. [integrator, central] make some changes on the release branch
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 2. [integrator, central] versions 0.3.0 and 0.4.0 both exist
        assert tagsOf(env.integratorRepo).asList().contains('version-0.3.0')
        assert tagsOf(env.integratorRepo).asList().contains('version-0.4.0')

        assert tagsOf(env.centralRepo).asList().contains('version-0.3.0')
        assert tagsOf(env.centralRepo).asList().contains('version-0.4.0')

        // 3. [integrator, central] the release branch is descending from version 0.3.0, but not from 0.4.0
        assert isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            commitOfTag(env.integratorRepo, 'version-0.3.0'))

        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, releaseBranch),
            commitOfTag(env.centralRepo, 'version-0.3.0'))

        assert ! isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            commitOfTag(env.integratorRepo, 'version-0.4.0'))

        assert ! isDescendant(env.centralRepo,
            branchTip(env.centralRepo, releaseBranch),
            commitOfTag(env.centralRepo, 'version-0.4.0'))

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        def commit = commitIdOfTag(env.integratorRepo, 'version-0.4.0')
        assert "Version '0.4.0' already exists; see commit '${commit}'!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionFileAlreadyUpToDate_butNoTag() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 2. [integrator, central] the version file exists and contains the first minor version
        createAndCommitNewFile(env.integratorRepo, versionFile, "version='0.1.0'")
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // 3. [integrator, central] no version tags exists (no tags exist at all)
        assert tagsOf(env.integratorRepo).asList().isEmpty()
        assert tagsOf(env.centralRepo).asList().isEmpty()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator, central] no new commit was created on the release branch
        assert integratorReleaseBranchBeforeTest == branchTip(env.integratorRepo, releaseBranch)
        assert centralReleaseBranchBeforeTest == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator, central] the version tag was created
        def expectedTag = versionPrefix + '0.1.0'
        assert [expectedTag] == tagsOf(env.integratorRepo).asList()
        assert [expectedTag] == tagsOf(env.centralRepo).asList()

        // 3. [integrator] the release branch is still checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)
    }

    @Test
    public void test_createNewVersion_whenVersionFileContainsARandomVersionString() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] several versions exist
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '1.0.1')

        // 2. [integrator, central] the version file was committed with a random version
        changeFile(env.integratorRepo, versionFile, "version='4.8.15'")
        commitFile(env.integratorRepo, versionFile)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. create a new version
        helper.test_createNewVersion('minor', '1.1.0')
    }

    @Test
    public void test_createNewVersion_whenVersionFileDoesNotContainTheVersionProperty() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] several versions exist
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('major', '1.0.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '1.0.1')

        // 2. [integrator, central] the version file was committed with a wrong version property name
        changeFile(env.integratorRepo, versionFile, "versiune='1.0.1'")
        commitFile(env.integratorRepo, versionFile)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Property 'version' is missing from '${versionFile}'." == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionPropertyValueDoesNotContainApostrophes() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] several versions exist
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.2.1')

        // 2. [integrator, central] the version file was committed with version property value that is not a version string
        def versionString = '0.2.1'
        changeFile(env.integratorRepo, versionFile, "version=${versionString}")
        commitFile(env.integratorRepo, versionFile)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Value of 'version' property, '${versionString}', does not match expected format: ''(.+)''. Update '${versionFile}' and try again." == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionFileDoesNotContainAVersionString() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] several versions exist
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('patch', '0.2.1')

        // 2. [integrator, central] the version file was committed with version property value that is not a version string
        def wrongVersionString = 'a.b.c'
        changeFile(env.integratorRepo, versionFile, "version='${wrongVersionString}'")
        commitFile(env.integratorRepo, versionFile)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Version '${wrongVersionString}' does not match expected format: '^(0|[1-9][0-9]*).(0|[1-9][0-9]*).(0|[1-9][0-9]*)\$'. Update '${versionFile}' and try again." == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionFileDoesNotExist() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] version file does not exist on the release branch
        assert ! isFileInRepoOnBranch(env.integratorRepo, versionFile, releaseBranch)
        assert branchTip(env.centralRepo, releaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        // 2. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'major').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        def path = Paths.get(env.integratorRepo.repository.rootDir.canonicalPath, versionFile).toString()
        assert "File '${path}' does not exist." == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenNoChanges() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] no changes since the last version on the release branch
        assert branchTip(env.integratorRepo, releaseBranch) == commitOfTag(env.integratorRepo, 'version-0.2.0')
        assert branchTip(env.centralRepo, releaseBranch) == commitOfTag(env.centralRepo, 'version-0.2.0')

        // 2. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Cannot create new version because there are no changes since version '0.2.0'!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenNoChanges_butVersionAlreadyExists() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create versions 0.1.0 and 0.2.0
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator, central] reset the release branch to version 0.1.0
        env.integratorRepo.reset(commit: commitIdOfTag(env.integratorRepo, 'version-0.1.0'), mode: ResetOp.Mode.HARD)
        env.integratorRepo.push(refsOrSpecs: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] no apparent changes since version 0.1.0 on the release branch
        assert branchTip(env.integratorRepo, releaseBranch) == commitOfTag(env.integratorRepo, 'version-0.1.0')
        assert branchTip(env.centralRepo, releaseBranch) == commitOfTag(env.centralRepo, 'version-0.1.0')

        // 2. [integrator, central] version 0.2.0 exists
        assert tagsOf(env.integratorRepo).asList().contains('version-0.2.0')
        assert tagsOf(env.centralRepo).asList().contains('version-0.2.0')

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        def commit = commitIdOfTag(env.integratorRepo, 'version-0.2.0')
        assert "Version '0.2.0' already exists; see commit '${commit}'!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewPatchVersion_whenNextMinorAlreadyExists() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create versions 0.1.0 and 0.2.0
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator, central] reset the release branch to version 0.1.0
        env.integratorRepo.reset(commit: commitIdOfTag(env.integratorRepo, 'version-0.1.0'), mode: ResetOp.Mode.HARD)
        env.integratorRepo.push(refsOrSpecs: [releaseBranch], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] no apparent changes since version 0.1.0 on the release branch
        assert branchTip(env.integratorRepo, releaseBranch) == commitOfTag(env.integratorRepo, 'version-0.1.0')
        assert branchTip(env.centralRepo, releaseBranch) == commitOfTag(env.centralRepo, 'version-0.1.0')

        // 2. [integrator, central] version 0.2.0 exists
        assert tagsOf(env.integratorRepo).asList().contains('version-0.2.0')
        assert tagsOf(env.centralRepo).asList().contains('version-0.2.0')

        // 3. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'patch').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Cannot create new patch version '0.1.1' because next minor version ('0.2.0') already exists!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenVersionTagExistsOnlyOnCentral_andReleaseBranchIsBehindItsUpstream() {
        def existingVersionTag = 'version-0.1.0'

        def newVersionType = 'major'
        def newVersion = '1.0.0'
        def newVersionTag = 'version-1.0.0'
        def newCommitMessage = 'Version 1.0.0'

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create the first version
        helper.test_createFirstVersion()

        // 2. [integrator, central] make some changes on the release branch
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] reset the release branch back two commits
        env.integratorRepo.reset(commit: 'HEAD~2', mode: ResetOp.Mode.HARD)

        // 4. [integrator] remove the version tag
        env.integratorRepo.tag.remove(names: [existingVersionTag])

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] the release branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 2. [integrator] the release branch is behind its upstream
        assert isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // 3. [integrator] the version tag does not exist
        assert ! tagsOf(env.integratorRepo).asList().contains(existingVersionTag)

        // 4. [central] the version tag exists
        assert tagsOf(env.centralRepo).asList().contains(existingVersionTag)

        // 5. [central] the release branch has changes since the last new version
        assert isDescendant(env.centralRepo,
            branchTip(env.centralRepo, releaseBranch),
            commitOfTag(env.centralRepo, existingVersionTag))

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newCreateNewVersionAction(ext, integratorGit, newVersionType).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator] the version tag from central and the new version tag both exist in the repo
        def integratorTags = tagsOf(env.integratorRepo).asList()
        assert 2 == integratorTags.size()
        assert integratorTags.contains(existingVersionTag)
        assert integratorTags.contains(newVersionTag)

        // 2. [integrator] the main branch is checked out
        assert isCheckedOut(env.integratorRepo, releaseBranch)

        // 3. [integrator] the commit with the version tag contains the version file
        def fileContents = fileContentsOnTag(env.integratorRepo, versionFile, newVersionTag)
        assert fileContents != null

        // 4. [integrator] ... and the version file contains the expected version number
        assert fileContents.any { "version='${newVersion}'" == it }

        // 5. [integrator] the commit with the version tag has the correct commit message
        assert newCommitMessage == env.integratorRepo.log(maxCommits: 1).fullMessage.first()

        // 6. [integrator] the version tag is at the tip of the main branch
        assert commitOfTag(env.integratorRepo, newVersionTag) == branchTip(env.integratorRepo, releaseBranch)

        // 7. [integrator, central] the main branch is synchronized correctly
        assert remoteBranchesOf(env.integratorRepo).any { remoteReleaseBranch }
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        assert branchesOf(env.centralRepo).any { releaseBranch }
        assert branchTip(env.centralRepo, releaseBranch) == remoteBranchTip(env.integratorRepo, remoteReleaseBranch)

        // 8. [central] the old version tag and the new version tag both exist in the repo
        def centralTags = tagsOf(env.integratorRepo).asList()
        assert 2 == centralTags.size()
        assert centralTags.contains(existingVersionTag)
        assert centralTags.contains(newVersionTag)
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchIsParallelToCentral() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create the first version (0.1.0)
        helper.test_createFirstVersion()

        // 2. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] reset the release branch the previous commit
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        // 4. [integrator] ...and make a new commit from there
        commitSomething(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is parallel to its upstream
        assert ! isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        assert ! isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Release branch '${releaseBranch}' must be merged with its upstream, '${remoteReleaseBranch}', before creating a new version!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchIsBehindCentral() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create the first version (0.1.0)
        helper.test_createFirstVersion()

        // 2. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] reset the release branch the previous commit
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is behind its upstream
        assert isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.2.0')
    }

    @Test
    public void test_createNewVersion_whenLocalReleaseBranchIsAheadOfCentral() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create the first version (0.1.0)
        helper.test_createFirstVersion()

        // 2. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] commit something on the release branch
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        commitSomething(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is ahead of its remote
        assert isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('patch', '0.1.1')
    }

    @Test
    public void test_createFirstVersion_whenLocalReleaseBranchIsParallelToCentral() {
        // 1. [integrator] version file exists on the release branch
        createAndCommitFirstVersionFile(env.integratorRepo)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        assert isFileInRepoOnBranch(env.integratorRepo, ext.versioned.versionFile, releaseBranch)

        // 2. [integrator, central] the release branch is synchronized
        assert branchTip(env.centralRepo, releaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        // 3. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 4. [integrator] reset the release branch the previous commit
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        // 5. [integrator] ...and make a new commit from there
        commitSomething(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is parallel to its upstream
        assert ! isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        assert ! isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Release branch '${releaseBranch}' must be merged with its upstream, '${remoteReleaseBranch}', before creating a new version!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    @Test
    public void test_createFirstVersion_whenLocalReleaseBranchIsBehindCentral() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator] version file exists on the release branch
        createAndCommitFirstVersionFile(env.integratorRepo)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        assert isFileInRepoOnBranch(env.integratorRepo, ext.versioned.versionFile, releaseBranch)

        // 2. [integrator, central] the release branch is synchronized
        assert branchTip(env.centralRepo, releaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        // 3. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 4. [integrator] reset the release branch the previous commit
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is behind its upstream
        assert isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.1.0')
    }

    @Test
    public void test_createFirstVersion_whenLocalReleaseBranchIsAheadOfCentral() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator] version file exists on the release branch
        createAndCommitFirstVersionFile(env.integratorRepo)
        pushBranchToRemote(env.integratorRepo, ext.versioned.centralGitRepository, releaseBranch)

        assert isFileInRepoOnBranch(env.integratorRepo, ext.versioned.versionFile, releaseBranch)

        // 2. [integrator, central] the release branch is synchronized
        assert branchTip(env.centralRepo, releaseBranch) == branchTip(env.integratorRepo, releaseBranch)

        // 3. [integrator, central] make some changes for the next version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 4. [integrator] commit something on the release branch
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        commitSomething(env.integratorRepo)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the release branch is synchronized
        assert remoteBranchTip(env.integratorRepo, remoteReleaseBranch) == branchTip(env.centralRepo, releaseBranch)

        // 2. [integrator] the release branch is ahead of its remote
        assert isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // Test
        // ---------------------------------------------------------------------
        helper.test_createNewVersion('minor', '0.1.0')
    }

    @Test
    public void test_createNewVersion_whenVersionTagExistsWithDifferentCase() {
        def nextTag = 'version-0.4.0'
        def problematicTag = 'VERSION-0.4.0'

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator, central] create several versions
        helper.test_createFirstVersion()

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.2.0')

        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)
        helper.test_createNewVersion('minor', '0.3.0')

        // 2. [integrator, central] make some changes for the new version
        commitAndPushSomethingOnReleaseBranch(env.integratorRepo)

        // 3. [integrator] create tag VERSION-0.4.0
        env.integratorRepo.tag.add(name: problematicTag)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator] a tag differing only by case from the next version tag exists
        assert tagsOf(env.integratorRepo).asList().contains(problematicTag)
        assert problematicTag != nextTag
        assert problematicTag.toLowerCase() == nextTag.toLowerCase()

        // Test
        // ---------------------------------------------------------------------
        captureRepoStateBeforeTest()

        def e = shouldFail {
            IntegratorActions.newCreateNewVersionAction(ext, integratorGit, 'minor').run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        def commit = commitIdOfTag(env.integratorRepo, "${problematicTag}")
        assert "Version tag '${nextTag}' already exists with a different case: '${problematicTag}'; see commit '${commit}'!" == e.getMessage()

        // 2. [integrator, central] no changes after test
        checkResult_noRepoChanges()
    }

    private void captureRepoStateBeforeTest() {
        integratorBranchBeforeTest = env.integratorRepo.branch.current.name
        integratorTagsBeforeTest = tagsOf(env.integratorRepo)
        integratorReleaseBranchBeforeTest = branchTip(env.integratorRepo, releaseBranch)
        centralReleaseBranchBeforeTest = branchTip(env.centralRepo, releaseBranch)
        centralTagsBeforeTest = tagsOf(env.centralRepo)
    }

    private void checkResult_noRepoChanges() {
        // 1. [integrator] the same branch is checked out as before the test
        assert isCheckedOut(env.integratorRepo, integratorBranchBeforeTest)

        // 2. [integrator] the release branch is unchanged
        assert integratorReleaseBranchBeforeTest != null
        assert integratorReleaseBranchBeforeTest == branchTip(env.integratorRepo, releaseBranch)

        // 3. [integrator] no new tag was created
        assert integratorTagsBeforeTest != null
        assert tagsOf(env.integratorRepo).asList().minus(integratorTagsBeforeTest).isEmpty()

        // 4. [central] the release branch is unchanged
        assert centralReleaseBranchBeforeTest != null
        assert centralReleaseBranchBeforeTest == branchTip(env.centralRepo, releaseBranch)

        // 5. [central] no new tag was created
        assert centralTagsBeforeTest != null
        assert tagsOf(env.centralRepo).asList().minus(centralTagsBeforeTest).isEmpty()
    }

    private static void createAndCommitFirstVersionFile(Grgit repo) {
        createAndCommitNewFile(repo, versionFile, "version='0.0.0'")
    }

    private static void commitAndPushSomethingOnReleaseBranch(Grgit repo) {
        commitAndPushSomething(repo, ext.versioned.centralGitRepository, releaseBranch)
    }
}
