/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.conf

import groovy.transform.Immutable

@Immutable
class VersionedConfiguration {
    String versionFile
    Closure<String> bumpVersionCommitMessage
    Closure<String> maintenanceBranchFromMaintainedVersion
    Closure<String> maintainedVersionFromMaintenanceBranch
    Closure<Boolean> isMaintenanceBranch
    Closure<Boolean> isPseudoMaintenanceBranch
}
