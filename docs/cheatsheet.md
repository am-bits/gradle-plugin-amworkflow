# Cheatsheet

All *am*workflow tasks listed conveniently in one place.

___

```bash
# Continuous delivery, developer
gradle createFeature  -PfeatureName=myFeature
gradle goToFeature    -PfeatureName=myFeature
gradle discardFeature -PfeatureName=myFeature
gradle deliverFeature
gradle syncWithCentral

# Continuous delivery, integrator
gradle showIntegrationStatus     -Prole=INTEGRATOR
gradle prepareFeatureIntegration -Prole=INTEGRATOR
gradle integrateFeature          -Prole=INTEGRATOR
gradle cancelIntegration         -Prole=INTEGRATOR

# Versioned delivery, main branch, developer
gradle createFeature  -PfeatureName=myFeature
gradle goToFeature    -PfeatureName=myFeature
gradle discardFeature -PfeatureName=myFeature
gradle deliverFeature
gradle syncWithCentral

# Versioned delivery, main branch, integrator
gradle showIntegrationStatus     -Prole=INTEGRATOR
gradle prepareFeatureIntegration -Prole=INTEGRATOR
gradle integrateFeature          -Prole=INTEGRATOR
gradle cancelIntegration         -Prole=INTEGRATOR
gradle createNewVersion          -Prole=INTEGRATOR -PversionType={major|minor|patch}

# Versioned delivery, maintenance branch, developer
gradle createMaintenanceBranch   -PmaintainedVersion=1
gradle createMaintenanceFeature  -PmaintainedVersion=1 -PfeatureName=myFeature
gradle goToMaintenanceFeature    -PmaintainedVersion=1 -PfeatureName=myFeature
gradle discardMaintenanceFeature -PmaintainedVersion=1 -PfeatureName=myFeature
gradle deliverFeature
gradle syncWithCentral

# Versioned delivery, maintenance branch, integrator
gradle showMaintenanceIntegrationStatus     -Prole=INTEGRATOR -PmaintainedVersion=1
gradle prepareMaintenanceFeatureIntegration -Prole=INTEGRATOR -PmaintainedVersion=1
gradle integrateMaintenanceFeature          -Prole=INTEGRATOR -PmaintainedVersion=1
gradle cancelMaintenanceIntegration         -Prole=INTEGRATOR -PmaintainedVersion=1
gradle createNewMaintenanceVersion          -Prole=INTEGRATOR -PmaintainedVersion=1 -PversionType={minor|patch}
```

## Aliases

A cheatsheet for [our recommended developer aliases](extra/#helpful-aliases).

```bash
# Continuous delivery, developer
# Versioned delivery, main branch, developer
crf myFeature       # [cr]eate [f]eature
gtf myFeature       # [g]o [t]o [f]eature
rmf myFeature       # discard ([r]e[m]ove) [f]eature
dlf                 # [d]e[l]iver [f]eature
syn                 # [syn]chronize with central repo

# Versioned delivery, v1 maintenance branch, developer
crf1 myFeature # [cr]eate [f]eature for v[1]
gtf1 myFeature # [g]o [t]o [f]eature of v[1]
rmf1 myFeature # discard ([r]e[m]ove) [f]eature of v[1]
dlf            # [d]e[l]iver [f]eature
syn            # [syn]chronize with central repo
```
