/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions

import java.nio.file.Paths

import org.ajoberstar.grgit.Branch
import org.ajoberstar.grgit.Commit
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.Tag
import org.ajoberstar.grgit.operation.BranchListOp
import org.ajoberstar.grgit.operation.MergeOp
import org.ajoberstar.grgit.util.JGitUtil

class GitTestUtil {
    static boolean isFileInRepo(Grgit repo, String fileName) {
        return new File("${repo.repository.rootDir.canonicalPath}/${fileName}").exists()
    }

    static boolean isFileInRepoOnBranch(Grgit repo, String fileName, String branch) {
        def initialBranch = repo.branch.current.name

        repo.checkout(branch: branch)
        assert branch == repo.branch.current.name

        def isFile = new File("${repo.repository.rootDir.canonicalPath}/${fileName}").exists()
        
        repo.checkout(branch: initialBranch)
        assert initialBranch == repo.branch.current.name
        
        return isFile
    }

    static boolean isFileInRepoOnTag(Grgit repo, String fileName, String tag) {
        def initialBranch = repo.branch.current.name

        def tagMarker = "${System.currentTimeMillis()}"
        repo.checkout(startPoint: tag, branch: tagMarker, createBranch: true)
        assert tagMarker == repo.branch.current.name
        
        def isFile = new File("${repo.repository.rootDir.canonicalPath}/${fileName}").exists()

        repo.checkout(branch: initialBranch)
        assert initialBranch == repo.branch.current.name

        repo.branch.remove(names: [tagMarker])
        
        return isFile
    }

    static List fileContentsOnBranch(Grgit repo, String fileName, String branch) {
        def initialBranch = repo.branch.current.name

        repo.checkout(branch: branch)
        assert branch == repo.branch.current.name

        def file = new File("${repo.repository.rootDir.canonicalPath}/${fileName}")
        
        def lines = null
        if (file.exists()) {
            lines = file.readLines()
        }

        repo.checkout(branch: initialBranch)
        assert initialBranch == repo.branch.current.name

        return lines
    }

    static List fileContentsOnTag(Grgit repo, String fileName, String tag) {
        def initialBranch = repo.branch.current.name

        def tagMarker = "${System.currentTimeMillis()}"
        repo.checkout(startPoint: tag, branch: tagMarker, createBranch: true)
        assert tagMarker == repo.branch.current.name

        def file = new File("${repo.repository.rootDir.canonicalPath}/${fileName}")
        
        def lines = null
        if (file.exists()) {
            lines = file.readLines()
        }

        repo.checkout(branch: initialBranch)
        assert initialBranch == repo.branch.current.name

        repo.branch.remove(names: [tagMarker])

        return lines
    }

    static String createEmptyFileInRepo(Grgit repo) {
        def fileName = UUID.randomUUID().toString() + ".txt"
        def filePath = Paths.get(repo.repository.rootDir.canonicalPath, fileName).toString()
        def file = new File(filePath)

        assert !file.exists()

        file.createNewFile()
        return fileName
    }
    
    static void stageSomething(Grgit repo) {
        def fileName = createEmptyFileInRepo(repo)
        repo.add(patterns: [fileName])
    }
    
    static final List messages = [
        'Solve race condition',
        'Fight bugs',
        'Slay the dragon',
        'Die another day',
        'Reticulate splines',
        'Take a sad song and make it better',
        'Save the world',
        'Do what we must because we can',
        'Fly to the Moon and back',
        'Swing on a star',
        'Colonize the solar system',
        'Don\'t panic',
        'Reverse entropy'
    ]

    static void commitSomething(Grgit repo) {
        def fileName = createEmptyFileInRepo(repo)
        def message = messages[(new Random()).nextInt(messages.size())]
        
        repo.add(patterns: [fileName])
        repo.commit(message: message)
    }
    
    static void createAndCommitNewFile(Grgit repo, String fileName, String fileContents) {
        def filePath = Paths.get(repo.repository.rootDir.canonicalPath, fileName).toString()
        def file = new File(filePath)
        
        assert !file.exists()
        
        file.text = fileContents
        
        repo.add(patterns: [fileName])
        repo.commit(message: "Add file ${fileName}")
    }
    
    static void changeFile(Grgit repo, String fileName, String fileContents) {
        def filePath = Paths.get(repo.repository.rootDir.canonicalPath, fileName).toString()
        def file = new File(filePath)
        
        assert file.exists()
        
        file.text = fileContents
    }

    static void stageFile(Grgit repo, String fileName) {
        def filePath = Paths.get(repo.repository.rootDir.canonicalPath, fileName).toString()
        def file = new File(filePath)
        
        assert file.exists()
        assert workingDirectoryOf(repo).asList().contains(fileName)
        
        repo.add(patterns: [fileName])
    }
        
    static void commitFile(Grgit repo, String fileName) {
        def filePath = Paths.get(repo.repository.rootDir.canonicalPath, fileName).toString()
        def file = new File(filePath)
        
        assert file.exists()
        assert workingDirectoryOf(repo).asList().contains(fileName)
        
        repo.add(patterns: [fileName])
        repo.commit(message: "Update file ${fileName}")
    }
    
    static void pushBranchToRemote(Grgit repo, String remoteName, String branchName) {
        repo.push(remote: remoteName, refsOrSpecs: [branchName])
    }
    
    static void commitAndPushSomething(Grgit repo, String remoteName, String branch) {
        def initialBranch = repo.branch.current.name

        repo.checkout(branch: branch)
        assert isCheckedOut(repo, branch)

        commitSomething(repo)
        pushBranchToRemote(repo, remoteName, branch)

        repo.checkout(branch: initialBranch)
        assert isCheckedOut(repo, initialBranch)
    }

    static void mergeIntoCurrentBranch(Grgit repo, String branchToMerge) {
        repo.merge(head: branchToMerge, mode: MergeOp.Mode.DEFAULT)
    }
    
    static Commit branchTip(Grgit repo, String branchName) {
        Branch branch = repo.resolve.toBranch("refs/heads/${branchName}")
        return branch ? repo.resolve.toCommit(branch) : null
    }

    static Commit remoteBranchTip(Grgit repo, String remoteBranchName) {
        Branch branch = repo.resolve.toBranch("refs/remotes/${remoteBranchName}")
        return branch ? repo.resolve.toCommit(branch) : null
    }

    static boolean isAncestor(Grgit repo, Commit potentialAncestor, Commit commit) {
        def areDifferent = (potentialAncestor != commit)
        def isAncestor = JGitUtil.isAncestorOf(repo.repository, potentialAncestor, commit)
        
        return areDifferent && isAncestor 
    }
    
    static boolean isDescendant(Grgit repo, Commit potentialDescendant, Commit commit) {
        def areDifferent = (potentialDescendant != commit)
        def isDescendant = JGitUtil.isAncestorOf(repo.repository, commit, potentialDescendant)
        
        return areDifferent && isDescendant
    }
    
    static Iterable<String> branchesOf(Grgit git) {
        return git.branch.list(mode: BranchListOp.Mode.LOCAL)*.name
    }
    
    static Iterable<String> remoteBranchesOf(Grgit repo) {
        return repo.branch.list(mode: BranchListOp.Mode.REMOTE)*.name
    }
    
    static Iterable<String> tagsOf(Grgit repo) {
        return repo.tag.list()*.name
    }
    
    static Iterable<String> stagingAreaOf(Grgit repo) {
        return repo.status().staged.allChanges
    }
    
    static Iterable<String> workingDirectoryOf(Grgit repo) {
        return repo.status().unstaged.allChanges
    }
    
    static boolean isCheckedOut(Grgit repo, String branchName) {
        return branchName == repo.branch.current.name
    }
    
    static boolean isTracking(Grgit repo, String branchName, String remoteBranchName) {
        def branch = repo.resolve.toBranch(branchName)
        def trackingBranch = (null != branch) ? branch.trackingBranch : null
        def fullRemoteBranchName = "refs/remotes/${remoteBranchName}"
        return (null != trackingBranch) && (fullRemoteBranchName == trackingBranch.fullName)
    }
    
    static String commitIdOfTag(Grgit repo, String tagName) {
        Tag tag = repo.resolve.toTag("refs/tags/${tagName}")
        return tag ? repo.resolve.toCommit(tag).getAbbreviatedId() : null
    }

    static Commit commitOfTag(Grgit repo, String tagName) {
        Tag tag = repo.resolve.toTag("refs/tags/${tagName}")
        return tag ? repo.resolve.toCommit(tag) : null
    }
}
