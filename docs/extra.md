# Extra stuff

Various tips and tricks.

___

## Helpful aliases

If you use a Unix shell it's helpful to define <a href="http://tldp.org/LDP/abs/html/aliases.html">aliases</a>, especially for the developer tasks, which are invoked manually and often.

Download the [.amworkflow](https://bitbucket.org/am-bits/gradle-plugin-amworkflow/raw/master/docs/resources/.amworkflow) file to your HOME and add the following lines to your `.bashrc` (or `.bash_profile`, if you use Git Bash):

```bash
#------------------------------------------------------------------------------
# Bash support for amworkflow.
#------------------------------------------------------------------------------
if [ -f ~/.amworkflow ]; then
  . ~/.amworkflow
fi
```

This will add [several developer aliases](cheatsheet/#aliases) to your environment.
