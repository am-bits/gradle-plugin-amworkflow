/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.combined

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitIdOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.integrator.IntegratorActions
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MultipleMaintenanceBranchesInVersionedWorkflowTest extends BaseTest {
    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maint',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName1 = 'SpinElectron'
    private static final String featureBranch1 = "task/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/task/${featureName1}"

    private static final String featureName2 = 'CollapseWaveFunction'
    private static final String featureBranch2 = "task/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/task/${featureName2}"

    private static final String featureName3 = 'SaveCat'
    private static final String featureBranch3 = "task/${featureName3}"
    private static final String remoteFeatureBranch3 = "origin/task/${featureName3}"

    private static final String featureName4 = 'DisproveTheory'
    private static final String featureBranch4 = "task/${featureName4}"
    private static final String remoteFeatureBranch4 = "origin/task/${featureName4}"

    private static final String featureName5 = 'DetectPhotons'
    private static final String featureBranch5 = "task/${featureName5}"
    private static final String remoteFeatureBranch5 = "origin/task/${featureName5}"

    private static final int v1 = 1
    private static final String v1_maintenanceBranch = 'maint_v1'
    private static final String v1_remoteMaintenanceBranch = 'origin/maint_v1'
    private static final String v1_preReleaseBranch = 'maint_v1_pre'

    private static final String v1_featureName1 = 'SpinElectronAgain'
    private static final String v1_featureBranch1 = "maint_v1_task/${v1_featureName1}"
    private static final String v1_remoteFeatureBranch1 = "origin/maint_v1_task/${v1_featureName1}"

    private static final String v1_featureName2 = 'UncollapseWaveFunction'
    private static final String v1_featureBranch2 = "maint_v1_task/${v1_featureName2}"
    private static final String v1_remoteFeatureBranch2 = "origin/maint_v1_task/${v1_featureName2}"

    private static final String v1_featureName3 = 'KillCat'
    private static final String v1_featureBranch3 = "maint_v1_task/${v1_featureName3}"
    private static final String v1_remoteFeatureBranch3 = "origin/maint_v1_task/${v1_featureName3}"

    private static final int v2 = 2
    private static final String v2_maintenanceBranch = 'maint_v2'
    private static final String v2_remoteMaintenanceBranch = 'origin/maint_v2'
    private static final String v2_preReleaseBranch = 'maint_v2_pre'

    private static final String v2_featureName1 = 'ProveGeneralRelativity'
    private static final String v2_featureBranch1 = "maint_v2_task/${v2_featureName1}"
    private static final String v2_remoteFeatureBranch1 = "origin/maint_v2_task/${v2_featureName1}"

    private static final String releaseBranch = ext.versioned.mainBranch

    private static TestEnv env

    private static GitRepo devGit
    private static GitRepo integratorGit

    private static TestHelper helper

    @BeforeClass
    public static void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @AfterClass
    public static void tearDown() {
        integratorGit.close()
        devGit.close()

        env.tearDown()
    }

    @Test
    public void step01_createAndDeliverFeature1() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void step02_integrateFeature1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
    }

    @Test
    public void step03_createFirstVersion() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createFirstVersion() // 0.1.0
    }

    @Test
    public void step04_createAndDeliverFeature2() {
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
    }

    @Test
    public void step05_integrateFeature2() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch2 == it }
    }

    @Test
    public void step06_createVersion1_0_0() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('major', '1.0.0')
    }

    @Test
    public void step07_createAndDeliverFeature3() {
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)
    }

    @Test
    public void step08_integrateFeature3() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch3 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch3 == it }
    }

    @Test
    public void step09_createVersion1_1_0() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('minor', '1.1.0')
    }

    @Test
    public void step10_createAndDeliverFeature4() {
        helper.test_createFeature(featureName4, featureBranch4, remoteFeatureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
        helper.test_deliverFeature(featureBranch4, remoteFeatureBranch4)
    }

    @Test
    public void step11_integrateFeature4() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch4 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch4 == it }
    }

    @Test
    public void step12_createVersion2_0_0() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('major', '2.0.0')
    }

    @Test
    public void step13_createMaintenanceBranchForV1() {
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)
    }

    @Test
    public void step14_createAndDeliverMaintenanceFeature1ForV1() {
        helper.test_createMaintenanceFeature(v1, v1_maintenanceBranch, v1_featureName1, v1_featureBranch1, v1_remoteFeatureBranch1)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch1)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch1)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch1)
        helper.test_deliverMainenanceFeature(v1, v1_featureBranch1, v1_remoteFeatureBranch1)
    }

    @Test
    public void step15_integrateMaintenanceFeature1ForV1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { v1_featureBranch1 == it }

        helper.test_integrateMaintenenceFeature(v1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { v1_featureBranch1 == it }
    }

    @Test
    public void step16_createVersion1_2_0() {
        helper.test_createNewMaintenanceVersion(v1, 'minor', '1.2.0')
    }

    @Test
    public void step17_createAndDeliverMaintenanceFeature2ForV1() {
        helper.test_createMaintenanceFeature(v1, v1_maintenanceBranch, v1_featureName2, v1_featureBranch2, v1_remoteFeatureBranch2)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch2)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch2)
        helper.test_deliverMainenanceFeature(v1, v1_featureBranch2, v1_remoteFeatureBranch2)
    }

    @Test
    public void step18_integrateMaintenanceFeature2ForV1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { v1_featureBranch2 == it }

        helper.test_integrateMaintenenceFeature(v1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { v1_featureBranch2 == it }
    }

    @Test
    public void step19_createAndDeliverMaintenanceFeature3ForV1() {
        helper.test_createMaintenanceFeature(v1, v1_maintenanceBranch, v1_featureName3, v1_featureBranch3, v1_remoteFeatureBranch3)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch3)
        helper.test_deliverMainenanceFeature(v1, v1_featureBranch3, v1_remoteFeatureBranch3)
    }
    
    @Test
    public void step20_cancelIntegrationMaintenanceFeature3ForV1() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. pre-release branch exists on central
        assert branchesOf(env.centralRepo).any { v1_preReleaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. show status - not mandatory to do this before cancel integration, we just test it here as well
        IntegratorActions.newShowMaintenanceIntegrationStatusAction(ext, integratorGit, v1 as String).run()
        // 2. cancel integration
        IntegratorActions.newCancelMaintenanceIntegrationAction(ext, integratorGit, v1 as String).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. pre-release branch no longer exists on central
        assert ! branchesOf(env.centralRepo).any { v1_preReleaseBranch == it }
    }

    @Test
    public void step21_discardMaintenanceFeature3ForV1() {
        helper.test_discardMaintenanceFeature(v1, v1_featureName3, v1_featureBranch3, v1_remoteFeatureBranch3)
    }

    @Test
    public void step22_createVersion1_2_1() {
        helper.test_createNewMaintenanceVersion(v1, 'patch', '1.2.1')
    }

    @Test
    public void step23_recreateMaintenanceFeature3ForV1() {
        helper.test_createMaintenanceFeature(v1, v1_maintenanceBranch, v1_featureName3, v1_featureBranch3, v1_remoteFeatureBranch3)
        helper.test_workOnMaintenanceFeatureBranch(v1, v1_featureBranch3)
    }

    @Test
    public void step24_createAndDeliverFeature5() {
        helper.test_createFeature(featureName5, featureBranch5, remoteFeatureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_deliverFeature(featureBranch5, remoteFeatureBranch5)
    }

    @Test
    public void step25_deliver3RdMaintenanceFeatureForV1() {
        helper.test_goToMaintenanceFeature(v1, v1_featureName3, v1_featureBranch3)
        helper.test_deliverMainenanceFeature(v1, v1_featureBranch3, v1_remoteFeatureBranch3)
    }

    @Test
    public void step26_integrateMaintenanceFeature3ForV1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { v1_featureBranch3 == it }

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateMaintenenceFeature(v1)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { v1_featureBranch3 == it }
    }

    @Test
    public void step27_integrateFeature5() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch5 == it }

        helper.test_integrateFeature()

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch5 == it }
    }

    @Test
    public void step28_createVersion3_0_0() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('major', '3.0.0')
    }

    @Test
    public void step29_createMaintenanceBranchForV2() {
        helper.test_createMaintenanceBranch(v2, v2_maintenanceBranch, v2_remoteMaintenanceBranch)
    }

    @Test
    public void step30_createAndDeliverMaintenanceFeature1ForV2() {
        helper.test_createMaintenanceFeature(v2, v2_maintenanceBranch, v2_featureName1, v2_featureBranch1, v2_remoteFeatureBranch1)
        helper.test_workOnMaintenanceFeatureBranch(v2, v2_featureBranch1)
        helper.test_workOnMaintenanceFeatureBranch(v2, v2_featureBranch1)
        helper.test_deliverMainenanceFeature(v2, v2_featureBranch1, v2_remoteFeatureBranch1)
    }

    @Test
    public void step31_integrateMaintenanceFeature1ForV2() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { v2_featureBranch1 == it }

        helper.test_integrateMaintenenceFeature(v2)

        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { v2_featureBranch1 == it }
    }

    @Test
    public void step32_createVersion2_1_0() {
        helper.test_createNewMaintenanceVersion(v2, 'minor', '2.1.0')
    }
}
