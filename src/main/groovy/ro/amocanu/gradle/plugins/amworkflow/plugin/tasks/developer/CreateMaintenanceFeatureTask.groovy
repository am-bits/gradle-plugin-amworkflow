/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.developer

import ro.amocanu.gradle.plugins.amworkflow.actions.developer.DeveloperActions
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.BaseWorkflowTask
import ro.amocanu.gradle.plugins.amworkflow.plugin.tasks.TaskParameters
import ro.amocanu.gradle.plugins.amworkflow.util.ProjectUtil


class CreateMaintenanceFeatureTask extends BaseWorkflowTask {

    static final NAME = 'createMaintenanceFeature'

    private static final TASK_PARAMS = [ 
        TaskParameters.MAINTAINED_VERSION, 
        TaskParameters.FEATURE_NAME 
    ]

    public CreateMaintenanceFeatureTask() {
        description = 
            'Creates a maintenance feature branch in the local and central repos, and checks it out. ' + 
            usage(TASK_PARAMS)
    }

    @Override
    protected Runnable makeDelegate(PluginExt ext, GitRepo git) {
        def featureName = ProjectUtil.getProperty(project, TASK_PARAMS[1].id)
        def version = ProjectUtil.getProperty(project, TASK_PARAMS[0].id)
        return DeveloperActions.newCreateMaintenanceFeatureAction(ext, git, featureName, version)
    }
}
