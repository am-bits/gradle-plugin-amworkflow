/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.git

/**
 * This interface models a Git repository that has one remote repository, 
 * the "central" repository.
 */
interface GitRepo {

    /**
     * The absolute path of the repository.
     */
    String rootDir()
    
    /**
     * Call this to release underlying resources. 
     * 
     * No operation must be performed on the GitRepo instance after this method is called.
     */
    void close()

    /**
     * @return All the tags in the repository.
     */
    Iterable<String> allTags()

    /**
     * @return All the tags in the repository that belong to ancestors of HEAD, 
     *         including HEAD itself.
     */
    Iterable<String> allAncestorTags()

    /**
     * @param tagName
     * @return <code>true</code> if the repository has a tag with the given name, 
     *         <code>false</code> otherwise 
     */
    boolean isTag(String tagName)

    /**
     * @return All staged changes in the repository.
     */
    Iterable<String> stagedChanges()

    /**
     * @return All unstaged changes in the repository.
     */
    Iterable<String> unstagedChanges()

    /**
     * @return All changes in the repository (staged and unstaged).
     */
    Iterable<String> changes()

    /**
     * Creates a commit consisting only of the given file, if possible.
     * 
     * If the file is unstaged, it adds it to the staging area first.
     * 
     * If the file is unchanged, or if there are other changes beside this file, throws an exception. 
     * 
     * @param file The path of the file to be committed, relative to the repository root 
     * @param commitMessage A commit message
     */
    void commitSingleFile(String file, String commitMessage)

    /**
     * Creates a tag with the given name.
     * 
     * @param tagName
     */
    void createTag(String tagName)

    /**
     * @param tagName
     * @return The id of the commit where the given tag is placed.
     */
    String commitIdOfTag(String tagName)

    /**
     * @param branchName
     * @return The id of the commit where the given branch is placed.
     */
    String commitIdOfBranch(String branchName)

    /**
     * @param remoteBranchName
     * @return The id of the commit where the given remote-tracking (central) branch is placed.
     */
    String commitIdOfRemoteBranch(String remoteBranchName)

    /**
     * @return The name of the currently checked-out branch, or 
     *         <code>null</code> if repository is in detached head.
     */
    String currentBranch()

    /**
     * Checks out the branch with the given name. Does nothing if already checked out.
     * @param branchName
     */
    void checkoutBranch(String branchName)

    /**
     * Creates a local branch with the given name, starting from the given point,
     * and a branch with the same name in the central repository, tracked by the local branch.
     *
     * @param branchName
     * @param startPointBranch The name of a branch that will serve as starting point
     */
    void createLocalAndRemoteBranchAtBranch(String branchName, String startPointBranch)

    /**
     * Creates a local branch with the given name, starting from the given point,
     * and a branch with the same name in the central repository, tracked by the local branch.
     *
     * @param branchName
     * @param startPointRemoteBranch The name of a remote-tracking branch that will serve as starting point
     */
    void createLocalAndRemoteBranchAtRemoteBranch(String branchName, String startPointRemoteBranch)

    /**
     * Creates a local branch with the given name, starting from the given point,
     * and a branch with the same name in the central repository, tracked by the local branch.
     *
     * @param branchName
     * @param startPointTag The name of the tag that will serve as starting point
     */
    void createLocalAndRemoteBranchAtTag(String branchName, String startPointTag)

    /**
     * Creates a local branch with the given name, starting from the given 
     * remote branch and tracking it.
     * 
     * @param branchName
     * @param remoteBranchName
     */
    void createLocalBranchTrackingRemote(String branchName, String remoteBranchName)

    /**
     * @param branchName
     * @param remoteBranchName
     * 
     * @return <code>true</code> if the given branch is tracking the given remote branch 
     *         <code>false</code> otherwise
     */
    boolean isLocalBranchTrackingRemoteBranch(String branchName, String remoteBranchName)
    
    /**
     * Fetches all the branches in the central repository, pruning all the 
     * remote-tracking branches that no longer exist on the central repository.
     * 
     * No tags are fetched.
     */
    void fetchAllBranchesAndPrune()

    /**
     * Fetches all the tags from the central repository.
     * 
     * No branches are fetched.
     */
    void fetchAllTags()

    /**
     * Removes the branch with the given name from the repository.
     * 
     * @param branchName
     */
    void removeBranch(String branchName)

    /**
     * Removes the branch with the given name from the central repository.
     * 
     * @param branchName The name of a branch in the central repository.
     */
    void removeBranchOnCentral(String branchName)

    /**
     * @return All local branch names.
     */
    Iterable<String> localBranches()

    /**
     * @return All names of branches that remote-track branches on the central repository.
     */
    Iterable<String> remoteTrackingCentralBranches()

    /**
     * @param branchName
     * @return <code>true</code>, if the given name belongs to a local branch,
     *         <code>false</code>, otherwise
     */
    boolean isLocalBranch(String branchName)

    /**
     * @param branchName
     * @return <code>true</code>, if the given name belongs to a branch on the central repository,
     *         <code>false</code>, otherwise
     */
    boolean isCentralBranch(String branchName)

    /**
     * Pushed the given branch to the central repository.
     * 
     * @param branchName
     */
    void pushBranchToCentral(String branchName)

    /**
     * Pushes all the tags to the central repository.
     */
    void pushTagsToCentral()

    /**
     * Pushed the given branch to the central repository together with any existing tags.
     *
     * @param branchName
     */
    void pushBranchToCentralWithTags(String branchName)
    
    enum Relation {
        SAME, ANCESTOR, DESCENDANT, NONE
    }

    /**
     * @param firstCommitId
     * @param secondCommitId
     *
     * @return The {@link Relation relation} of the first commit to the second commit.
     */
    Relation relation(String firstCommitId, String secondCommitId)

    /**
     * Merges the given branch into the currently checked-out branch in fast-forward-only mode.
     * 
     * Throws exception if fast-forward is not possible.
     * 
     * @param someOtherBranch
     */
    void fastForwardCurrentBranchTo(String someOtherBranch)
    
    /**
     * Resets the currently checked-out branch to the commit with the given id
     * in the local repository.
     * 
     * @param commitId
     */
    void resetCurrentBranchToCommit(String commitId)
    
    /**
     * @return <code>true</code> if the repository is in the detached head state, 
     *         <code>false</code> otherwise
     */
    boolean isDetachedHead()

    
}
