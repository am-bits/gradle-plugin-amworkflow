/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.DevTestHelper
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class CreateMaintenanceBranchTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'version_',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))
    
    private static final int v0 = 0

    private static final int v1 = 1
    private static final String v1_maintenanceBranch = 'maintenance_version_1'
    private static final String v1_remoteMaintenanceBranch = 'origin/maintenance_version_1'

    private static final String featureName1 = 'so_long_and_thanks_for_all_the_fish'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = "don't_panic"
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String featureName3 = "oh_no_not_again"
    private static final String featureBranch3 = "feature/${featureName3}"
    private static final String remoteFeatureBranch3 = "origin/feature/${featureName3}"

    private static final String someRandomBranch = 'the_answer_is_42'

    private static final String releaseBranch = ext.versioned.mainBranch

    private static final PluginExt ext2 = new PluginExt(
        root: new RootExtension(
            workflowType: ext.root.workflowType),
        versioned: new VersionedExtension(
            centralGitRepository: ext.versioned.centralGitRepository,
            featureBranchPrefix: ext.versioned.featureBranchPrefix,
            preReleaseBranch: ext.versioned.preReleaseBranch,
            mainBranch: ext.versioned.mainBranch,
            mainIntegrationURLTrigger: ext.versioned.mainIntegrationURLTrigger,
            ciUsername: ext.versioned.ciUsername,
            ciPassword: ext.versioned.ciPassword,
            versionPrefix: ext.versioned.versionPrefix,
            versionFile: ext.versioned.versionFile,
            bumpVersionCommitMessage: ext.versioned.bumpVersionCommitMessage,
            maintenanceBranchPrefix: 'MAINTENANCE',
            maintenanceIntegrationURLTriggers: ext.versioned.maintenanceIntegrationURLTriggers))

    def new_v1_maintenanceBranch = "MAINTENANCE_version_1"
    def new_v1_remoteMaintenanceBranch = "origin/MAINTENANCE_version_1"

    private TestEnv env

    private GitRepo devGit
    private GitRepo devGit2
    private GitRepo integratorGit

    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        devGit2 = GrGitRepo.open(env.devRepo2.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        devGit.close()
        devGit2.close()
        integratorGit.close()

        env.tearDown()
    }

    @Test
    public void test_createSameMaintenanceBranchTwice() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        // 2. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 3. create the first version
        helper.test_createFirstVersion()
        // 4. create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
        // 5. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 6. create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')
        // 7. create and deliver a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)
        // 8. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 9. create a new version 2.0.0
        helper.test_createNewVersion('major', '2.0.0')
        // 10. create the maintenance branch for version 1
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)
        // 11. try creating the same maintenance branch for version 1 again
        def e = shouldFail {
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Maintenance branch '${v1_maintenanceBranch}' already exists!" == e.getMessage()
    }

    @Test
    public void test_createMaintenanceBranchTooEarly() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        // 2. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 3. create the first version
        helper.test_createFirstVersion()
        // 4. create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
        // 5. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 6. create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')
        // 7. try creating a maintenance branch for version 1
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Maintenance branch '${v1_maintenanceBranch}' cannot be created before version 2.0.0 is released!" == e.getMessage()
    }

    @Test
    public void test_createMaintenanceBranch_forVersion0() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        // 2. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 3. create the first version (0.1.0)
        helper.test_createFirstVersion()
        // 4. create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
        // 5. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 6. create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')
        // 7. try creating a maintenance branch for version 0
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, v0 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert 'Maintenance branches may only be created starting with version 1!' == e.getMessage()
    }

    @Test
    public void test_createMaintenanceBranch_forInexistentVersion() {
        // Test
        // ---------------------------------------------------------------------
        // 1. create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        // 2. integrate the feature
        helper.test_integrateFeature()
        env.integratorRepo.checkout(branch: releaseBranch)
        // 3. create the first version (0.1.0)
        helper.test_createFirstVersion()
        // 4. try creating a maintenance branch for version 1 before creating version 1 itself
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Maintenance branch '${v1_maintenanceBranch}' cannot be created because no appropriate version tag was found!" == e.getMessage()
    }

    @Test
    public void test_createMaintenanceBranch_withInvalidPropertyValue() {
        // Test
        // ---------------------------------------------------------------------
        // 1. try creating a maintenance branch when the version parameter is not an integer
        def invalidVersionNumber = 'v1'
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext, devGit, invalidVersionNumber).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Could not convert '${invalidVersionNumber}' to an integer!" == e.getMessage()
    }

    @Test
    public void test_createSameMaintenanceBranchTwice_withCaseDifferences() {       
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. [integrator, central] integrate the feature
        helper.test_integrateFeature()
        
        // 3. [integrator, central] create the first version
        helper.test_createFirstVersion()

        // 4. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 5. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 6. [integrator, central] create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')

        // 7. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)

        // 8. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 9. [integrator, central] create a new version 2.0.0
        helper.test_createNewVersion('major', '2.0.0')

        // 10. [dev, central] create the maintenance branch for version 1
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the maintenance branches in ext and ext2 differ only by case
        assert ext.versioned.maintenanceBranchPrefix != ext2.versioned.maintenanceBranchPrefix
        assert ext.versioned.maintenanceBranchPrefix.toLowerCase() == ext2.versioned.maintenanceBranchPrefix.toLowerCase()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. [dev2] create the maintenance branch for version 1 again
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext2, devGit, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Maintenance branch '${new_v1_maintenanceBranch}' already exists with a different case: '${v1_maintenanceBranch}'!" == e.getMessage()
    }

    @Test
    public void test_sameMaintenanceBranchCreatedByDifferentDevelopers_withCaseDifferences() {
        DevTestHelper helper2 = new DevTestHelper(env, ext2, devGit2)

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. [integrator, central] integrate the feature
        helper.test_integrateFeature()
        
        // 3. [integrator, central] create the first version
        helper.test_createFirstVersion()

        // 4. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 5. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 6. [integrator, central] create a new version 1.0.0
        helper.test_createNewVersion('major', '1.0.0')

        // 7. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)

        // 8. [integrator, central] integrate the feature
        helper.test_integrateFeature()

        // 9. [integrator, central] create a new version 2.0.0
        helper.test_createNewVersion('major', '2.0.0')

        // 10. [dev, central] create the maintenance branch for version 1
        helper.test_createMaintenanceBranch(v1, v1_maintenanceBranch, v1_remoteMaintenanceBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the maintenance branches in ext and ext2 differ only by case
        assert ext.versioned.maintenanceBranchPrefix != ext2.versioned.maintenanceBranchPrefix
        assert ext.versioned.maintenanceBranchPrefix.toLowerCase() == ext2.versioned.maintenanceBranchPrefix.toLowerCase()
        
        // Test
        // ---------------------------------------------------------------------
        // 1. [dev2] create the maintenance branch for version 1 again
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit2).run()
            DeveloperActions.newCreateMaintenanceBranchAction(ext2, devGit2, v1 as String).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Remote-tracking maintenance branch '${new_v1_remoteMaintenanceBranch}' already exists with a different case: '${v1_remoteMaintenanceBranch}'!" == e.getMessage()
    }
}
