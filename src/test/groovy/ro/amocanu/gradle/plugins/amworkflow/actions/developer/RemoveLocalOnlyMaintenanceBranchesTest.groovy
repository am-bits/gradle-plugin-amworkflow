/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class RemoveLocalOnlyMaintenanceBranchesTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'version_',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String v1_featureName = 'drinkPotion'
    private static final String v1_featureBranch = "maintenance_version_1_feature/${v1_featureName}"
    private static final String v1_remoteFeatureBranch = "origin/maintenance_version_1_feature/${v1_featureName}"

    private static final String someRandomBranch = 'upgradeArmor'

    private static List v1_localFeatureBranches = (1..3).collect { i -> "${v1_featureBranch}-$i" as String }
    private static List localNonFeatureBranches = (1..3).collect { i -> "${someRandomBranch}-$i" as String }

    private static final String mainBranch = ext.versioned.mainBranch
    private static final String v1_maintenanceBranch = 'maintenance_version_1'
    private static final String v1_localPreReleaseBranch = 'maintenance_version_1_preRelease'
    private static final int v1 = 1

    private static final List<String> v2_v3_localMaintenanceBranches = ['maintenance_version_2', 'maintenance_version_3']

    private static final String v4_maintenanceBranch = 'maintenance_version_4'

    private static final String v4_featureName = 'buyElixir'
    private static final String v4_featureBranch = "maintenance_version_4_feature/${v4_featureName}"
    private static final String v4_remoteFeatureBranch = "origin/maintenance_version_4_feature/${v4_featureName}"

    private static List v4_featureBranches = (1..3).collect { i -> "${v4_featureBranch}-$i" as String }

    private static final String central = ext.versioned.centralGitRepository

    private TestEnv env
    private GitRepo devGit
    private GitRepo integratorGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        env.devRepo.checkout(branch: v1_maintenanceBranch, createBranch: true)
        pushBranchToRemote(env.devRepo, central, v1_maintenanceBranch)

        localNonFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        (v1_localFeatureBranches + [v1_localPreReleaseBranch] + v2_v3_localMaintenanceBranches).each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        v4_featureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
            pushBranchToRemote(env.devRepo, central, branch)
        }

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev, central] several local-only v1 feature-branches exist
        assert branchesOf(env.devRepo).asList().containsAll(v1_localFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in v1_localFeatureBranches }

        // 2. [dev, central] a local-only v1 pre-release branch exists
        assert branchesOf(env.devRepo).asList().contains(v1_localPreReleaseBranch)
        assert ! branchesOf(env.centralRepo).asList().contains(v1_localPreReleaseBranch)

        // 3. [dev, central] several local-only maintenance-branches exist (v2, v3)
        assert branchesOf(env.devRepo).asList().containsAll(v2_v3_localMaintenanceBranches)
        assert ! branchesOf(env.centralRepo).any { it in v2_v3_localMaintenanceBranches }

        // 4. [dev, central] several local-only non-feature branches exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localNonFeatureBranches }

        // 5. [dev, central] several v4 feature-branches exist, but no v4 branch exists
        assert branchesOf(env.devRepo).asList().containsAll(v4_featureBranches)
        assert branchesOf(env.centralRepo).any { it in v4_featureBranches }

        assert ! branchesOf(env.devRepo).asList().contains(v4_maintenanceBranch)
        assert ! branchesOf(env.centralRepo).asList().contains(v4_maintenanceBranch)

        // 6. [dev] no other local branches exist except the main branch and the v1 maintenance branch
        assert [mainBranch, v1_maintenanceBranch].sort() ==
        branchesOf(env.devRepo).asList().minus(
            v1_localFeatureBranches + 
            [v1_localPreReleaseBranch] + 
            v2_v3_localMaintenanceBranches + 
            localNonFeatureBranches + 
            v4_featureBranches).sort()
    }

    @After
    public void tearDown() {
        devGit.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onCreateFeature() {
        helper.test_createMaintenanceFeature(v1, v1_maintenanceBranch, v1_featureName, v1_featureBranch, v1_remoteFeatureBranch)

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onGoToFeature() {
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToMaintenanceFeatureAction(ext, devGit, v1_featureName, v1 as String).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onDeliverFeature() {
        env.devRepo.checkout(branch: v1_featureBranch, createBranch: true)
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onDiscardFeature() {
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardMaintenanceFeatureAction(ext, devGit, v1_featureName, v1 as String).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onSyncWithCentral() {
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
        checkSyncResults()
    }

    private void checkSyncResults() {
        // 1. [dev] the local-only v1 feature-branches disappeared
        assert ! branchesOf(env.devRepo).any { it in v1_localFeatureBranches }

        // 2. [dev] the local-only v1 pre-release branch disappeared
        assert ! branchesOf(env.devRepo).asList().contains(v1_localPreReleaseBranch)

        // 3. [dev] the local-only v2, v3 maintenance branches disappeared
        assert ! branchesOf(env.devRepo).any { it in v2_v3_localMaintenanceBranches }

        // 4. [dev] the v4 feature branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(v4_featureBranches)

        // 5. [dev] the other local-only branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
    }
}
