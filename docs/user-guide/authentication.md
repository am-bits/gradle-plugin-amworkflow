# Authenticating with the central repo
This page describes the various options for Git authentication when using *am*workflow.

<hr/>

## Over HTTP

When using [Git over HTTP](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols#The-HTTP-Protocols),
the Git credentials (username and password) can be stored in the *am*workflow configuration:

```no-highlight
amworkflow {
  workflowType = 'CONTINUOUS'
  continuous {
    // 'myGitUsername' and 'myGitPassword' should be defined in each team member's private gradle.properties file
    gitUsername = myGitUsername
    gitPassword = myGitPassword
  }
}
```

```no-highlight
amworkflow {
  workflowType = 'VERSIONED'
  versioned {
    // 'myGitUsername' and 'myGitPassword' should be defined in each team member's private gradle.properties file
    gitUsername = myGitUsername
    gitPassword = myGitPassword
  }
}
```

If `gitUsername` and `gitPassword` are not specified, Git authentication will be interactive.

!!! note
    We strongly recommended that you store the credentials in the *am*workflow configuration, or else you will be prompted to enter them several times during each *am*workflow task.

<hr/>

## Over SSH

### SSH with password authentication

[Git over SSH](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols#The-SSH-Protocol) with password authentication functions exactly the same as Git over HTTP. You can store the username and the password in the *am*workflow configuration; leave them undefined and the authentication will become interactive.

<hr/>

### SSH with public key authentication

If you use public key authentication, leave the `gitUsername` and `gitPassword` properties undefined.

#### Without passphrase

Make sure your private key is stored at the default location (`~/.ssh/id_rsa`).

You don't need to do anything else.

#### With passphrase

If **your private key is protected by a passphrase**, we strongly recommended that you use [ssh-agent](http://linux.die.net/man/1/ssh-agent). Otherwise, you will be prompted to enter the passphrase several times during each *am*workflow task.

You can start `ssh-agent` manually, like this:

```bash
eval `ssh-agent`
ssh-add [non-standard-identity-file-path]
```

You should now be able to use *am*workflow without having the passphrase prompt pop up all the time.

!!! warning
    You shouldn't really start the agent like this, except for quickly testing that it works. Read more about [starting](http://mah.everybody.org/docs/ssh#run-ssh-agent) [the agent](http://rabexc.org/posts/pitfalls-of-ssh-agents#starting-the-agent).

## Possible issues over SSH

### 1. SSH banner pop-up keeps appearing

The only workaround is to disable the banner from the [sshd config](http://linux.die.net/man/5/sshd_config) on the server.

### 2. "The authenticity of host 'xxx' can't be established." pop-up keeps appearing

This happens if your [SSH config](http://linux.die.net/man/5/ssh_config) has `StrictHostKeyChecking` enabled (default), but you don't have the **RSA** key of the Git server host in your `known_hosts` file. You can check easily if this is the case:

```bash
cat ~/.ssh/known_hosts
```

If the output is something like this, then you shouldn't have this issue:

```bash
[...]
your-Git-server-host ssh-rsa AAAA...
# uses ssh-rsa => OK
```

However, if it looks like this (this is just an example), your are not using a RSA key:

```bash
[...]
your-Git-server-host ecdsa-sha2-nistp256 AAAA...
# doesn't use ssh-rsa => NOT OK
```

**To fix this**, add the RSA key of the Git server to your `known_hosts`. This is one way to do it (very likely, not the smartest):

```bash
# 1. Back up and remove known_hosts:
mv ~/.ssh/known_hosts ~/.ssh/known_hosts.bak

# 2. ssh into the Git server host using 'ssh-rsa':
ssh user@your-Git-server-host -o HostKeyAlgorithms=ssh-rsa -o FingerprintHash=md5

# 3. Answer 'yes' to add the server's key to your new known_hosts (though, be sure that you trust the fingerprint!), then exit the ssh session. You now have the RSA key of the host in your known_hosts file.

# 4. Restore your original known_hosts with the new key added to it:
cat ~/.ssh/known_hosts >> ~/.ssh/known_hosts.bak
mv ~/.ssh/known_hosts.bak ~/.ssh/known_hosts
```
