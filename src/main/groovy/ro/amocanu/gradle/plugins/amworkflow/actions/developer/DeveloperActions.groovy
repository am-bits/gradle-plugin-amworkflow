/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import ro.amocanu.gradle.plugins.amworkflow.actions.conf.ConfigurationFactory
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt

class DeveloperActions {

    static Runnable newCreateFeatureAction(PluginExt ext, GitRepo git, String featureName) {
        return new CreateFeature(ConfigurationFactory.fromExtension(ext), git, featureName)
    }
    
    static Runnable newDeliverFeatureAction(PluginExt ext, GitRepo git) {
        return new DeliverFeature(ext, git)
    }

    static Runnable newDiscardFeatureAction(PluginExt ext, GitRepo git, String featureName) {
        return new DiscardFeature(ConfigurationFactory.fromExtension(ext), git, featureName)
    }

    static Runnable newGoToFeatureAction(PluginExt ext, GitRepo git, String featureName) {
        return new GoToFeature(ConfigurationFactory.fromExtension(ext), git, featureName)
    }

    static Runnable newCreateMaintenanceBranchAction(PluginExt ext, GitRepo git, String version) {
        return new CreateMaintenanceBranch(ext, git, version)
    }

    static Runnable newCreateMaintenanceFeatureAction(PluginExt ext, GitRepo git, String featureName, String version) {
        return new CreateFeature(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, version), git, featureName)
    }
    
    static Runnable newDiscardMaintenanceFeatureAction(PluginExt ext, GitRepo git, String featureName, String version) {
        return new DiscardFeature(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, version), git, featureName)
    }

    static Runnable newGoToMaintenanceFeatureAction(PluginExt ext, GitRepo git, String featureName, String version) {
        return new GoToFeature(ConfigurationFactory.fromVersionedExtensionWithMaintenedVersion(ext.versioned, version), git, featureName)
    }

    static Runnable newSyncWithCentralAction(PluginExt ext, GitRepo git) {
        return new SyncWithCentral(ext, git)
    }
}
