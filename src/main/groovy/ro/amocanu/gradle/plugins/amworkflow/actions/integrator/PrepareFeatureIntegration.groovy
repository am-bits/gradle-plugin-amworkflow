/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation

@PackageScope
class PrepareFeatureIntegration extends BaseAction {

    private String initialBranch

    PrepareFeatureIntegration(Configuration conf, GitRepo git) {
        super(conf, git)
    }

    @Override
    void run() {
        repoMustNotHaveChanges()

        fetchFromCentralAndPrune()

        initialBranch = git.currentBranch()
        mustHaveRemoteReleaseBranch()
        checkOutReleaseBranch()

        try {
            syncReleaseBranchWithUpstream()
            mustHaveRemotePreReleaseBranch()
            checkOutPreReleaseBranch()
        } catch (any) {
            checkOutInitialBranch()
            throw any
        }

        try {
            syncPreReleaseBranchWithUpstream()
            preReleaseBranchMustBeAheadOfReleaseBranch()
        } catch (any) {
            cancelIntegration()
            checkOutInitialBranch()

            throw new GradleException(any.getMessage() 
                + "\n\n" 
                + 'Feature integration failed! The feature must be delivered again by the developer after these problems are solved.')
        }
    }

    private void checkOutInitialBranch() {
        if (initialBranch && git.currentBranch() != initialBranch) {
            git.checkoutBranch(initialBranch)
        }
    }

    private void checkOutReleaseBranch() {
        checkOutBranch(conf.releaseBranch)
    }

    private void syncReleaseBranchWithUpstream() {
        syncBranchWithUpstream(conf.releaseBranch)
    }

    private void mustHaveRemotePreReleaseBranch() {
        if (!git.isCentralBranch(conf.preReleaseBranch)) {
            throw new GradleException('No features delivered for release found in the central repository!')
        }
    }

    private void checkOutPreReleaseBranch() {
        checkOutBranch(conf.preReleaseBranch)
    }

    private void syncPreReleaseBranchWithUpstream() {
        syncBranchWithUpstream(conf.preReleaseBranch)
    }

    private void syncBranchWithUpstream(branchName) {
        assert git.currentBranch() == branchName
        
        def upstreamBranchName = "${conf.centralGitRepository}/${branchName}"
        def branchToUpstreamRelation = git.relation(
            git.commitIdOfBranch(branchName),
            git.commitIdOfRemoteBranch(upstreamBranchName))

        def throwException = false
        switch (branchToUpstreamRelation) {
            case Relation.SAME:
                // nothing to do, already sync-ed
                break

            case Relation.ANCESTOR:
                git.fastForwardCurrentBranchTo(upstreamBranchName)
                break

            case Relation.NONE:
                throwException = true
                break

            case Relation.DESCENDANT:
                throwException = true
                break

            default:
                assert false
        }

        if (throwException) {
            throw new GradleException("Branch '${branchName}' cannot be sync-ed with its upstream, '${upstreamBranchName}', by fast-forwarding!")
        }
    }

    private void preReleaseBranchMustBeAheadOfReleaseBranch() {
        def preRelease = conf.preReleaseBranch
        def release = conf.releaseBranch

        def relation = git.relation(
            git.commitIdOfBranch(preRelease), 
            git.commitIdOfBranch(release))

        def throwException = false
        switch (relation) {
            case Relation.DESCENDANT:
            // ok, nothing to do
                break

            case Relation.SAME:
                throwException = true
                break

            case Relation.ANCESTOR:
                throwException = true
                break

            case Relation.NONE:
                throwException = true
                break

            default:
                assert false
        }

        if (throwException) {
            throw new GradleException("Pre-release branch '${preRelease}' is not ahead of the release branch '${release}'!")
        }
    }
    
    private void cancelIntegration() {
        checkOutReleaseBranch()
        deletePreReleaseBranch()
    }

    private void deletePreReleaseBranch() {
        def preRelease = conf.preReleaseBranch

        if (git.isLocalBranch(preRelease)) {
            assert (git.currentBranch() != preRelease)
            git.removeBranch(preRelease)
            println "Removed branch '${preRelease}' from the local repository"
        }

        if (git.isCentralBranch(preRelease)) {
            git.removeBranchOnCentral(preRelease)
            println "Removed branch '${preRelease}' from the central repository"
        }
    }    
}
