/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.operation.ResetOp
import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class PrepareFeatureIntegrationTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))
    
    private static final String featureName1 = 'To+Be'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName2 = 'Or_Not_To_Be'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String someRandomBranch = 'That.Is.The.Question'

    private static final String centralGitRepository = ext.continuous.centralGitRepository
    private static final String releaseBranch = ext.continuous.releaseBranch
    private static final String remoteReleaseBranch = "${centralGitRepository}/${releaseBranch}"
    private static final String preReleaseBranch = ext.continuous.preReleaseBranch
    private static final String remotePreReleaseBranch = "${centralGitRepository}/${preReleaseBranch}"

    private static final String pseudoPreReleaseBranch = 'PRERELEASE'

    private TestEnv env

    private GitRepo devGit
    private GitRepo integratorGit

    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        integratorGit.close()
        devGit.close()

        env.tearDown()
    }

    @Test
    public void test_prepareFeatureIntegration_whenNoFeatureIsDelivered() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            'No features delivered for release found in the central repository!')

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenNoReleaseBranchOnCentral() {
        def ext2 = new PluginExt(
            root: new RootExtension(
                workflowType: ext.root.workflowType),
            continuous: new ContinuousExtension(
                centralGitRepository: ext.continuous.centralGitRepository,
                featureBranchPrefix: ext.continuous.featureBranchPrefix,
                preReleaseBranch: ext.continuous.preReleaseBranch,
                releaseBranch: 'release',
                releaseTagPrefix: ext.continuous.releaseTagPrefix,
                integrationURLTrigger: ext.continuous.integrationURLTrigger,
                ciUsername: ext.continuous.ciUsername,
                ciPassword: ext.continuous.ciPassword))
                
        def releaseBranch2 = ext2.continuous.releaseBranch
        def preReleaseBranch2 = ext2.continuous.preReleaseBranch
        def remoteReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${releaseBranch2}"
        def remotePreReleaseBranch2 = "${ext2.continuous.centralGitRepository}/${preReleaseBranch2}"

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature was delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { releaseBranch2 == it }

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext2, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "Branch '${releaseBranch2}' not found in central repository!" == e.getMessage()

        // 2. the local pre-release branch does not exit on integrator repo
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch2 == it }
        
        // 3. ... but the remote pre-release branch continues to exist on both integrator and central repos
        assert remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch2 == it }
        assert branchesOf(env.centralRepo).any { preReleaseBranch2 == it }

        // 4. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenReleaseBranchIsBehindUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. on integrator repo, the release branch is behind its upstream

        //    this is obtained by integrating a feature branch and then resetting
        //    the release branch to one commit behind in the integrator repo
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        env.integratorRepo.checkout(branch: releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        assert isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // 2. a feature was delivered for release
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateFeature()
    }

    @Test
    public void test_prepareFeatureIntegration_whenReleaseBranchIsAheadOfUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. on integrator repo, the release branch is ahead of its upstream

        //    to obtain this, we simply make a commit directly on the release branch in the integrator repo
        assert isCheckedOut(env.integratorRepo, releaseBranch)
        commitSomething(env.integratorRepo)

        assert isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, releaseBranch),
            remoteBranchTip(env.integratorRepo, remoteReleaseBranch))

        // 2. a feature was delivered for release
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Branch '${releaseBranch}' cannot be sync-ed with its upstream, '${remoteReleaseBranch}', by fast-forwarding!")

        // 2. the pre-release branch still exists on the central repo
        assert branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenReleaseBranchIsParallelToUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. on integrator repo, the release branch is parallel to its upstream

        //    this is obtained by integrating a feature branch
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()
        //    ...  then resetting the release branch to one commit behind in the integrator repo
        env.integratorRepo.checkout(branch: releaseBranch)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        //    ...  and then making a commit on the release branch in the integrator repo
        commitSomething(env.integratorRepo)

        def release = branchTip(env.integratorRepo, releaseBranch)
        def remoteRelease = remoteBranchTip(env.integratorRepo, remoteReleaseBranch)

        assert release != remoteRelease
        assert ! isDescendant(env.integratorRepo, release, remoteRelease)
        assert ! isAncestor(env.integratorRepo, release, remoteRelease)

        // 2. a feature was delivered for release
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Branch '${releaseBranch}' cannot be sync-ed with its upstream, '${remoteReleaseBranch}', by fast-forwarding!")

        // 2. the pre-release branch still exists on the central repo
        assert branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the release branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchIsBehindUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature is delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, the pre-release branch is behind its upstream
        //    this is obtained by fetching the pre-release branch in the integrator repo and resetting it to parent commit
        env.integratorRepo.fetch(remote: centralGitRepository)
        env.integratorRepo.checkout(branch: preReleaseBranch, startPoint: remotePreReleaseBranch, createBranch: true)
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        assert isAncestor(env.integratorRepo,
            branchTip(env.integratorRepo, preReleaseBranch),
            remoteBranchTip(env.integratorRepo, remotePreReleaseBranch))

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateFeature()
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchIsAheadOfUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature is delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, the pre-release branch is ahead of its upstream
        //    this is obtained by fetching the pre-release branch in the integrator repo and committing something on it
        env.integratorRepo.fetch(remote: centralGitRepository)
        env.integratorRepo.checkout(branch: preReleaseBranch, startPoint: remotePreReleaseBranch, createBranch: true)
        commitSomething(env.integratorRepo)

        assert isDescendant(env.integratorRepo,
            branchTip(env.integratorRepo, preReleaseBranch),
            remoteBranchTip(env.integratorRepo, remotePreReleaseBranch))

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Branch '${preReleaseBranch}' cannot be sync-ed with its upstream, '${remotePreReleaseBranch}', by fast-forwarding!")

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchIsParallelToUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature is delivered for release
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. on integrator repo, the pre-release branch is parallel with its upstream
        //    this is obtained by fetching the pre-release branch in the integrator repo
        env.integratorRepo.fetch(remote: centralGitRepository)
        env.integratorRepo.checkout(branch: preReleaseBranch, startPoint: remotePreReleaseBranch, createBranch: true)
        //    ...  then resetting the pre-release branch to one commit behind
        env.integratorRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        //    ...  and then making a commit on the release branch
        commitSomething(env.integratorRepo)

        def preRelease = branchTip(env.integratorRepo, preReleaseBranch)
        def remotePreRelease = remoteBranchTip(env.integratorRepo, remotePreReleaseBranch)

        assert preRelease != remotePreRelease
        assert ! isDescendant(env.integratorRepo, preRelease, remotePreRelease)
        assert ! isAncestor(env.integratorRepo, preRelease, remotePreRelease)

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Branch '${preReleaseBranch}' cannot be sync-ed with its upstream, '${remotePreReleaseBranch}', by fast-forwarding!")

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseAndReleaseAreTheSame() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the pre-release branch does not exist on the central repo
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. on central repo, the pre-release branch is pointing to the same commit as the release branch
        //    this is obtained by creating a feature without working on it
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        // ... then creating the pre-release branch manually
        env.devRepo.checkout(branch: preReleaseBranch, startPoint: featureBranch1, createBranch: true)
        // ... and then pushing it to central
        env.devRepo.push(remote: centralGitRepository)
        
        assert branchTip(env.centralRepo, featureBranch1) == branchTip(env.centralRepo, releaseBranch)

        // 2. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Pre-release branch '${preReleaseBranch}' is not ahead of the release branch '${releaseBranch}'!")

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchIsBehindReleaseBranch() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature is integrated
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()
        
        // 2. on central repo, the pre-release branch is behind the release branch
        //    this is obtained creating the preRelease branch manually in the dev repo
        env.devRepo.checkout(branch: preReleaseBranch, startPoint: featureBranch1, createBranch: true)
        // ... resetting it to the parent commit of the delivered feature
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        // ... and then pushing it to central
        env.devRepo.push(remote: centralGitRepository)

        assert isAncestor(env.centralRepo,
            branchTip(env.centralRepo, preReleaseBranch),
            branchTip(env.centralRepo, releaseBranch))

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Pre-release branch '${preReleaseBranch}' is not ahead of the release branch '${releaseBranch}'!")

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchIsParallelToReleaseBranch() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. a feature is integrated
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
        helper.test_integrateFeature()

        // 2. on central repo, the pre-release branch is behind the release branch
        //    this is obtained creating the preRelease branch manually in the dev repo
        env.devRepo.checkout(branch: preReleaseBranch, startPoint: featureBranch1, createBranch: true)
        // ... resetting it to the parent commit of the delivered feature and committing something
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        commitSomething(env.devRepo)
        // ... and then pushing it to central
        env.devRepo.push(remote: centralGitRepository)

        assert branchTip(env.centralRepo, preReleaseBranch) != branchTip(env.centralRepo, releaseBranch)
        assert ! isAncestor(env.centralRepo,
            branchTip(env.centralRepo, preReleaseBranch),
            branchTip(env.centralRepo, releaseBranch))
        assert ! isDescendant(env.centralRepo,
            branchTip(env.centralRepo, preReleaseBranch),
            branchTip(env.centralRepo, releaseBranch))

        // 3. on integrator repo, a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }
        

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            "Pre-release branch '${preReleaseBranch}' is not ahead of the release branch '${releaseBranch}'!")

        // 2. pre-release branch does not exit on integrator or central repos
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. on integrator repo, the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenPreReleaseBranchExistsWithADifferentCase() {
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create and push the pseudo-pre-release branch
        env.devRepo.branch.add(name: pseudoPreReleaseBranch)
        pushBranchToRemote(env.devRepo, ext.continuous.centralGitRepository, pseudoPreReleaseBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists with a different case
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        assert preReleaseBranch != pseudoPreReleaseBranch
        assert preReleaseBranch.equalsIgnoreCase(pseudoPreReleaseBranch)

        // 2. [integrator] a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. [integrator] prepare feature integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            'No features delivered for release found in the central repository!')

        // 2. [integrator, central] the pre-release branch still does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. [central] the pseudo-pre-release branch still exists
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }
        
        // 4. [integrator] the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenNoFeatureIsDelivered_butATagNamedLikeTheRemotePreReleaseBranchExists() {
        def initialBranch = someRandomBranch

        // Setup
        // ---------------------------------------------------------------------
        // 1. [integrator] create a tag named like the remote pre-release branch
        env.integratorRepo.tag.add(name: remotePreReleaseBranch)

        // 2. [integrator] check out a random branch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the pre-release branch does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).asList().contains(preReleaseBranch)

        // 2. [integrator] a tag named like the remote pre-release branch exists
        assert tagsOf(env.integratorRepo).asList().contains(remotePreReleaseBranch)
        
        // 3. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        // 1. prepare integration
        def e = shouldFail {
            IntegratorActions.newPrepareFeatureIntegrationAction(ext, integratorGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert e.getMessage().startsWith(
            'No features delivered for release found in the central repository!')

        // 2. [integrator, central] the pre-release branch still does not exist 
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 3. [integrator] the initial branch is checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_prepareFeatureIntegration_whenAFeatureIsDelivered_andATagNamedLikeTheRemotePreReleaseBranchExists() {
        def initialBranch = someRandomBranch

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create and deliver a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. [integrator] create a tag named like the remote pre-release branch 
        //    at the same commit as the release branch
        env.integratorRepo.checkout(branch: releaseBranch)
        env.integratorRepo.tag.add(name: remotePreReleaseBranch)

        // 3. [integrator] check out a random branch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists
        assert branchesOf(env.centralRepo).asList().contains(preReleaseBranch)

        // 2. [integrator] the pre-release branch does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }

        // 3. [integrator] a tag named like the remote pre-release branch exists on the release branch tip
        assert tagsOf(env.integratorRepo).asList().contains(remotePreReleaseBranch)
        assert branchTip(env.integratorRepo, releaseBranch) == commitOfTag(env.integratorRepo, remotePreReleaseBranch)

        // 4. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        helper.test_integrateFeature()
    }
}
