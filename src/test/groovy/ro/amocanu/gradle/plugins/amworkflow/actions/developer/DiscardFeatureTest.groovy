/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class DiscardFeatureTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'supercalifragilisticexpialidocious'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName1b = 'SuperCaliFragilisticExpialidocious'

    private static final String featureName2 = 'openUmbrella'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"

    private static final String someRandomBranch = 'dociousaliexpilisticfragicalirupes'

    private static final String releaseBranch = ext.continuous.releaseBranch

    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, null)
    }

    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_discardFeature() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void test_discardInexistentFeature() {
        // Preconditions
        // 1. the feature branch does not exist on dev or central repos
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 2. a random branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        assert initialBranch == env.devRepo.branch.current.name

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1}' does not exist!" == e.getMessage()

        // 2. the initial branch is still checked-out
        assert initialBranch == env.devRepo.branch.current.name
    }

    @Test
    public void test_discardFeatureWhenFeatureIsDeliveredButNotIntegrated() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature is delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. the feature branch is checked-out
        helper.test_goToFeature(featureName1, featureBranch1)

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1).run()
        }

        // 1. the correct error message is thrown
        assert "Cannot delete branch '${featureBranch1}' while the feature is delivered but not yet integrated!" == e.getMessage()

        // 2. the feature branch is still checked-out
        assert featureBranch1 == env.devRepo.branch.current.name

    }

    @Test
    public void test_discardFeatureWhenAnotherFeatureIsDeliveredButNotIntegrated() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. feature exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. another feature is delivered
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)

        // Test
        // ---------------------------------------------------------------------
        helper.test_discardFeature(featureName1, featureBranch1, remoteFeatureBranch1)
    }

    @Test
    public void test_discardFeatureWhenFeatureExistsOnlyOnCentral() {
        // Preconditions
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 1. a random branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        assert initialBranch == env.devRepo.branch.current.name

        // 2. the feature exists only on central
        env.devRepo.branch.remove(names: [featureBranch1])

        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 3. the release branch exists in the dev repo
        assert branchesOf(env.devRepo).any { releaseBranch == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. discard the feature
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
        DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. the feature does not exist on dev and central repos
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 2. on dev, the release branch is now checked-out
        assert releaseBranch == env.devRepo.branch.current.name
    }

    @Test
    public void test_discardFeatureIsCaseSensitive() {
        def initialBranch = someRandomBranch
        
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        
        // 2. [dev] checkout a random branch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------      
        // 1. [dev] a random branch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 2. the used feature name differs by case from the name if the existing feature
        assert featureName1 != featureName1b
        assert featureName1.toLowerCase() == featureName1b.toLowerCase()

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName1b).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1b}' does not exist! (Did you mean '${featureName1}'?)" == e.getMessage()
        
        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }
}
