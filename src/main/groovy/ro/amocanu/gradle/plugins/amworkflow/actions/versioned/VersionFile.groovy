/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.versioned

import java.nio.file.Paths

import org.gradle.api.GradleException

class VersionFile {

    private static final String VERSION_PROPERTY_NAME = 'version'
    private static final String VERSION_PROPERTY_FORMAT = '\'(.+)\''
    
    private final String versionFile
    private final String versionFilePath
    
    VersionFile(String versionFile, String rootDir) {
        this.versionFile = versionFile
        this.versionFilePath = Paths.get(rootDir, versionFile).toString()
    }
    
    Version readVersion() {
        File propertiesFile = new File(versionFilePath)
        
        if (!propertiesFile.exists()) {
            throw new GradleException ("File '$versionFilePath' does not exist.")            
        }
        
        Properties properties = new Properties()
        InputStream fileInputStream = propertiesFile.newDataInputStream()
        
        try {
            properties.load(fileInputStream)
        } finally {
            fileInputStream.close()
        }

        def propValue = properties.getProperty(VERSION_PROPERTY_NAME)
        
        if (propValue == null) {
            throw new GradleException (
                "Property '$VERSION_PROPERTY_NAME' is missing from '$versionFile'.")
        }
        
        def m = (propValue =~ VERSION_PROPERTY_FORMAT) 
        if (!m.matches()) {
            throw new GradleException (
                "Value of '${VERSION_PROPERTY_NAME}' property, '${propValue}', does not match expected format: '${VERSION_PROPERTY_FORMAT}'. " +
                "Update '$versionFile' and try again.")
        }

        def versionString = m[0][1]
        if (!Version.isVersionString(versionString)) {
            throw new GradleException (
                "Version '$versionString' does not match expected format: '${Version.VERSION_FORMAT}'. " +
                "Update '$versionFile' and try again.")
        }

        return Version.fromString(versionString)
    }

    void writeVersion(Version version) {
        assert version != null
        
        File propertiesFile = new File(versionFilePath)
        assert propertiesFile.exists()
        
        FileOutputStream out = new FileOutputStream(propertiesFile)
        try {
            propertiesFile.write("${VERSION_PROPERTY_NAME}='${version.toString()}'")
            out.getFD().sync()
            println "Version '${version}' written to '${versionFile}'"
        } finally {
            out.close()
        }
    }
}
