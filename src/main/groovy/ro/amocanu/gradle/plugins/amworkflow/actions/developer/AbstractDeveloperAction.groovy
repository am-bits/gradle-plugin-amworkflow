/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo

/**
 * Note: It is assumed that any AbstractDeveloperAction is run only if SyncWithCentral
 *       was run and finished successfully. This is enforced by a 'dependsOn'
 *       relation between the developer tasks and the 'syncWithCentral' task.
 */
@PackageScope
abstract class AbstractDeveloperAction extends BaseAction {

    private String initialBranch

    AbstractDeveloperAction(Configuration conf, GitRepo git) {
        super (conf, git)
    }

    @Override
    void run() {
        initialBranch = git.currentBranch()
        checkOutReleaseBranch()

        try {
            centralMustNotHavePseudoFeatureBranches()
            runDeveloperAction()
        } catch (any) {
            checkOutInitialBranch()
            throw (any)
        }
    }

    abstract protected void runDeveloperAction()

    private void checkOutReleaseBranch() {
        checkOutBranch(conf.releaseBranch)
    }

    private void centralMustNotHavePseudoFeatureBranches() {
        def remotePrefix = conf.centralGitRepository + '/'
        def remainingRemoteBranches = git.remoteTrackingCentralBranches()
        def centralPseudoFeatureBranches = 
            remainingRemoteBranches
                .findAll { remoteBranch ->
                    def branch = remoteBranch[remotePrefix.size()..-1]
                    conf.isPseudoFeatureBranch(branch)
                }
                .collect { remoteBranch ->
                    remoteBranch[remotePrefix.size()..-1]
                }
        
        if (centralPseudoFeatureBranches) {
            throw new GradleException("Illegal pseudo-feature branches found on central: ${centralPseudoFeatureBranches}!")
        }
    }

    private void checkOutInitialBranch() {
        if (initialBranch && initialBranch != git.currentBranch()) {
            def initial = initialBranch
            if (git.localBranches().any { it.equalsIgnoreCase(initial) }) {
                git.checkoutBranch(initialBranch)
            }
        }
    }
}
