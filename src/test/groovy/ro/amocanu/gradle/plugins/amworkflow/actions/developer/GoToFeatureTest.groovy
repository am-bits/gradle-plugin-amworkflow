/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitOfTag
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.ajoberstar.grgit.operation.ResetOp
import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class GoToFeatureTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'CatchThatChocobo!'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String featureName1b = 'CatchThatCHOCOBO!'

    private static final String featureName2 = 'Summon/GF'
    private static final String featureBranch2 = "feature/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/feature/${featureName2}"
       
    private static final String someRandomBranch = 'Equip/Materia'

    private static final String releaseBranch = ext.continuous.releaseBranch

    private TestEnv env
    private GitRepo devGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        helper = new TestHelper(env, ext, devGit, null)
    }
    
    @After
    public void tearDown() {
        devGit.close()
        env.tearDown()
    }

    @Test
    public void test_goToInexistentFeature() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature branch does not exist on dev or central repos
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        // 2. a random branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)
        
        assert initialBranch == env.devRepo.branch.current.name
        
        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1}' does not exist!" == e.getMessage()
        
        // 2. the initial branch is still checked-out
        assert initialBranch == env.devRepo.branch.current.name
    }

    @Test
    public void test_goToSameFeatureTwice() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        
        // 2. another (random) branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)
        
        assert initialBranch == env.devRepo.branch.current.name
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)
        helper.test_goToFeature(featureName1, featureBranch1)
    }  

    @Test
    public void test_goToFeatureWhenFeatureExistsOnlyOnCentral() {
        // Preconditions
        // ---------------------------------------------------------------------
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 1. a random branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        assert initialBranch == env.devRepo.branch.current.name

        // 2. the feature exists only on central
        env.devRepo.branch.remove(names: [featureBranch1])
        
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        // Test
        // ---------------------------------------------------------------------
        // 1. [dev] sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. [dev] go to feature
        DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [dev] the feature branch was created at the same commit as the remote feature branch 
        assert branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchTip(env.devRepo, remoteFeatureBranch1) == branchTip(env.devRepo, featureBranch1)
        
        // 2. [dev] the feature branch is tracking the remote feature branch
        assert isTracking(env.devRepo, featureBranch1, remoteFeatureBranch1)

        // 3. [dev] the feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
    }
    
    @Test
    public void test_goToFeatureWhenAheadOfUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists and is ahead of its upstream
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranchLocally(featureBranch1)

        assert isDescendant(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        def featureBranchBeforeTest = branchTip(env.devRepo, featureBranch1)
        
        // 2. a random branch is checked-out
        def initialBranch = someRandomBranch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)
        
        assert initialBranch == env.devRepo.branch.current.name
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. go to feature
        DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [dev] the feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
        
        // 2. [dev] the feature branch did not move
        assert featureBranchBeforeTest == branchTip(env.devRepo, featureBranch1)
    }
    
    @Test
    public void test_goToFeatureWhenBehindUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the feature exists and is behind its upstream
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)

        assert isAncestor(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)
        
        // Check results
        // ---------------------------------------------------------------------
        // 1. on dev, the feature branch is now sync-ed with its upstream
        assert branchTip(env.devRepo, featureBranch1) == remoteBranchTip(env.devRepo, remoteFeatureBranch1)
    }
    
    @Test
    public void test_goToFeatureWhenParallelWithUpstream() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. the local feature branch and its upstream evolved in parallel
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        
        helper.test_workOnFeatureBranch(featureBranch1)
        env.devRepo.reset(commit: 'HEAD~1', mode: ResetOp.Mode.HARD)
        
        helper.test_workOnFeatureBranchLocally(featureBranch1)

        assert ! isAncestor(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        assert ! isDescendant(env.devRepo,
            branchTip(env.devRepo, featureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        assert branchTip(env.devRepo, featureBranch1) != remoteBranchTip(env.devRepo, remoteFeatureBranch1)
        
        def featureBranchBeforeTest = branchTip(env.devRepo, featureBranch1)
        
        // Test
        // ---------------------------------------------------------------------
        // 1. sync with central
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()

        // 2. go to feature
        DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [dev] the feature branch is checked-out
        assert isCheckedOut(env.devRepo, featureBranch1)
        
        // 2. [dev] the feature branch did not move
        assert featureBranchBeforeTest == branchTip(env.devRepo, featureBranch1)
    }

    @Test
    public void test_goToFeatureIsCaseSensitive() {
        def initialBranch = someRandomBranch
        
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create a feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        
        // 2. [dev] checkout a random branch
        env.devRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------      
        // 1. [dev] a random branch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 2. the used feature name differs by case from the name if the existing feature
        assert featureName1 != featureName1b
        assert featureName1.toLowerCase() == featureName1b.toLowerCase()

        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1b).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1b}' does not exist! (Did you mean '${featureName1}'?)" == e.getMessage()
        
        // 2. the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_goToInexistentFeature_whenATagIsNamedLikeTheRemoteFeatureBranch() {
        def initialBranch = someRandomBranch

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] commit something on the release branch
        env.devRepo.checkout(branch: releaseBranch)
        commitSomething(env.devRepo)

        // 2. [dev] create a tag named like the remote feature branch
        env.devRepo.tag.add(name: remoteFeatureBranch1)

        // 3. [dev] check out a random branch
        env.devRepo.checkout(branch: initialBranch, createBranch: true, startPoint: releaseBranch)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a tag named like the remote feature branch exists
        assert tagsOf(env.devRepo).asList().contains(remoteFeatureBranch1)

        // 2. [dev, central] the feature branch does not exist
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert ! remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        // 3. [dev] a random branch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
        
        // Test
        // ---------------------------------------------------------------------
        def e = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName1).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the correct error message is thrown
        assert "Feature '${featureName1}' does not exist!" == e.getMessage()
        
        // 2. [dev] the initial branch is still checked-out
        assert isCheckedOut(env.devRepo, initialBranch)
    }

    @Test
    public void test_goToFeature_whenFeatureExistsOnlyOnCentral_andADescendantTagNamedLikeTheRemoteFeatureBranchExists() {
        def initialBranch = someRandomBranch
        
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create the feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. [dev] commit something on the local feature branch
        env.devRepo.checkout(branch: featureBranch1)
        commitSomething(env.devRepo)
        
        // 3. [dev] create a tag named like the remote feature branch
        env.devRepo.tag.add(name: remoteFeatureBranch1)

        // 4. [dev] check out a random branch
        env.devRepo.checkout(branch: initialBranch, createBranch: true, startPoint: releaseBranch)

        // 5. [dev] remove the local feature branch
        env.devRepo.branch.remove(names: [featureBranch1], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a random branch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 2. [central] the feature branch exists       
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 3. [dev] only the remote-tracking feature branch exists
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }

        // 4. [dev] a tag named like the remote feature branch exists
        assert tagsOf(env.devRepo).asList().contains(remoteFeatureBranch1)

        // 5. [dev] ...and it is placed on a commit descending from the remote feature branch
        assert isDescendant(env.devRepo,
            commitOfTag(env.devRepo, remoteFeatureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)
    }

    @Test
    public void test_goToFeature_whenFeatureExistsOnlyOnCentral_andAnAncestorTagNamedLikeTheRemoteFeatureBranchExists() {
        def initialBranch = someRandomBranch
        
        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] create the feature
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)

        // 2. [dev] create a tag named like the remote feature branch
        env.devRepo.tag.add(name: remoteFeatureBranch1)

        // 3. [dev, central] work on feature
        helper.test_workOnFeatureBranch(featureBranch1)

        // 4. [dev] check out a random branch
        env.devRepo.checkout(branch: initialBranch, createBranch: true, startPoint: releaseBranch)

        // 5. [dev] remove the local feature branch
        env.devRepo.branch.remove(names: [featureBranch1], force: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev] a random branch is checked-out
        assert isCheckedOut(env.devRepo, initialBranch)

        // 2. [central] the feature branch exists
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }

        // 3. [dev] only the remote-tracking feature branch exists
        assert ! branchesOf(env.devRepo).any { featureBranch1 == it }
        assert remoteBranchesOf(env.devRepo).any { remoteFeatureBranch1 == it }

        // 4. [dev] a tag named like the remote feature branch exists
        assert tagsOf(env.devRepo).asList().contains(remoteFeatureBranch1)

        // 5. [dev] ...and it is placed on an ancestor commit to the remote feature branch
        assert isAncestor(env.devRepo,
            commitOfTag(env.devRepo, remoteFeatureBranch1),
            remoteBranchTip(env.devRepo, remoteFeatureBranch1))
        
        // Test
        // ---------------------------------------------------------------------
        helper.test_goToFeature(featureName1, featureBranch1)
    }
}
