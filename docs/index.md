# *am*workflow

Centralized, feature-driven Git workflow for your team, via Gradle tasks!

___

## Overview

*am*workflow is a [Gradle](http://gradle.org) plugin for development teams that use [Git](https://git-scm.com) and practice a [centralized](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows#Centralized-Workflow), [feature-driven](https://en.wikipedia.org/wiki/Feature-driven_development) Git workflow.

### Gradle tasks

A Gradle task for each workflow step. Make your team's Git workflow painless and safe, and mitigate lack of Git knowledge!

### For developers and integrators

Meant to be used by all the developers and integrator(s) in a team. The integrator may be a [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration) server.

### Continuous or versioned delivery style

Supports both [continuous delivery](https://en.wikipedia.org/wiki/Continuous_delivery), as well as a more traditional delivery style based on [semantic versioning](http://semver.org).

___

## How it works

  - Developers make all their commits on **feature branches**.
  - The lifecycle of feature branches is **completely manageable using *am*workflow tasks**!
  - Completed features are promoted to the **release branch**, **one at a time**, via a **transient pre-release branch**.
  - The **pre-release branch is transparently managed** by *am*workflow. Only the integrator must be aware of it, because
  - ...the integrator's job is to build and test the pre-release branch (among other things).
  - If the integrator is a CI server, *am*workflow can be configured to **trigger the pre-release build automatically** via a URL hook.
  - If the pre-release build passes, the integrator merges the pre-release branch into the permanent release branch... using an *am*workflow task, of course.
  - *am*workflow **prunes its own branches automatically** from the central and local repos after they are no longer needed.

<hr class="gradient_divider"/>

**For continuous delivery:**

  - On every successful integration, *am*workflow automatically creates a **timestamp-based tag** on the release branch.

![Git tree](img/features-continuous.png "An example of Git tree when doing continuous delivery")

<hr class="gradient_divider"/>

**For versioned delivery:**

- *am*workflow supports basic [semantic versioning](http://semver.org) <small>(only the MAJOR.MINOR.PATCH part of the [semver specification](http://semver.org/spec/v2.0.0.html), no [pre-release metadata](http://semver.org/#spec-item-9) or [build metadata](http://semver.org/#spec-item-10)!)</small>
- The team develops the latest major version of the project on the **main branch**.
- Development of older major versions is done on **maintenance branches**, in parallel with the main branch development.
- Ever so often, the integrator creates **releases containing several features** and assigns **semantic version numbers** depending on the contents.

![Git tree](img/features-versioned.png "An example of Git tree when doing versioned delivery")

___

## Start using it!

*am*workflow is published on the [Gradle Plugin Portal](https://plugins.gradle.org/plugin/ro.amocanu.amworkflow). To start using it, paste one of the two snippets below into your `build.gradle` file.

Build script snippet for <a href="https://docs.gradle.org/current/userguide/plugins.html#sec:plugins_block">new plugin mechanism</a> introduced in Gradle 2.1:

```groovy
plugins {
  id "ro.amocanu.amworkflow" version "1.1"
}
```

Build script snippet for use with all Gradle versions:

```groovy
buildscript {
  repositories {
    maven {
      url "https://plugins.gradle.org/m2/"
    }
  }
  dependencies {
    classpath "ro.amocanu:amworkflow:1.1"
  }
}

apply plugin: "ro.amocanu.amworkflow"
```
___

## What's in a name?

If you wonder about the name "*am*workflow", it's down to three things: "A" and "M" are the initials of our names. It's a workflow. Naming things is hard.

Not to be confused with [AMWorkflow](https://developer.apple.com/library/mac/documentation/AppleApplications/Reference/AMWorkflow_class/index.html) of Apple's [Automator Framework](https://developer.apple.com/library/mac/documentation/AppleApplications/Reference/AutomatorReference/index.html).
