/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class IllegalCentralMaintenanceBranchesTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'version_',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'maintenance',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName = 'drinkPotion'
    private static final String featureBranch = "maintenance_version_1_feature/${featureName}"
    private static final String remoteFeatureBranch = "origin/maintenance_version_1_feature/${featureName}"
    private static final String pseudoFeatureBranch = "maintenance_VERSION_1_feature/healParty"

    private static final String preReleaseBranch = 'maintenance_version_1_preRelease'
    private static final String pseudoPreReleaseBranch = 'maintenance_version_1_prerelease'
    
    private static final String someRandomBranch = 'upgradeArmor'

    private static List<String> localNonFeatureBranches = (1..3).collect { i -> "${someRandomBranch}-$i" as String }
    private static List<String> localOnlyPseudoFeatureBranches = (1..3).collect { i -> "${pseudoFeatureBranch}-$i" as String }
    private static List<String> pseudoFeatureBranches = (4..5).collect { i -> "${pseudoFeatureBranch}-$i" as String }

    private static final String mainBranch = ext.versioned.mainBranch
    private static final String maintenanceBranch = 'maintenance_version_1'
    private static final String maintenanceBranchPrefix = 'maintenance_version_' 
    private static final int v1 = 1

    private static final List<String> pseudoMaintenanceBranches = ['Maintenance_version_2', 'maintenance_Version_4']

    private static final String central = ext.versioned.centralGitRepository

    private TestEnv env
    private GitRepo devGit
    private GitRepo integratorGit
    private TestHelper helper

    private Exception exc

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        env.devRepo.checkout(branch: maintenanceBranch, createBranch: true)
        pushBranchToRemote(env.devRepo, central, maintenanceBranch)

        localNonFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }
        
        localOnlyPseudoFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        (pseudoFeatureBranches + [pseudoPreReleaseBranch] + pseudoMaintenanceBranches).each { branch ->
            env.devRepo.branch.add(name: branch)
            pushBranchToRemote(env.devRepo, central, branch)
        }
        
        // Preconditions
        // ---------------------------------------------------------------------
        String featurePrefix = ext.versioned.featureBranchPrefix
        String prefix = pseudoFeatureBranch[0..featurePrefix.size()]

        assert prefix != featurePrefix
        assert prefix.toLowerCase() != featurePrefix.toLowerCase()

        // 1. [dev, central] several local-only pseudo-feature-branches exist
        //    (they have the feature prefix but with a different case)
        assert localOnlyPseudoFeatureBranches.every { it.startsWith(prefix) }

        assert branchesOf(env.devRepo).asList().containsAll(localOnlyPseudoFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev, central] several other pseudo-feature-branches exist
        assert pseudoFeatureBranches.every { it.startsWith(prefix) }

        assert branchesOf(env.devRepo).asList().containsAll(pseudoFeatureBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoFeatureBranches)

        // 3. [dev, central] a pseudo-pre-release-branch exists
        assert pseudoPreReleaseBranch != preReleaseBranch
        assert pseudoPreReleaseBranch.equalsIgnoreCase(preReleaseBranch)

        assert branchesOf(env.devRepo).asList().contains(pseudoPreReleaseBranch)
        assert branchesOf(env.centralRepo).asList().contains(pseudoPreReleaseBranch)

        // 4. [dev, central] several pseudo-maintenance-branches exists
        assert pseudoMaintenanceBranches.every { 
            ! it.startsWith(maintenanceBranchPrefix) && 
            it.toLowerCase().startsWith(maintenanceBranchPrefix.toLowerCase()) }

        assert branchesOf(env.devRepo).asList().containsAll(pseudoMaintenanceBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoMaintenanceBranches)

        // 5. [dev, central] several local-only non-feature branches exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localNonFeatureBranches }

        // 6. [dev, central] no other local branches exist except the main branch and the maintenance branch
        assert [mainBranch, maintenanceBranch].sort() ==
            branchesOf(env.devRepo).asList().minus(
                localOnlyPseudoFeatureBranches + 
                pseudoFeatureBranches + 
                [pseudoPreReleaseBranch] + 
                pseudoMaintenanceBranches +
                localNonFeatureBranches).sort()
    }

    @After
    public void tearDown() {
        devGit.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onCreateFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newCreateMaintenanceFeatureAction(ext, devGit, featureName, v1 as String).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onGoToFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToMaintenanceFeatureAction(ext, devGit, featureName, v1 as String).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onDeliverFeature() {
        env.devRepo.checkout(branch: featureBranch, createBranch: true)

        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        // Check results
        // ---------------------------------------------------------------------
        // 1. the error message is the correct one
        assert "The checked-out branch '${mainBranch}' is not a feature branch, it cannot be delivered!" == exc.message

        // 2. sync results are as expected
        checkSyncResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onDiscardFeature() {
        exc = shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardMaintenanceFeatureAction(ext, devGit, featureName, v1 as String).run()
        }

        checkResults()
    }

    @Test
    public void test_removeLocalPseudoFeatureBranches_onSyncWithCentral() {
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
        checkSyncResults()
    }

    private void checkResults() {
        // 1. the error message is the correct one
        assert "Illegal pseudo-feature branches found on central: ${pseudoFeatureBranches}!" == exc.message

        checkSyncResults()
    }

    private void checkSyncResults() {
        // 1. [dev] all the local-only pseudo-feature-branches disappeared
        assert ! branchesOf(env.devRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev, central] the other pseudo-feature-branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(pseudoFeatureBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoFeatureBranches)

        // 3. [dev, central] the pseudo-pre-release-branch still exists
        assert branchesOf(env.devRepo).asList().contains(pseudoPreReleaseBranch)
        assert branchesOf(env.centralRepo).asList().contains(pseudoPreReleaseBranch)

        // 4. [dev, central] the pseudo-maintenance-branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(pseudoMaintenanceBranches)
        assert branchesOf(env.centralRepo).asList().containsAll(pseudoMaintenanceBranches)

        // 5. [dev] the other local branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(localNonFeatureBranches)
    }
}
