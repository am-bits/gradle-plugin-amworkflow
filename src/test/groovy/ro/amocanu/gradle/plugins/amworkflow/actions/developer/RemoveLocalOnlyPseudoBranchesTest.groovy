/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.developer

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class RemoveLocalOnlyPseudoBranchesTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName = 'drinkPotion'
    private static final String featureBranch = "feature/${featureName}"
    private static final String remoteFeatureBranch = "origin/feature/${featureName}"

    private static final String someRandomBranch = 'upgradeArmor'
    private static final String pseudoFeatureBranch = "Feature/healParty"

    private static List localOnlyNonFeatureBranches = (1..3).collect { i -> "${someRandomBranch}-$i" as String }
    private static List<String> localOnlyPseudoFeatureBranches = (1..3).collect { i -> "${pseudoFeatureBranch}-$i" as String }

    private static final String preReleaseBranch = ext.continuous.preReleaseBranch
    private static final String localOnlyPseudoPreReleaseBranch = 'PrErElEaSe'

    private static final String releaseBranch = ext.continuous.releaseBranch

    private static final String central = ext.continuous.centralGitRepository

    private TestEnv env
    private GitRepo devGit
    private GitRepo integratorGit
    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, central)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, central)
        helper = new TestHelper(env, ext, devGit, integratorGit)

        localOnlyNonFeatureBranches.each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        (localOnlyPseudoFeatureBranches + [localOnlyPseudoPreReleaseBranch]).each { branch ->
            env.devRepo.branch.add(name: branch)
        }

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [dev, central] several local-only pseudo-feature-branches exist
        //    (they have the feature prefix but with a different case)
        String featurePrefix = ext.continuous.featureBranchPrefix
        String prefix = pseudoFeatureBranch[0..featurePrefix.size()-1]

        assert prefix != featurePrefix
        assert prefix.toLowerCase() == featurePrefix.toLowerCase()

        assert localOnlyPseudoFeatureBranches.every { it.startsWith(prefix) }

        assert branchesOf(env.devRepo).asList().containsAll(localOnlyPseudoFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev, central] a local-only pseudo-pre-release-branch exists
        assert localOnlyPseudoPreReleaseBranch != preReleaseBranch
        assert localOnlyPseudoPreReleaseBranch.toLowerCase() == preReleaseBranch.toLowerCase()

        assert branchesOf(env.devRepo).asList().contains(localOnlyPseudoPreReleaseBranch)
        assert ! branchesOf(env.centralRepo).asList().contains(localOnlyPseudoPreReleaseBranch)

        // 3. [dev, central] several local-only non-feature branches exist
        assert branchesOf(env.devRepo).asList().containsAll(localOnlyNonFeatureBranches)
        assert ! branchesOf(env.centralRepo).any { it in localOnlyNonFeatureBranches }

        // 4. [dev] no other local branches exist except the release branch
        assert [releaseBranch] == branchesOf(env.devRepo).asList().minus(
            localOnlyPseudoFeatureBranches + [localOnlyPseudoPreReleaseBranch] + localOnlyNonFeatureBranches)
    }

    @After
    public void tearDown() {
        devGit.close()
        integratorGit.close()
        env.tearDown()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onCreateFeature() {
        helper.test_createFeature(featureName, featureBranch, remoteFeatureBranch)

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onGoToFeature() {
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newGoToFeatureAction(ext, devGit, featureName).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onDeliverFeature() {
        env.devRepo.checkout(branch: featureBranch, createBranch: true)
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDeliverFeatureAction(ext, devGit).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onDiscardFeature() {
        shouldFail {
            DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
            DeveloperActions.newDiscardFeatureAction(ext, devGit, featureName).run()
        }

        checkSyncResults()
    }

    @Test
    public void test_removeLocalOnlyFeatureBranches_onsyncWithCentral() {
        DeveloperActions.newSyncWithCentralAction(ext, devGit).run()
        checkSyncResults()
    }

    private void checkSyncResults() {
        // 1. [dev] all the local pseudo-feature-branches disappeared
        assert ! branchesOf(env.devRepo).any { it in localOnlyPseudoFeatureBranches }

        // 2. [dev] the local pseudo-pre-release-branch disappeared
        assert ! branchesOf(env.devRepo).asList().contains(localOnlyPseudoPreReleaseBranch)

        // 2. [dev] the other local branches still exist
        assert branchesOf(env.devRepo).asList().containsAll(localOnlyNonFeatureBranches)
    }
}
