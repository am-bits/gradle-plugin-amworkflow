# *am*workflow: a Git workflow for your team, via Gradle tasks #

***am*workflow** is a **[Gradle](https://gradle.org/) plugin** aimed at development teams that want to practice a **feature-oriented workflow** using **[Git](https://git-scm.com/)**.

* #### [User documentation](https://am-bits.bitbucket.io/amworkflow/index.html) ####
* #### [Get it from the Gradle plugin portal](https://plugins.gradle.org/plugin/ro.amocanu.amworkflow) ####
* #### [Report a bug or request a feature](https://bitbucket.org/am-bits/gradle-plugin-amworkflow/issues/new) ####
* #### [Change log](CHANGELOG.md) ####
* #### [Apache 2.0 licence](http://www.apache.org/licenses/LICENSE-2.0) ####