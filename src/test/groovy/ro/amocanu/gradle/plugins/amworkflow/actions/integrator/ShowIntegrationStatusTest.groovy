/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.changeFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createAndCommitNewFile
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.createEmptyFileInRepo
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isAncestor
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stageSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.stagingAreaOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.workingDirectoryOf

import org.junit.After
import org.junit.Before
import org.junit.Test

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class ShowIntegrationStatusTest extends BaseTest {
    
    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.CONTINUOUS.toString()),
        continuous: new ContinuousExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'feature',
            preReleaseBranch: 'preRelease',
            releaseBranch: 'master',
            releaseTagPrefix: 'build-',
            integrationURLTrigger: '',
            ciUsername: '',
            ciPassword: ''))

    private static final String featureName1 = 'press-X-to-continue'
    private static final String featureBranch1 = "feature/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/feature/${featureName1}"

    private static final String someRandomBranch = 'GameOver'

    private static final String preReleaseBranch = ext.continuous.preReleaseBranch
    private static final String remotePreReleaseBranch = "${ext.continuous.centralGitRepository}/${preReleaseBranch}"

    private static final String pseudoPreReleaseBranch = 'PRErelease'
    private static final String remotePseudoPreReleaseBranch = "${ext.continuous.centralGitRepository}/${pseudoPreReleaseBranch}"
    
    private TestEnv env

    private GitRepo devGit
    private GitRepo integratorGit

    private TestHelper helper

    @Before
    public void setUp() {
        env = new TestEnv()

        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.continuous.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }

    @After
    public void tearDown() {
        integratorGit.close()
        devGit.close()

        env.tearDown()
    }

    @Test
    public void test_showIntegrationStatus_whenAFeatureIsDelivered() {
        def initialBranch = someRandomBranch

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev, central] a feature was delivered
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)

        // 2. [integrator] checkout a random branch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists
        assert branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. [integrator] the pre-release branch does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }

        // 3. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newShowIntegrationStatusAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch still exists
        assert branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. [integrator] only the remote-tracking pre-release branch exists
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
    }

    @Test
    public void test_showIntegrationStatus_whenNoFeatureIsDelivered() {
        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the pre-release branch does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. [integrator] a random branch is checked-out
        def initialBranch = someRandomBranch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newShowIntegrationStatusAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the pre-release branch still does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. [integrator] the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }

    @Test
    public void test_showIntegrationStatus_whenPseudoPreReleaseBranchExists() {
        def initialBranch = someRandomBranch

        // Setup
        // ---------------------------------------------------------------------
        // 1. [dev] create and push the pseudo-pre-release branch
        env.devRepo.branch.add(name: pseudoPreReleaseBranch)
        pushBranchToRemote(env.devRepo, ext.continuous.centralGitRepository, pseudoPreReleaseBranch)

        // 2. [integrator] checkout a random branch
        env.integratorRepo.checkout(branch: initialBranch, createBranch: true)

        // Preconditions
        // ---------------------------------------------------------------------
        // 1. [central] the pre-release branch exists with a different case
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        assert preReleaseBranch != pseudoPreReleaseBranch
        assert preReleaseBranch.equalsIgnoreCase(pseudoPreReleaseBranch)

        // 2. [integrator] a random branch is checked-out
        assert isCheckedOut(env.integratorRepo, initialBranch)

        // Test
        // ---------------------------------------------------------------------
        IntegratorActions.newShowIntegrationStatusAction(ext, integratorGit).run()

        // Check results
        // ---------------------------------------------------------------------
        // 1. [integrator, central] the pre-release branch still does not exist
        assert ! branchesOf(env.integratorRepo).any { preReleaseBranch == it }
        assert ! remoteBranchesOf(env.integratorRepo).any { remotePreReleaseBranch == it }
        assert ! branchesOf(env.centralRepo).any { preReleaseBranch == it }

        // 2. [central] the pseudo-pre-release branch still exists
        assert branchesOf(env.centralRepo).any { pseudoPreReleaseBranch == it }

        // 3. [integrator] only the remote-tracking pre-release branch exists
        assert ! branchesOf(env.integratorRepo).any { pseudoPreReleaseBranch == it }
        assert remoteBranchesOf(env.integratorRepo).any { remotePseudoPreReleaseBranch == it }

        // 4. [integrator] the initial branch is still checked out
        assert isCheckedOut(env.integratorRepo, initialBranch)
    }
}
