/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.combined

import static groovy.io.FileType.*
import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.*
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchTip
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.branchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.commitSomething
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isCheckedOut
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isDescendant
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.isTracking
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.mergeIntoCurrentBranch
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.pushBranchToRemote
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.remoteBranchesOf
import static ro.amocanu.gradle.plugins.amworkflow.actions.GitTestUtil.tagsOf

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseTest
import ro.amocanu.gradle.plugins.amworkflow.actions.TestEnv
import ro.amocanu.gradle.plugins.amworkflow.actions.testhelpers.TestHelper
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GrGitRepo
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.RootExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MultipleFeaturesInVersionedWorkflowTest extends BaseTest {

    private static final PluginExt ext = new PluginExt(
        root: new RootExtension(
            workflowType: WorkflowType.VERSIONED.toString()),
        versioned: new VersionedExtension(
            centralGitRepository: 'origin',
            featureBranchPrefix: 'Task',
            preReleaseBranch: 'pre',
            mainBranch: 'master',
            mainIntegrationURLTrigger: '',
            ciUsername: '',
            ciPassword: '',
            versionPrefix: 'v',
            versionFile: 'version.properties',
            bumpVersionCommitMessage: { version -> "Bump it to ${version}!" },
            maintenanceBranchPrefix: 'Support',
            maintenanceIntegrationURLTriggers: [:]))

    private static final String featureName1 = 'FarmResources'
    private static final String featureBranch1 = "Task/${featureName1}"
    private static final String remoteFeatureBranch1 = "origin/Task/${featureName1}"

    private static final String featureName2 = 'FixFarmResources'
    private static final String featureBranch2 = "Task/${featureName2}"
    private static final String remoteFeatureBranch2 = "origin/Task/${featureName2}"
    
    private static final String featureName3 = 'BuyEquipment'
    private static final String featureBranch3 = "Task/${featureName3}"
    private static final String remoteFeatureBranch3 = "origin/Task/${featureName3}"

    private static final String featureName4 = 'DefeatEnemies'
    private static final String featureBranch4 = "Task/${featureName4}"
    private static final String remoteFeatureBranch4 = "origin/Task/${featureName4}"

    private static final String featureName5 = 'RedesignLevelUpSystem'
    private static final String featureBranch5 = "Task/${featureName5}"
    private static final String remoteFeatureBranch5 = "origin/Task/${featureName5}"

    private static final String releaseBranch = ext.versioned.mainBranch

    private static TestEnv env

    private static GitRepo devGit
    private static GitRepo integratorGit
    
    private static TestHelper helper

    @BeforeClass
    public static void setUp() {
        env = new TestEnv()
        
        devGit = GrGitRepo.open(env.devRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)
        integratorGit = GrGitRepo.open(env.integratorRepo.repository.rootDir.absolutePath, ext.versioned.centralGitRepository)

        helper = new TestHelper(env, ext, devGit, integratorGit)
    }
    
    @AfterClass
    public static void tearDown() {
        integratorGit.close()
        devGit.close()
        
        env.tearDown()
    }
    
    @Test
    public void step01_createAndDeliverFeature1() {
        helper.test_createFeature(featureName1, featureBranch1, remoteFeatureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_workOnFeatureBranch(featureBranch1)
        helper.test_deliverFeature(featureBranch1, remoteFeatureBranch1)
    }
    
    @Test
    public void step02_integrateFeature1() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch1 == it }
        
        helper.test_integrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch1 == it }
    }

    @Test
    public void step03_createFirstVersion() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createFirstVersion() // 0.1.0
    }
    
    @Test
    public void step04_createAndDeliverFeature2() {
        helper.test_createFeature(featureName2, featureBranch2, remoteFeatureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_workOnFeatureBranch(featureBranch2)
        helper.test_deliverFeature(featureBranch2, remoteFeatureBranch2)
    }
    
    @Test
    public void step05_integrateFeature2() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch2 == it }
        
        helper.test_integrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch2 == it }
    }
    
    @Test
    public void step06_createNewPatchVersion() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('patch', '0.1.1')
    }

    @Test
    public void step07_createAndDeliverFeature3() {
        helper.test_createFeature(featureName3, featureBranch3, remoteFeatureBranch3)
        helper.test_workOnFeatureBranch(featureBranch3)
        helper.test_deliverFeature(featureBranch3, remoteFeatureBranch3)
    }

    @Test
    public void step08_integrateFeature3() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch3 == it }
        
        helper.test_integrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch3 == it }
    }

    @Test
    public void step09_createAndDeliverFeature4() {
        helper.test_createFeature(featureName4, featureBranch4, remoteFeatureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
        helper.test_workOnFeatureBranch(featureBranch4)
        helper.test_deliverFeature(featureBranch4, remoteFeatureBranch4)
    }

    @Test
    public void step10_integrateFeature4() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch4 == it }
        
        helper.test_integrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch4 == it }
    }

    @Test
    public void step11_createNewMinorVersion() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('minor', '0.2.0')
    }

    @Test
    public void step12_createAndDeliverFeature5() {
        helper.test_createFeature(featureName5, featureBranch5, remoteFeatureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_workOnFeatureBranch(featureBranch5)
        helper.test_deliverFeature(featureBranch5, remoteFeatureBranch5)
    }

    @Test
    public void step13_integrateFeature5() {
        // Additional preconditions
        // ---------------------------------------------------------------------
        // 1. feature branch exists on central
        assert branchesOf(env.centralRepo).any { featureBranch5 == it }
        
        helper.test_integrateFeature()
        
        // Additional checks
        // ---------------------------------------------------------------------
        // 1. the delivered feature no longer exists on central
        assert ! branchesOf(env.centralRepo).any { featureBranch5 == it }
    }

    @Test
    public void step14_createNewMajorVersion() {
        env.integratorRepo.checkout(branch: releaseBranch)
        helper.test_createNewVersion('major', '1.0.0')
    }
}
