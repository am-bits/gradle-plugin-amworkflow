/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.plugin.ext
import groovy.transform.Canonical

import org.gradle.api.GradleException
import org.gradle.api.Project

import ro.amocanu.gradle.plugins.amworkflow.WorkflowPlugin

@Canonical
class PluginExt {
    RootExtension root
    ContinuousExtension continuous
    VersionedExtension versioned

    static void validate(Project project) {
        def wt = project."${WorkflowPlugin.ROOT_EXTENSION}".workflowType

        if (wt != WorkflowType.CONTINUOUS.toString() && wt != WorkflowType.VERSIONED.toString()) {
            throw new GradleException("Invalid workflow type: ${wt}")
        }

        def workflowType = WorkflowType.valueOf(wt)
        
        if (workflowType == WorkflowType.CONTINUOUS && project.getExtensions().findByType(VersionedExtension)) {
            throw new GradleException("Workflow type is 'CONTINUOUS' but versioned configuration found!")
        }

        if (workflowType == WorkflowType.VERSIONED && project.getExtensions().findByType(ContinuousExtension)) {
            throw new GradleException("Workflow type is 'VERSIONED' but continuous configuration found!")
        }
    }

    static WorkflowType workflowTypeOf(Project project) {
        def wt = project."${WorkflowPlugin.ROOT_EXTENSION}".workflowType
        def workflowType = WorkflowType.valueOf(wt)

        return workflowType
    }

    static PluginExt extensionOf(Project project) {
        return new PluginExt(
            root: rootExtensionOf(project),
            continuous: continuousExtensionOf(project),
            versioned: versionedExtensionOf(project))
    }

    static RootExtension rootExtensionOf(Project project) {
        return new RootExtension(workflowType: workflowTypeOf(project))
    }

    static ContinuousExtension continuousExtensionOf(Project project) {
        if (workflowTypeOf(project) == WorkflowType.VERSIONED) {
            return null
        }

        def ext = project."${WorkflowPlugin.ROOT_EXTENSION}"."${WorkflowPlugin.CONTINUOUS_EXTENSION}"

        def centralGitRepository = ext.centralGitRepository
        def gitUsername = ext.gitUsername
        def gitPassword = ext.gitPassword
        def featureBranchPrefix = ext.featureBranchPrefix
        def preReleaseBranch= ext.preReleaseBranch
        def releaseBranch= ext.releaseBranch
        def releaseTagPrefix = ext.releaseTagPrefix
        def integrationURLTrigger = ext.integrationURLTrigger
        def ciUsername = ext.ciUsername
        def ciPassword = ext.ciPassword

        def extStr = "${WorkflowPlugin.ROOT_EXTENSION}.${WorkflowPlugin.CONTINUOUS_EXTENSION}"

        mustNotContainSlashes("${extStr}.centralGitRepository", centralGitRepository)
        mustNotContainSlashes("${extStr}.featureBranchPrefix", featureBranchPrefix)
        mustNotContainSlashes("${extStr}.preReleaseBranch", preReleaseBranch)
        mustNotContainSlashes("${extStr}.releaseBranch", releaseBranch)
        
        return new ContinuousExtension(
            centralGitRepository: centralGitRepository,
            gitUsername: gitUsername,
            gitPassword: gitPassword,
            featureBranchPrefix: featureBranchPrefix,
            preReleaseBranch: preReleaseBranch,
            releaseBranch: releaseBranch,
            releaseTagPrefix: releaseTagPrefix,
            integrationURLTrigger: integrationURLTrigger,
            ciUsername: ciUsername,
            ciPassword: ciPassword)
    }

    static VersionedExtension versionedExtensionOf(Project project) {
        if (workflowTypeOf(project) == WorkflowType.CONTINUOUS) {
            return null
        }
      
        def ext = project."${WorkflowPlugin.ROOT_EXTENSION}"."${WorkflowPlugin.VERSIONED_EXTENSION}"

        def centralGitRepository = ext.centralGitRepository
        def gitUsername = ext.gitUsername
        def gitPassword = ext.gitPassword
        def featureBranchPrefix = ext.featureBranchPrefix
        def preReleaseBranch= ext.preReleaseBranch
        def mainBranch = ext.mainBranch
        def versionPrefix = ext.versionPrefix
        def mainIntegrationURLTrigger = ext.mainIntegrationURLTrigger
        def ciUsername = ext.ciUsername
        def ciPassword = ext.ciPassword
        def versionFile = ext.versionFile
        def bumpVersionCommitMessage = ext.bumpVersionCommitMessage
        def maintenanceBranchPrefix= ext.maintenanceBranchPrefix
        def maintenanceIntegrationURLTriggers = ext.maintenanceIntegrationURLTriggers

        def extStr = "${WorkflowPlugin.ROOT_EXTENSION}.${WorkflowPlugin.VERSIONED_EXTENSION}"

        mustNotContainSlashes("${extStr}.centralGitRepository", centralGitRepository)
        mustNotContainSlashes("${extStr}.featureBranchPrefix", featureBranchPrefix)
        mustNotContainSlashes("${extStr}.preReleaseBranch", preReleaseBranch)
        mustNotContainSlashes("${extStr}.mainBranch", mainBranch)
        mustNotContainSlashes("${extStr}.versionPrefix", versionPrefix)
        mustNotContainSlashes("${extStr}.maintenanceBranchPrefix", maintenanceBranchPrefix)

        return new VersionedExtension(
            centralGitRepository: centralGitRepository,
            gitUsername: gitUsername,
            gitPassword: gitPassword,
            featureBranchPrefix: featureBranchPrefix,
            preReleaseBranch: preReleaseBranch,
            mainBranch: mainBranch,
            versionPrefix: versionPrefix,
            mainIntegrationURLTrigger: mainIntegrationURLTrigger,
            ciUsername: ciUsername,
            ciPassword: ciPassword,
            versionFile: versionFile,
            bumpVersionCommitMessage: bumpVersionCommitMessage,
            maintenanceIntegrationURLTriggers: maintenanceIntegrationURLTriggers,
            maintenanceBranchPrefix: maintenanceBranchPrefix)
    }

    private static void mustNotContainSlashes(String property, String value) {
        if (value.contains('/')) {
            throw new GradleException("The '/' character is not allowed in property '${property}'!")
        }
    }
}
