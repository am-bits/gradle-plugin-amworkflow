/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.conf

import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.ContinuousExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.PluginExt
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.VersionedExtension
import ro.amocanu.gradle.plugins.amworkflow.plugin.ext.WorkflowType

class ConfigurationFactory {

    private static final String DELIM = '/'
    private static final String DELIM2 = '_'
    
    static Configuration fromExtension(PluginExt ext) {
        switch (ext.root.workflowType) {
            case WorkflowType.CONTINUOUS.toString():
                return fromContinuousExtension(ext.continuous)

            case WorkflowType.VERSIONED.toString():
                return fromVersionedExtension(ext.versioned)

            default:
                assert false
        }
    }

    static Configuration fromContinuousExtension(ContinuousExtension ext) {
        String fPrefix = ext.featureBranchPrefix + DELIM
        int sz = fPrefix.size()

        return new Configuration(
            centralGitRepository: ext.centralGitRepository,
            featureBranchFromFeatureName: { featureName -> fPrefix + featureName },
            featureNameFromFeatureBranch: { featureBranch -> featureBranch[sz..-1] },
            isFeatureBranch: { string -> string ==~ (fPrefix + '.+') },
            isPseudoFeatureBranch: { String branch -> 
                !(branch ==~ (fPrefix + '.+')) && (branch.size() > sz) ? fPrefix.equalsIgnoreCase(branch[0..sz-1]) : false },
            preReleaseBranch: ext.preReleaseBranch,
            releaseBranch: ext.releaseBranch,
            releaseNameFromReleaseId: { releaseId -> ext.releaseTagPrefix + releaseId },
            releaseIdFromReleaseName: { releaseName -> releaseName[ext.releaseTagPrefix.size()..-1] },
            isReleaseName: { string -> string ==~ ext.releaseTagPrefix + '.+' },
            releaseCIJobURL: ext.integrationURLTrigger,
            ciUsername: ext.ciUsername,
            ciPassword: ext.ciPassword,
            versioned: null)
    }

    static Configuration fromVersionedExtension(VersionedExtension ext) {
        String fPrefix = ext.featureBranchPrefix + DELIM
        int sz = fPrefix.size()

        return new Configuration(
            centralGitRepository: ext.centralGitRepository,
            featureBranchFromFeatureName: { featureName -> fPrefix + featureName },
            featureNameFromFeatureBranch: { featureBranch -> featureBranch[sz..-1] },
            isFeatureBranch: { string -> string ==~ (fPrefix + '.+') },
            isPseudoFeatureBranch: { String branch -> 
                !(branch ==~ (fPrefix + '.+')) && (branch.size() > sz) ? fPrefix.equalsIgnoreCase(branch[0..sz-1]) : false },
            preReleaseBranch: ext.preReleaseBranch,
            releaseBranch: ext.mainBranch,
            releaseNameFromReleaseId: { releaseId -> ext.versionPrefix + releaseId },
            releaseIdFromReleaseName: { releaseName -> releaseName[ext.versionPrefix.size()..-1] },
            isReleaseName: { string -> string ==~ ext.versionPrefix + '.+' },
            releaseCIJobURL: ext.mainIntegrationURLTrigger,
            ciUsername: ext.ciUsername,
            ciPassword: ext.ciPassword,
            versioned: createVersionedConfiguration(ext))
    }

    static Configuration fromVersionedExtensionWithMaintenedVersion(VersionedExtension ext, String maintainedVersion) {
        def versioned = createVersionedConfiguration(ext)
        def versionPrefix = ext.versionPrefix
        def maintenance = versioned.maintenanceBranchFromMaintainedVersion.curry(maintainedVersion)

        String fPrefix = maintenance(ext.featureBranchPrefix) + DELIM
        int sz = fPrefix.size()

        return new Configuration(
            centralGitRepository: ext.centralGitRepository,
            featureBranchFromFeatureName: { featureName -> fPrefix + featureName },
            featureNameFromFeatureBranch: { featureBranch -> featureBranch[sz..-1]},
            isFeatureBranch: { string -> string ==~ fPrefix + '.+' },
            isPseudoFeatureBranch: { String branch -> 
                !(branch ==~ (fPrefix + '.+')) && (branch.size() > sz) ? fPrefix.equalsIgnoreCase(branch[0..sz-1]) : false },
            preReleaseBranch: maintenance(ext.preReleaseBranch),
            releaseBranch: maintenance(''),
            releaseNameFromReleaseId: { releaseId -> ext.versionPrefix + releaseId },
            releaseIdFromReleaseName: { releaseName -> releaseName[ext.versionPrefix.size()..-1] },
            isReleaseName: { string -> string ==~ ext.versionPrefix + '.+' },
            releaseCIJobURL: ext.maintenanceIntegrationURLTriggers[maintainedVersion as int],
            ciUsername: ext.ciUsername,
            ciPassword: ext.ciPassword,
            versioned: versioned)
    }

    private static VersionedConfiguration createVersionedConfiguration(VersionedExtension ext) {
        def mRoot = ext.maintenanceBranchPrefix + DELIM2 + ext.versionPrefix
        def mPattern = mRoot + '([1-9][0-9]*)' + "(${DELIM2}.+)?"

        return new VersionedConfiguration(
            versionFile: ext.versionFile,
            bumpVersionCommitMessage: ext.bumpVersionCommitMessage,
            maintenanceBranchFromMaintainedVersion: { maintainedVersion, subBranch ->
                mRoot + maintainedVersion + (subBranch ? (DELIM2 + subBranch) : '') },
            maintainedVersionFromMaintenanceBranch: { maintenanceBranch -> (maintenanceBranch =~ mPattern)[0][1] },
            isMaintenanceBranch: { string -> string ==~ mPattern },
            isPseudoMaintenanceBranch: { String branch ->
                int sz = mRoot.size()
                !(branch ==~ mPattern) && ((branch.size() > sz) ? mRoot.equalsIgnoreCase(branch[0..sz-1]) : false)
            })
    }
}
