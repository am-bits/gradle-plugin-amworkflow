/*******************************************************************************
 * Copyright 2016 Andreea Mocanu & Alexandru Mocanu
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package ro.amocanu.gradle.plugins.amworkflow.actions.integrator

import groovy.transform.PackageScope

import org.gradle.api.GradleException

import ro.amocanu.gradle.plugins.amworkflow.actions.BaseAction
import ro.amocanu.gradle.plugins.amworkflow.actions.conf.Configuration
import ro.amocanu.gradle.plugins.amworkflow.actions.versioned.Version
import ro.amocanu.gradle.plugins.amworkflow.actions.versioned.VersionFile
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo
import ro.amocanu.gradle.plugins.amworkflow.git.GitRepo.Relation

@PackageScope
class CreateNewVersion extends BaseAction {

    private final Closure<Version> bumper
    private final VersionFile file
    private final String releaseBranch
    private final String remoteReleaseBranch

    private String initialBranch
    private String initialReleaseBranchCommitId

    private static final Version NO_VERSION = new Version(0, 0, 0)
    private static final Version FIRST_VERSION = new Version(0, 1, 0)

    private static final Map<String, Closure<Version> > bumpers = [
        'major':Version.makeNewMajorFrom,
        'minor':Version.makeNewMinorFrom,
        'patch':Version.makeNewPatchFrom
    ]

    CreateNewVersion(Configuration conf, GitRepo git, String versionType) {
        super(conf, git)
        file = new VersionFile(conf.versioned.versionFile, git.rootDir())
        bumper = getBumper(versionType)
        releaseBranch = conf.releaseBranch
        remoteReleaseBranch = "${conf.centralGitRepository}/${conf.releaseBranch}"
    }

    private Closure<Version> getBumper(String versionType) {
        def bumper = bumpers.get(versionType)

        if (!bumper) {
            throw new GradleException("Invalid value for 'versionType' property: '${versionType}', must be one of: major, minor or patch")
        }

        return bumper
    }

    @Override
    void run() {
        repoMustNotHaveChanges()
        mustHaveRemoteReleaseBranch()

        fetchFromCentralAndPrune()

        initialBranch = git.currentBranch()
        try {
            checkOutBranch(releaseBranch)
            syncReleaseBranchWithUpstream()
            initialReleaseBranchCommitId = git.commitIdOfBranch(releaseBranch)

            def highestAncestorVersion = findHighestVersion()
            def bumpedVersion = computeAndValidateBumpedVersion(highestAncestorVersion)
            def versionTag = computeAndValidateNewVersionTag(bumpedVersion)           
            newVersionMustContainChanges(highestAncestorVersion)
            bumpVersionInFile(bumpedVersion)
            createReleaseTag()
            pushToCentral()
        } catch (any) {
            rollBackOnException()
            throw any
        }
    }

    private void syncReleaseBranchWithUpstream() {
        def releaseBranch = conf.releaseBranch
        assert git.currentBranch() == releaseBranch

        def upstreamBranchName = "${conf.centralGitRepository}/${releaseBranch}"
        def branchToUpstreamRelation = git.relation(
            git.commitIdOfBranch(releaseBranch),
            git.commitIdOfRemoteBranch(upstreamBranchName))

        switch (branchToUpstreamRelation) {
            case Relation.SAME:
                // nothing to do, already sync-ed
                break

            case Relation.DESCENDANT:
                // nothing to do, will be pushed to remote after creating the version commit
                break

            case Relation.ANCESTOR:
                git.fastForwardCurrentBranchTo(upstreamBranchName)
                break

            case Relation.NONE:
                throw new GradleException("Release branch '${releaseBranch}' must be merged with its upstream, '${upstreamBranchName}', before creating a new version!")
                break

            default:
            assert false
        }
    }

    private Version findHighestVersion() {
        Iterable candidates = git.allAncestorTags()
        Iterable versions = candidates
        .findAll { candidate ->
            conf.isReleaseName(candidate) &&
            Version.isVersionString(conf.releaseIdFromReleaseName(candidate))
        }
        .collect { versionString ->
            Version.fromString(conf.releaseIdFromReleaseName(versionString))
        }

        return versions ? versions.sort().last() : NO_VERSION
    }

    private Version computeAndValidateBumpedVersion(Version highestAncestorVersion) {
        def bumpedVersion = bumper(highestAncestorVersion)

        if (bumpedVersion < FIRST_VERSION) {
            throw new GradleException("Version number less than ${FIRST_VERSION} is not allowed!")
        }

        return bumpedVersion
    }

    private String computeAndValidateNewVersionTag(Version bumpedVersion) {
        String versionTag = conf.releaseNameFromReleaseId(bumpedVersion.toString())
        if (git.isTag(versionTag)) {
            def commit = git.commitIdOfTag(versionTag)
            throw new GradleException("Version '${bumpedVersion}' already exists; see commit '${commit}'!")
        }

        def tag = git.allTags().find { versionTag.equalsIgnoreCase(it) }
        if (tag) {
            def commit = git.commitIdOfTag(tag)
            throw new GradleException("Version tag '${versionTag}' already exists with a different case: '${tag}'; see commit '${commit}'!")
        }

        if (bumpedVersion.patch > 0) {
            def nextMinorVersion = Version.makeNewMinorFrom(bumpedVersion)
            def nextMinorVersionTag = conf.releaseNameFromReleaseId(nextMinorVersion.toString())
            if (git.isTag(nextMinorVersionTag)) {
                throw new GradleException("Cannot create new patch version '${bumpedVersion}' because next minor version ('${nextMinorVersion}') already exists!")
            }
        }

        return versionTag
    }

    private void newVersionMustContainChanges(Version highestAncestorVersion) {
        if (highestAncestorVersion != NO_VERSION) {
            def highestVersionTag = conf.releaseNameFromReleaseId(highestAncestorVersion.toString())
            def commitWithHighestVersionTag = git.commitIdOfTag(highestVersionTag)
            def commitOnCurrentBranch = git.commitIdOfRemoteBranch(remoteReleaseBranch)
            if (commitOnCurrentBranch == commitWithHighestVersionTag) {
                throw new GradleException("Cannot create new version because there are no changes since version '${highestAncestorVersion}'!")
            }
        }
    }

    private void bumpVersionInFile(Version bumpedVersion) {
        def currentVersion = file.readVersion()

        boolean fileNeedsUpdate = (bumpedVersion != currentVersion)
        if (fileNeedsUpdate) {
            file.writeVersion(bumpedVersion)
            git.commitSingleFile(conf.versioned.versionFile, conf.versioned.bumpVersionCommitMessage(bumpedVersion))
        } else {
            println "Version in '${conf.versioned.versionFile}' is already bumped, did nothing."
        }
    }

    private void createReleaseTag() {
        def version = file.readVersion()
        def tagName = conf.releaseNameFromReleaseId(version.toString())
        git.createTag(tagName)
    }

    private void rollBackOnException() {
        if (initialReleaseBranchCommitId && git.commitIdOfBranch(releaseBranch) != initialReleaseBranchCommitId) {
            git.resetCurrentBranchToCommit(initialReleaseBranchCommitId)
        }
        
        if (initialBranch && git.currentBranch() != initialBranch) {
            git.checkoutBranch(initialBranch)
        }
    }

    private void pushToCentral() {
        git.pushBranchToCentralWithTags(releaseBranch)
    }
}
