# Contact us
Leave feedback.
___

## Submit a bug or request a feature

Please use the project's [issue tracker](https://bitbucket.org/am-bits/gradle-plugin-amworkflow/issues/new). (You will need a BitBucket account for this.)

___

## Leave us a message

<form id="contact-form" method="post" role="form">
  <div class="messages"></div>
  <div class="controls">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="form_name">Name</label>
          <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="form_email">Email *</label>
          <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label for="form_message">Message *</label>
        <textarea id="form_message" name="message" class="form-control" placeholder="Tell us something *" rows="4" required="required" data-error="A message is required."></textarea>
        <div class="help-block with-errors"></div>
      </div>
    </div>
    <div class="col-md-12">
      <input type="text" name="_gotcha" style="display:none"/>
      <input type="submit" class="btn btn-success btn-send" value="Send message">
    </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p class="text-muted"><strong>*</strong> These fields are required. Contact form template by <a href="http://bootstrapious.com" target="_blank">Bootstrapious</a>.</p>
      </div>
    </div>
  </div>
</form>
<script>
  var contactForm =  document.getElementById('contact-form');
  contactForm.setAttribute('action', 'https' + ':' + '//' + 'formspree' + '.' + 'io' + '/' + 'am' + '.' + 'bits' +'.' + 'contact' + '@' + 'gmail' + '.' + 'com');
</script>
